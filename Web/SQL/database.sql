/*
 * © Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */
-- MySQL dump 10.13  Distrib 5.5.15, for Win32 (x86)
--
-- Host: localhost    Database: virtuassist
-- ------------------------------------------------------
-- Server version	5.1.40-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `virtuassist`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `virtuassist` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `virtuassist`;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  `description` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'ATM','Automatic Teller Machine'),(2,'ScanPrinter','Multifunction printers'),(3,'Printer','Printer with only that function');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controls`
--

DROP TABLE IF EXISTS `controls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) DEFAULT '',
  `idSurface` int(11) unsigned NOT NULL,
  `Type` int(11) unsigned NOT NULL,
  `figure` int(11) unsigned NOT NULL,
  `color` char(12) DEFAULT NULL,
  `borderColor` char(12) DEFAULT NULL,
  `lineBorder` int(11) NOT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `Width` int(11) DEFAULT NULL,
  `Height` int(11) DEFAULT NULL,
  `radius` int(11) DEFAULT NULL,
  `text` char(100) DEFAULT NULL,
  `fontsize` int(11) DEFAULT NULL,
  `fontfamily` char(40) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `image` blob,
  PRIMARY KEY (`id`),
  KEY `Type` (`Type`),
  KEY `figure` (`figure`),
  KEY `parent` (`idSurface`),
  CONSTRAINT `controls_ibfk_1` FOREIGN KEY (`Type`) REFERENCES `typecontrols` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `controls_ibfk_2` FOREIGN KEY (`figure`) REFERENCES `typefigures` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `controls_ibfk_3` FOREIGN KEY (`idSurface`) REFERENCES `surfaces` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(60) NOT NULL DEFAULT '',
  `category` int(11) unsigned NOT NULL,
  `description` char(255) NOT NULL DEFAULT '',
  `qrCode` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  CONSTRAINT `devices_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `paths`
--

DROP TABLE IF EXISTS `paths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paths` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  `description` varchar(500) NOT NULL DEFAULT '',
  `idDevice` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device` (`idDevice`),
  CONSTRAINT `paths_ibfk_1` FOREIGN KEY (`idDevice`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `steps`
--

DROP TABLE IF EXISTS `steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `steps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idPath` int(10) unsigned NOT NULL,
  `idControl` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `instruction` varchar(2000) NOT NULL DEFAULT '',
  `image` blob,
  PRIMARY KEY (`id`),
  KEY `path` (`idPath`),
  KEY `control` (`idControl`),
  KEY `type` (`type`),
  CONSTRAINT `steps_ibfk_1` FOREIGN KEY (`idPath`) REFERENCES `paths` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `steps_ibfk_3` FOREIGN KEY (`idControl`) REFERENCES `controls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `steps_ibfk_4` FOREIGN KEY (`type`) REFERENCES `typesteps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `surfaces`
--

DROP TABLE IF EXISTS `surfaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surfaces` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) DEFAULT NULL,
  `type` int(11) unsigned NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `idDevice` int(11) unsigned DEFAULT NULL COMMENT 'this field is to reference a touch screen state from a control',
  `tag` int(10) unsigned DEFAULT NULL COMMENT 'this field is to reference a touch screen state. The touch screen has many states. Short the state with this field',
  `image` longblob COMMENT 'The editor user can upload a picture from each surface for a device. This picture can be shown in the editor and in the app mobile',
  `json` blob COMMENT 'The editor user can upload a description in json format from each surface for a device. this json file will be used to reload data for the editor',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `parent` (`idDevice`),
  CONSTRAINT `surfaces_ibfk_1` FOREIGN KEY (`type`) REFERENCES `typesurfaces` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `surfaces_ibfk_2` FOREIGN KEY (`idDevice`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `typecontrols`
--

DROP TABLE IF EXISTS `typecontrols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typecontrols` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typecontrols`
--

LOCK TABLES `typecontrols` WRITE;
/*!40000 ALTER TABLE `typecontrols` DISABLE KEYS */;
INSERT INTO `typecontrols` VALUES (1,'Button'),(2,'Speaker'),(3,'Tray'),(4,'Bending tray'),(5,'Plug'),(6,'Low relief decoration'),(7,'Relief decoration'),(8,'Sticker'),(9,'Screen display'),(10,'Touchscreen display'),(11,'Knob'),(12,'Lever'),(13,'Latch'),(14,'Hinged door'),(15,'Sliding door'),(16,'Input slot'),(17,'Bending slot'),(18,'Door handle'),(19,'Panel'),(20,'Element of Interest');
/*!40000 ALTER TABLE `typecontrols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typefigures`
--

DROP TABLE IF EXISTS `typefigures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typefigures` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typefigures`
--

LOCK TABLES `typefigures` WRITE;
/*!40000 ALTER TABLE `typefigures` DISABLE KEYS */;
INSERT INTO `typefigures` VALUES (1,'Line'),(2,'Rectangle'),(3,'FillRectangle'),(4,'Circle'),(5,'FillCircle'),(6,'Text'),(7,'Image');
/*!40000 ALTER TABLE `typefigures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typesteps`
--

DROP TABLE IF EXISTS `typesteps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typesteps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typesteps`
--

LOCK TABLES `typesteps` WRITE;
/*!40000 ALTER TABLE `typesteps` DISABLE KEYS */;
INSERT INTO `typesteps` VALUES (1,'Insert'),(2,'Push'),(3,'Pull'),(4,'Slide'),(5,'Take'),(6,'Drop'),(7,'Open'),(8,'Close'),(9,'Check'),(10,'Press');
/*!40000 ALTER TABLE `typesteps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typesurfaces`
--

DROP TABLE IF EXISTS `typesurfaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typesurfaces` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typesurfaces`
--

LOCK TABLES `typesurfaces` WRITE;
/*!40000 ALTER TABLE `typesurfaces` DISABLE KEYS */;
INSERT INTO `typesurfaces` VALUES (1,'Front'),(2,'Back'),(3,'Top'),(4,'Bottom'),(5,'Left'),(6,'Right'),(7,'Dynamic');
/*!40000 ALTER TABLE `typesurfaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typeusers`
--

DROP TABLE IF EXISTS `typeusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typeusers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typeusers`
--

LOCK TABLES `typeusers` WRITE;
/*!40000 ALTER TABLE `typeusers` DISABLE KEYS */;
INSERT INTO `typeusers` VALUES (1,'User'),(2,'Editor'),(3,'Administrator');
/*!40000 ALTER TABLE `typeusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  `password` char(40) NOT NULL DEFAULT '',
  `mail` char(60) NOT NULL DEFAULT '',
  `type` int(11) unsigned NOT NULL,
  `text` tinyint(1) NOT NULL COMMENT 'boolean setting',
  `video` tinyint(1) NOT NULL COMMENT 'Boolean setting',
  `image` tinyint(1) NOT NULL COMMENT 'boolean setting',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type`) REFERENCES `typeusers` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usersdevices`
--

DROP TABLE IF EXISTS `usersdevices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersdevices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `device` int(10) unsigned NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`user`),
  KEY `user` (`user`),
  KEY `device` (`device`),
  CONSTRAINT `usersdevices_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usersdevices_ibfk_2` FOREIGN KEY (`device`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userspaths`
--

DROP TABLE IF EXISTS `userspaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userspaths` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `path` int(10) unsigned NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `device` (`path`),
  CONSTRAINT `userspaths_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userspaths_ibfk_3` FOREIGN KEY (`path`) REFERENCES `paths` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-10 14:01:45
