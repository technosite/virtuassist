#VirtuAssist

## License

This project is under Apache V2 license. All files and resources should be under this license.
Please, read the [Apache V2](http://www.apache.org/licenses/LICENSE-2.0) license before any action with any content of this project.

## Folder structure

This repository has four main folders to store all files and resources for the project.
These folders are:

- Doc: this folder contains all documentation files for this project
- Mobile: this folder contains all files and resources about mobile applications for smart devices and Google glass.
- Web: this folder contains all files about web service and web contents.
- Bin: this folder contains all binary files to run the Virtuassist service

