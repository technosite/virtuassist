package com.technosite.translator;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import java.util.HashMap;

import android.content.Context;

import com.technosite.virtuassist.R;

public class TranslatorManager {

	public TranslatorManager(Context ct) {
				words = new HashMap<String,String>();
		String lang = ct.getResources().getString(R.string.language);
		if (lang.equalsIgnoreCase("English")) loadWords(1);
		else if (lang.equalsIgnoreCase("Spanish")) loadWords(2);
	}

	
	private static TranslatorManager  _instance = null;
	
	public static TranslatorManager  getSharedInstance(Context ct) {
		if (_instance == null) _instance = new TranslatorManager (ct);
		return _instance;
	}
	
	// ** Methods to get translated words
	private HashMap<String,String> words = null;
	
	public String get(String key) {
		return words.get(key);
	}
	
	
	// ** Load method
	
	private void loadWords(int idLanguage) {
if (idLanguage == 1) { // English
	 words.put("Bot�n","Button");
	 words.put( "Altavoz","Speaker");
	 words.put( "Bandeja","Tray");
	 words.put( "Bandeja expendedora/deslizante","Bending tray");
	 words.put( "Clavija/Enchufe","Plug");
	 words.put( "Decoraci�n en bajo relieve","Low relief decoration");
	 words.put( "Decoraci�n en relieve","Relief decoration");
	 words.put( "Etiqueta/Pegatina","Sticker");
	 words.put( "Indicador/Pantalla","Screen display");
	 words.put( "Indicador/Pantalla t�ctil","Touchscreen display");
	 words.put( "Pomo","Knob");
	 words.put( "Palanca","Lever");
	 words.put( "Pestillo","Latch");
	 words.put( "Puerta/Tapadera abatible","Hinged door");
	 words.put( "Puerta/Tapadera corredera","Sliding door");
	 words.put( "Ranura","Input slot");
	 words.put( "Rendija expendedora","Bending slot");
	 words.put( "Tirador","Door handle");
	 words.put( "Panel","Panel");
	 words.put( "Elemento de Inter�s","Element of Interest");
	 words.put( "Cajero Autom�tico","ATM");
	 words.put( "Impresora multifunci�n","Multifunction printer");
	 words.put( "Impresora","Printer");
	 words.put( "M�quina de Vending","Vending machine");
	 words.put( "Tel�fono","Telephone");
/*
	 words.put( "Button","Button");
	 words.put( "Speaker","Speaker");
	 words.put( "Tray","Tray");
	 words.put( "Bending tray","Bending tray");
	 words.put( "Plug","Plug");
	 words.put( "Low relief decoration","Low relief decoration");
	 words.put( "Relief decoration","Relief decoration");
	 words.put( "Sticker","Sticker");
	 words.put( "Screen display","Screen display");
	 words.put( "Touchscreen display","Touchscreen display");
	 words.put( "Knob","Knob");
	 words.put( "Lever","Lever");
	 words.put( "Latch","Latch");
	 words.put( "hinged door","Hinged door");
	 words.put( "Sliding door","Sliding door");
	 words.put( "Input slot","Input slot");
	 words.put( "Bending slot","Bending slot");
	 words.put( "Door handle","Door handle");
	 words.put( "Element of Interest","Element of Interest");
	 words.put( "ATM","ATM");
	 words.put( "Multifunction printer","Multifunction printer");
	 words.put( "Printer","Printer");
	 words.put( "Vending machine","Vending machine");
	 words.put( "Telephone","Telephone");
	 */	
} else if (idLanguage == 2) { // Spanish
	/*
		words.put("Bot�n","Bot&#243;n");
		words.put("Altavoz","Altavoz");
		words.put("Bandeja","Bandeja");
		words.put("Bandeja expendedora/deslizante","Bandeja expendedora/deslizante");
		words.put("Clavija/Enchufe","Clavija/Enchufe");
		words.put("Decoraci�n en bajo relieve","Decoraci&#243;n en bajo relieve");
		words.put("Decoraci�n en relieve","Decoraci&#243;n en relieve");
		words.put("Etiqueta/Pegatina","Etiqueta/Pegatina");
		words.put("Indicador/Pantalla","Indicador/Pantalla");
		words.put("Indicador/Pantalla t�ctil","Indicador/Pantalla t&#225;ctil");
		words.put("Pomo","Pomo");
		words.put("Palanca","Palanca");
		words.put("Pestillo","Pestillo");
		words.put("Puerta/Tapadera abatible","Puerta/Tapadera abatible");
		words.put("Puerta/Tapadera corredera","Puerta/Tapadera corredera");
		words.put("Ranura","Ranura");
		words.put("Rendija expendedora","Rendija expendedora");
		words.put("Tirador","Tirador");
		words.put("Panel","Panel");
		words.put("Elemento de Inter�s","Elemento de Inter&#233;s");
		words.put("Cajero Autom�tico","Cajero Autom&#225;tico");
		words.put("Impresora multifunci�n","Impresora multifunci&#243;n");
		words.put("Impresora","Impresora");
		words.put("M�quina de Vending","M&#225;quina de Vending");
		words.put("Tel�fono","Tel&#233;fono");
*/		    
		words.put("Button","Bot&#243;n");
		words.put("Speaker","Altavoz");
		words.put("Tray","Bandeja");
		words.put("Bending tray","Bandeja expendedora/deslizante");
		words.put("Plug","Clavija/Enchufe");
		words.put("Low relief decoration","Decoraci&#243;n en bajo relieve");
		words.put("Relief decoration","Decoraci&#243;n en relieve");
		words.put("Sticker","Etiqueta/Pegatina");
		words.put("Screen display","Indicador/Pantalla");
		words.put("Touchscreen display","Indicador/Pantalla t&#225;ctil");
		words.put("Knob","Pomo");
		words.put("Lever","Palanca");
		words.put("Latch","Pestillo");
		words.put("hinged door","Puerta/Tapadera abatible");
		words.put("Sliding door","Puerta/Tapadera corredera");
		words.put("Input slot","Ranura");
		words.put("Bending slot","Rendija expendedora");
		words.put("Door handle","Tirador");
		words.put("Element of Interest","Elemento de Inter&#233;s");
		words.put("ATM","Cajero Autom&#225;tico");
		words.put("Multifunction printer","Impresora multifunci&#243;n");
		words.put("Printer","Impresora");
		words.put("Vending machine","M&#225;quina de Vending");
		words.put("Telephone","Tel&#233;fono");		
	}
	}
	
	
}
