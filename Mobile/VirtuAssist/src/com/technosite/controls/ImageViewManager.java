package com.technosite.controls;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import android.annotation.SuppressLint;
import android.os.Build;
import android.widget.ImageView;

public class ImageViewManager {


	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void changeAlpha(ImageView image, int value) {
		if (Build.VERSION.SDK_INT>=16) image.setImageAlpha(value);		
		else image.setAlpha(value);
	}
}
