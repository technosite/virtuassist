package com.technosite.controls;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CustomImageView extends ImageView {

// ** Constructors
	
	public CustomImageView(Context context, AttributeSet attrs, int defStyle){
	    super(context, attrs,defStyle);
	}
	 
	public CustomImageView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	}
	 
	public CustomImageView(Context context) {
	    super(context);
	}
	
	// ** Size management
	
	public int currentWidth = 0;
	public int currentHeight = 0;
	public boolean resized = false;
	
    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        this.currentWidth = xNew;
        this.currentHeight = yNew;
        this.resized  = true;
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        }
    
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		if (!this.resized  ) {
			this.currentWidth = this.getWidth();
	        this.currentHeight = this.getHeight();
	        this.resized  = true;			
	         
		}
		
		}

}
