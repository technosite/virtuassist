package com.technosite.controls;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import java.io.Serializable;

public class Frame implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;
	
	public int x = 0;
	public int y = 0;
	public int width = 0;
	public int height = 0;
	
	public int top = 0;
	public int bottom = 0;
	public int left = 0;
	public int right = 0;
	
	public int centerX = 0;
	public int centerY = 0;
	
	
	public Frame(int pX, int pY, int pWidth, int pHeight) {
		x = pX;
		y = pY;
		width = pWidth;
		height = pHeight;
		top = y;
		bottom = y + height;
		left = x;
		right = x + width;
		centerX = (right-left) / 2;
		centerY = (bottom-top) / 2;
	}
	
	public boolean isPointInFrame(int pX, int pY) {
		boolean result = false;
		if (pX>=left && pX<=right) {
			if (pY<=bottom && pY>=top) result = true;
		}
			
				return result;
	}
	
	public boolean isOverlappingWithFrame(Frame pFrame) {
		boolean result = false;
		if (isPointInFrame(pFrame.left, pFrame.top)) result = true;
		if (isPointInFrame(pFrame.right,pFrame.top)) result = true;
		if (isPointInFrame(pFrame.left,pFrame.bottom)) result = true;
		if (isPointInFrame(pFrame.right,pFrame.bottom)) result = true;
		if (pFrame.isPointInFrame(this.left, this.top)) result = true;
		if (pFrame.isPointInFrame(this.right,this.top)) result = true;
		if (pFrame.isPointInFrame(this.left,this.bottom)) result = true;
		if (pFrame.isPointInFrame(this.right,this.bottom)) result = true;
		
				
		return result;
	}
	
	public boolean isInFrame(Frame pFrame) {
		boolean result = false;
		if (this.top>=pFrame.top && this.left>=pFrame.left) {
			if (this.bottom<=pFrame.bottom && this.right<=pFrame.right) result = true;			
		}
			
		return result;
	}
	
	public Frame adjustToFrame(Frame pFrame) {
		Frame result = new Frame(this.x,this.y,this.width,this.height);
		if (result.top<pFrame.top) {
			result.y = pFrame.y;
			result.height = result.height - pFrame.y;
		}
		if (result.left<pFrame.left) {
result.x = pFrame.x;
result.width = result.width -pFrame.x;
		}
		result.update();
				if (result.bottom>pFrame.bottom) result.height = pFrame.bottom - result.y;
		if (result.right>pFrame.right) result.width = pFrame.right - result.x;
		
		result.update();
		return result;
	}
	
	public Frame scaleTo(Frame pFrame) {
		Frame result = null;
		int partialX = 0;
		int partialY = 0;
		int partialWidth = 0;
		int partialHeight = 0;
		float xScale = (float) this.width / pFrame.width;
		float yScale = (float) this.height / pFrame.height;
		if (yScale>xScale) {
			partialX = Math.round(this.x * (1/yScale));
			partialY = Math.round(this.y * (1/yScale));
partialWidth = Math.round(this.width * (1/yScale));			
partialHeight = Math.round(this.height * (1/yScale));
		} else {
			partialX = Math.round(this.x * (1/yScale));
			partialY = Math.round(this.y * (1/yScale));
			partialWidth = Math.round(this.width * (1/yScale));			
			partialHeight = Math.round(this.height * (1/yScale));
		}
		result = new Frame(partialX,partialY,partialWidth,partialHeight);
		return result;
	}
	
	public Frame getProportionalTo(Frame origin, Frame destination) {
		int tx= (destination.width*x)/origin.width; 
		int ty= (destination.height*y)/origin.height;
		int twidth= (destination.width*width)/origin.width;
		int theight= (destination.height*height)/origin.height;
		Frame result = new Frame(tx,ty,twidth,theight);
		return result;
	}
	
	public boolean isDrawable() {
		boolean result = true;
		if (x<0) result = false;
		else if (y<0) result = false;else if (width<0) result = false;
		else if (height<0) result = false;
		
		return result;
	}
	
	
	public void update() {
		top = y;
		bottom = y + height;
		left = x;
		right = x + width;
		centerX = (right-left) / 2;
		centerY = (bottom-top) / 2;
	}
	
	public void update(int pX, int pY, int pWidth, int pHeight) {
		x = pX;
		y = pY;
		width = pWidth;
		height = pHeight;
		this.update();
	}
	
	public String toString() {
		return "("+x+","+y+":"+width+","+height+")";
	}

}
