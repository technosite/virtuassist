package com.technosite.ALog;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ALog {

	public static boolean debug = false;
	public static Context context = null;
	
	public static void setContext(Context ct) {
		context = ct;
	}
	
	public ALog() {
		// TODO Auto-generated constructor stub
		

	}

	public static void showMessage(String message) {
		
		Toast.makeText(context, message,
				 Toast.LENGTH_SHORT).show();		
	}
	
	public static void i(String title, String message) {
		String text = title + ":\n" + message;
if (debug) {
Log.i(title,message);
text = "Information of " +title + ":\n" + message;
ALog.showMessage(text);
}

	}
	

	public static void d(String title, String message) {
		String text = title + ":\n" + message;
if (debug) {
Log.d(title,message);
text = "Debug of " +title + ":\n" + message;
ALog.showMessage(text);
}

	}
	

	public static void e(String title, String message) {
		String text = title + ":\n" + message;
if (debug) {
Log.e(title,message);
text = "Error in " +title + ":\n" + message;
ALog.showMessage(text);
}

	}
	

	public static void w(String title, String message) {
		String text = title + ":\n" + message;
if (debug) {
Log.w(title,message);
text = "Warning from " +title + ":\n" + message;
ALog.showMessage(text);
}

	}
	
}
