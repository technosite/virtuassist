package com.technosite.model;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import com.technosite.virtuassist.Globals;
import com.technosite.virtuassist.Persistence;
import com.technosite.virtuassist.DBManager.DataManager;

public class PDTManager {

	private static PDTManager _instance = null; 
	
	public PDTManager() {
		// TODO Auto-generated constructor stub
	}
	
	public static PDTManager getSharedInstance() {
		if (_instance == null) {
			_instance = new PDTManager();
			_instance.createTypeTables();
		}
		return _instance;
	}
	
	
	// ** Attributes
	
	public PDTDevice device = null;
	public PDTSurface surface = null;
	public PDTPath path = null;
	public PDTStep step = null;

	
	// ** Types
	
	public PDTTypes typeUser = null;
	public PDTTypes typeDevice = null;
	public PDTTypes typeSurface = null;
	public PDTTypes typeFigure = null;
	public PDTTypes typeControl = null;
	public PDTTypes typeStep = null;
	
	private void createTypeTables() {
		DataManager dm = DataManager.getSharedInstance();
		typeUser = dm.loadTypeTable("typeusers");
		typeDevice = dm.loadTypeTable("categories");
		typeSurface = dm.loadTypeTable("typesurfaces");
		typeFigure = dm.loadTypeTable("typefigures");
		typeControl = dm.loadTypeTable("typecontrols");
		typeStep = dm.loadTypeTable("typesteps");
	}
	
	// ** Global functions

	public static boolean isStoredDevice(String code) {
		boolean result = false;
Globals globals = Globals.getSharedInstance();
String lastDevice = globals.getValueForKey(Persistence.kLastDevice, null);

if (lastDevice!=null && lastDevice.equalsIgnoreCase(code)) result = true;

// debug
result = false;
		return result;
	}
	
	public static String parseQrCode(String textCode) {
		String result = null;
		String code = textCode.trim();
		String r = code.substring(0,4);
		if (!r.equalsIgnoreCase("Pdt-")) return null;
		
				String number = code.substring(4);
				int n;
				try {
					n= Integer.valueOf(number);
				} catch (Exception e) { return null; }
				
				if (n>=0) result = "Pdt-" + n ;
				else return null;
				
		return result;
	}
	
	// ** Constants
	
	public static int CONTROLPANEL = 19;
	public static int FIGURETEXT = 6;
	public static int FIGUREIMAGE = 7;
}
