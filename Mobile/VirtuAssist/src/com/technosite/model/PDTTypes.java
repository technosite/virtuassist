package com.technosite.model;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import java.util.HashMap;

import com.technosite.translator.TranslatorManager;
import com.technosite.virtuassist.Globals;


public class PDTTypes {

public String name = null;	

	public PDTTypes(String nameTable) {
		this.name = nameTable;
		data = new HashMap<String,String>();
	}

	
	private HashMap<String,String> data = null;
	

	// ** Method to manage values and indexes
	
	public void add(int index, String value) {
		data.put(String.valueOf(index), value);
	}
	
	public int getIndexForValue(String value) {
int result = -1;		
for (int i=1;i<=data.size();i++) {
if (value.equals(getValueForIndex(i))) {
result = i;
break;
}
}
return result;		
	}
	
	public String getValueForIndex(int index) {
		String key = data.get(String.valueOf(index));
		String result = TranslatorManager.getSharedInstance(Globals.context).get(key);
		if (result==null) result = key;
		return result; 
	}
	
}
