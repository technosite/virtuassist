package com.technosite.model;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

public class PDTPersistence {

	// Geography constants

	public static final int kpos_NONE = 0;
public static final int kpos_TOP = 2;
public static final int kpos_TOPRIGHT = 3;
public static final int kpos_RIGHT = 6;
public static final int kpos_BOTTOMRIGHT = 9;
public static final int kpos_BOTTOM = 8;
public static final int kpos_BOTTOMLEFT = 7;
public static final int kpos_LEFT = 4;
public static final int kpos_TOPLEFT = 1;
public static final int kpos_CENTER = 5;
}
