package com.technosite.model;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import java.io.Serializable;

public class PDTStep implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;
	
	public int id = 0;
	public int position = 0;
	public int type = 0;
	public String instruction = null;
	public PDTPath path = null;
	public int idControl = 0;
	public PDTControl control = null;
	public Object image = null;
	

	public PDTStep(int pId,int pos, int pType, String text,int pIdControl, Object pImage, PDTPath pPath) {
		// TODO Auto-generated constructor stub
		this.position = pos;
		this.instruction = text;
		this.type = pType;
		this.idControl = pIdControl;
		this.control = null;
		this.image = pImage;
		this.path = pPath;
	}
	
	public void update() {
		
	}

}
