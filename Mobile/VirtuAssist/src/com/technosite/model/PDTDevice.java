package com.technosite.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import com.technosite.virtuassist.Globals;
import com.technosite.virtuassist.Persistence;

public class PDTDevice  implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;

	public int id = 0;
	public String name = null;
	public String description= null;
	public int category = 0;
	public String qrCode= null;


	public PDTDevice() {
		// TODO Auto-generated constructor stub
		surfaces = new ArrayList<PDTSurface>();
		paths = new ArrayList<PDTPath>();
	}

	public PDTDevice(int pId, String pName, String pDescription, int pCategory, String pqrCode) {
		surfaces = new ArrayList<PDTSurface>();
		paths = new ArrayList<PDTPath>();
		this.id = pId;
		this.qrCode = pqrCode;
		this.name = pName;
		this.category = pCategory;
		this.description = pDescription;
	}



	// ** Methos to manage surfaces

	public ArrayList<PDTSurface> surfaces = null;

	public int getNumberOfSurfaces() {
		return surfaces.size();
	}

	public void addSurfaces(ArrayList<PDTSurface> list) {
for (int i=0;i<list.size();i++) {
	this.addSurface(list.get(i));
}
	}

	public void addSurface(PDTSurface pSurface) {
		pSurface.device = this;
		this.surfaces.add(pSurface);
	}

	public void removeSurface(PDTSurface pSurface) {
		this.surfaces.remove(pSurface);
	}

	public PDTSurface getSurface(int index) {
		return surfaces.get(index);
	}
	
	public PDTSurface getSurfaceById(int id) {
		PDTSurface r = null;
		PDTSurface t = null;
		for (int i=0;i<surfaces.size();i++) {
t = surfaces.get(i);
if (t.id == id) {
	r = t;
	break;	
} 
		}
		return r;
	}

	// ** Methos to manage paths

		public ArrayList<PDTPath> paths = null;

		public int getNumberOfPaths() {
			return paths.size();
		}

		public void addPaths(ArrayList<PDTPath> list) {
for (int i = 0;i<list.size();i++) {
	this.addPath(list.get(i));
}
		}

		public void addPath(PDTPath pPath) {
			pPath.device = this;
			this.paths.add(pPath);
		}

		public void removePath(PDTPath pPath) {
			this.paths.remove(pPath);
		}

		public PDTPath getPath(int index) {
			return paths.get(index);
		}

		// ** Methods to manage controls

		public PDTControl findControlById(int idValue) {
			PDTControl result = null;
			for (int iSurface = 0;iSurface<surfaces.size();iSurface++) {
PDTSurface surf = surfaces.get(iSurface);
result = surf.findControlById(idValue);
if (result != null) break;
			}
			return result;
		}

		// ** Methods to manage the device

		public void update() {
// Manage surfaces
			if (surfaces.size()>0) for (int i=0;i<surfaces.size();i++) {
PDTSurface tmp = surfaces.get(i);
tmp.update();
			}
			if (paths.size()>0) for (int i=0;i<paths.size();i++) {
PDTPath tmp = paths.get(i);
tmp.update();
			}
		}

	// ** Persistence


		public String serializedString() {
			String result = null;
			try {
				ByteArrayOutputStream bo = new ByteArrayOutputStream();
				ObjectOutputStream so = new ObjectOutputStream(bo);
				so.writeObject(this);
				so.flush();
				result = bo.toString("ISO-8859-1");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		public static PDTDevice loadFromDevice() {
			PDTDevice result = null;
			Globals globals = Globals.getSharedInstance();
						String data = globals.getValueForKey(Persistence.kDataOfStoredDevice, null);
						try {
							byte b[] = data.getBytes("ISO-8859-1");
							ByteArrayInputStream bi = new ByteArrayInputStream(b);
							ObjectInputStream si = new ObjectInputStream(bi);
							result = PDTDevice.class.cast(si.readObject());
						} catch (Exception e) {
							e.printStackTrace();
						}

return result;
		}

		public void saveToDevice() {
			Globals globals = Globals.getSharedInstance();
			globals.setValueForKey(Persistence.kLastDevice,qrCode);
			String data = serializedString();
			globals.setValueForKey(Persistence.kDataOfStoredDevice,data);

		}

}
