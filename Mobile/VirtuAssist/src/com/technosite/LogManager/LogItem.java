package com.technosite.LogManager;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class LogItem {

	public LogItem(String pUser, String pTask, Date pDate) {
		user = pUser;
		task = pTask;
		date = pDate;
		
	}
	
	public String user = null;
	public String task = null;
	public String notes = null;
	public Date date = null;
	public long startTime = 0;
	public long stopTime = 0;
	public boolean precision = false;
	
	public String toString() {
		DateFormat df;
		if (precision) df= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SS");
		else df= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String stringDate = df.format(date);
		return user + ": " + task + "(" + stringDate + ")";
	}
	
	public void start() {
		startTime = System.currentTimeMillis(); 
	}
	
	public void stop() {
		stopTime = System.currentTimeMillis();
	}
	
	public long getResultTimeInMillis() {
		return stopTime - startTime;
	}

}
