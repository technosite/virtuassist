/*
 *
 * LogManager
 * 
 * Class to manage log resulst for user tests
 * 
 * How to use this class:
 * 1. Set the user for the test
 * 2. Add tasks for the log
 * 3. Export the result to a file
 * 
 * You can use this class as a singleton:
 * LogManager.setUser("user name");
 * LogManager.addTask("Text for a task");
 * 
 */
package com.technosite.LogManager;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.technosite.virtuassist.Globals;
import com.technosite.virtuassist.PDTBluetoothService;

public class LogManager {

	public static final String SYSTEM_USER = "SYSTEM";
	public static final String VISION_USER = "OpenCV";
	private static LogManager _instance = null;
	public ArrayList<LogItem> items = null;
	public String currentUser = null;

	public static LogManager getSharedInstance() {
		if (_instance==null) _instance = new LogManager();
		return _instance;
	}
	
	public LogManager() {
		items = new ArrayList<LogItem>();
		remoteItems = new ArrayList<String>();
	}
	
	public static void setUser(String pUser) {
		if (_instance==null) LogManager.getSharedInstance();
		_instance.currentUser = pUser;
	}
	
	public static void resetLog() {
		if (_instance==null) _instance = LogManager.getSharedInstance();
		_instance.items.clear();
		_instance.remoteItems.clear();
			}
	
	public static void addUserTask(String pTask) {
		if (_instance==null) LogManager.getSharedInstance();
		if (_instance.currentUser==null) {
			Log.w("LogManager", "Current user is not defined");
			return;
		}
		LogItem item = new LogItem(_instance.currentUser, pTask, new Date());
_instance.items.add(item);		
	}
	
	public static void addSystemTask(String pTask) {
		if (_instance==null) LogManager.getSharedInstance();
				LogItem item = new LogItem(SYSTEM_USER, pTask, new Date());
_instance.items.add(item);		
	}

	public static void addVisionTask(String pTask) {
		if (_instance==null) LogManager.getSharedInstance();
				LogItem item = new LogItem(VISION_USER, pTask, new Date());
				item.precision = true;
_instance.items.add(item);		
	}
	public static void exportToFile(String fileName) {
		if (_instance==null) LogManager.getSharedInstance();
		String filenameWithPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName;
		// Save the file
		File logFile=new File(filenameWithPath);
		
		try {
			OutputStreamWriter fos= new OutputStreamWriter(new FileOutputStream(logFile));
			for (int i=0;i<_instance.items.size();i++) {
				fos.write(_instance.items.get(i).toString() + "\n");
			}
			
			fos.flush();
			fos.close();
			Log.w("LogManager", "Log data saved in " + fileName);
		} catch (java.io.IOException e) {
Log.e("LogManager error", "Error saving the log data to SDCard memory. " +e);					
		}
	}
	
	public static void exportRemoteToEMail(String address, Context context) {
		if (_instance==null) LogManager.getSharedInstance();
		String textResult = "\n\nVirtuAssist results for remote log activity\n\n";
		for (int i=0;i<_instance.remoteItems.size();i++) {
textResult += _instance.remoteItems.get(i).toString() + "\n";			
		}
		textResult += "\n\n\n";
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.putExtra(Intent.EXTRA_EMAIL, address);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[VirtuAssist test results] Results for " + _instance.currentUser);
        emailIntent.putExtra(Intent.EXTRA_TEXT, textResult);
        emailIntent.setType("message/rfc822");
        context.startActivity(Intent.createChooser(emailIntent, "Email "));
		
	}
	
	public static void exportToEMail(String address, Context context) {
		if (_instance==null) LogManager.getSharedInstance();
		String textResult = "\n\nVirtuAssist results for " + _instance.currentUser + "\n\n";
		for (int i=0;i<_instance.items.size();i++) {
textResult += _instance.items.get(i).toString() + "\n";			
		}
		textResult += "\n\n\n";
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.putExtra(Intent.EXTRA_EMAIL, address);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[VirtuAssist test results] Results for " + _instance.currentUser);
        emailIntent.putExtra(Intent.EXTRA_TEXT, textResult);
        emailIntent.setType("message/rfc822");
        context.startActivity(Intent.createChooser(emailIntent, "Email "));
		
	}

	 
	// ** Remote log management
	
	public ArrayList<String> remoteItems;
	
	public void addRemoteItem(String textItem) {
		remoteItems.add(textItem);
	}

private boolean sendingLog = false;	
	public void sendLogToRemoteDevice() {
		if (sendingLog ) return;
		sendingLog = true;
		PDTBluetoothService serv = PDTBluetoothService.getSharedInstance(Globals.context);
		for (int i=0;i<items.size();i++) {
			LogItem item = items.get(i);
			String itemString = i + " " +item.toString() + "("+ items.size() + ")";
serv.sendActivityLog(itemString);			
		}
		sendingLog = false; 
			}
	



}
