package com.technosite.BluetoothCommunicator;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */
public class BluetoothCommunicator {

	public interface BluetoothConnectionInterface {
	
		public void bluetoothDeviceConnecting();
		public void bluetoothDeviceConnected();
		public void bluetoothDeviceDisconnected();
		
		public void bluetoothDeviceDataReceived(String data);
	}
	
	public BluetoothConnectionInterface  delegate = null;
	
	// ** Singleton
	
	private static BluetoothCommunicator _instance = null;
	
	public static BluetoothCommunicator getSharedInstance() {
		return _instance;
	}
	
	public static BluetoothCommunicator getSharedInstance(Context ct) {
		if (_instance == null) _instance = new BluetoothCommunicator(ct);
		return _instance;
	}
	
	
	private Context context = null;
	private BluetoothCommunicatorService mBluetoothService = null;
	
	private static BluetoothAdapter mBluetoothAdapter = null;
	
    // Debugging
    private static final String TAG = "BluetoothCommunicator";
    

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    
	
	public BluetoothCommunicator(Context ct) {
		context = ct;
		initService();
	}
	
	// ** Bluetooth management
	
	public void activeBluetoothDiscoverableMode() {
        Log.d(TAG, "Checking if it is discoverable device");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            context.startActivity(discoverableIntent);
        }
    }
	
	public static boolean isConnected = false;
	public static boolean isAvailable = false;
	
	private String mConnectedDeviceName  = null;
	
	public static String getDeviceNameForConnection() {
String result = "";
if (mBluetoothAdapter !=null) {
	result = mBluetoothAdapter .getAddress();
}
return result;
	}
	
	public String getConnectedDeviceName() {
		return mConnectedDeviceName; 
	}
	
	public static boolean isAvailableService() {
		boolean result = false;
		if (mBluetoothAdapter==null) mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter!=null) {
			if (mBluetoothAdapter.isEnabled()) result = true;
		}
		
		return result;
	}
	
	private void initService() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter !=null) {
			  mBluetoothService = new BluetoothCommunicatorService(context, mHandler);
			if ( mBluetoothService  != null) {
				BluetoothCommunicator.isAvailable = true;

			} else Log.e("*Bluetooth service*", "Bluetooth service is not available");
			
		}

	}
	
    // ** Bluetooth protocol management

    public void chooseDevice(Activity activity, int requestCode) {
    	Intent i= new Intent(activity, DeviceListActivity.class);
        activity.startActivityForResult(i, requestCode);    	
    }

    
    public void disconnectDevice() {
    	if (BluetoothCommunicator.isConnected) {
    		mBluetoothService.stop();
        	isConnected = false;
        	if (delegate!=null) delegate.bluetoothDeviceDisconnected();
    	}
    }
    
    public void connectDevice(Intent data, boolean secure) {
    	if (BluetoothCommunicator.isAvailable ) {
    		// Get the device MAC address
            String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
            // Get the BluetoothDevice object
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
            // Attempt to connect to the device
             mBluetoothService.connect(device, secure);    		
    	}
    }
    
    public void connectDevice(String address , boolean secure) {
    	if (BluetoothCommunicator.isAvailable ) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
            // Attempt to connect to the device
             mBluetoothService.connect(device, secure);    		
    	}
    }
    
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	Log.w("*Bluetooth handler", "managing event");
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
            	Log.w(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothCommunicatorService.STATE_CONNECTED:
                	isConnected = true;
                    if (delegate!=null) delegate.bluetoothDeviceConnected();
                    break;
                case BluetoothCommunicatorService.STATE_CONNECTING:
                	isConnected = false;
                	if (delegate!=null) delegate.bluetoothDeviceConnecting();
                    break;
                case BluetoothCommunicatorService.STATE_LISTEN:
                	                case BluetoothCommunicatorService.STATE_NONE:
                	                	isConnected = false;
                	if (delegate!=null) delegate.bluetoothDeviceDisconnected();
                    break;
                }
                break;
            case MESSAGE_WRITE:
                // byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                // String writeMessage = new String(writeBuf);
                // mConversationArrayAdapter.add("Me:  " + writeMessage);
                break;
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String data= new String(readBuf, 0, msg.arg1);
                if (delegate!=null) delegate.bluetoothDeviceDataReceived(data);
  
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                // Toast.makeText(context, "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                // Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               // Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };

    
    public boolean sendString(String message) {
        // Check that we're actually connected before trying anything
        if (mBluetoothService.getState() != BluetoothCommunicatorService.STATE_CONNECTED) {
        	Log.e(TAG, "Error sending data because bluetooth service is not available");
            return false;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mBluetoothService.write(send);

            
            
            return true;
        } else {
        	return false;
        }
    }
    
    
       
    
    
}