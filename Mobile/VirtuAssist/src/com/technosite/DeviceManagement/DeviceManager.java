package com.technosite.DeviceManagement;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class DeviceManager {

	private static DeviceManager _instance = null;
	
	public static DeviceManager getSharedInstance(Context ct) {
if (_instance == null) _instance = new DeviceManager(ct);
return _instance;
	}
	
	private Context context = null;
	
	public void update() {
		updateConnections();		
	}
	
	
	public DeviceManager(Context ct) {
		// Features initialization
		context = ct;
		initConnectionManagement();
	}
	
	// ** Connection management
	
	public String connectionType = null;
	public int speedBandwidth = -1;
	private WifiManager wifiManager = null;
	
	
	
	private void initConnectionManagement() {
		wifiManager = (WifiManager ) context.getSystemService(Context.WIFI_SERVICE);		
	}
	
	public void updateConnections() {
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		if (wifiInfo != null) {
		    Integer linkSpeed = wifiInfo.getLinkSpeed(); //measured using WifiInfo.LINK_SPEED_UNITS
		    speedBandwidth  = (int) linkSpeed ; 
		    connectionType  = "WIFI";
		}
	}
	
	

}
