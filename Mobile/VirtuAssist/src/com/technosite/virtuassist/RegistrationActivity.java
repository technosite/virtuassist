package com.technosite.virtuassist;
/*
 * © Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an ÒAS ISÓ BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.technosite.ALog.ALog;
import com.technosite.virtuassist.DBManager.DataManager;

public class RegistrationActivity extends Activity {
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				setContentView(R.layout.activity_registration);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.login, menu);
		return false;
	}

	// ** Interface management

	public void register(View view) {
		EditText txtUser = (EditText) findViewById(R.id.userField);
		EditText txtPassword = (EditText) findViewById(R.id.passwordField);
		
		String user = txtUser.getText().toString();
		String password = txtPassword.getText().toString();
		
		if (user.equals("") || password.equals("")) {
			String message = getResources().getString(R.string.error_empty_fields_message);
			Toast.makeText(this, message,
					 Toast.LENGTH_SHORT).show();			
		} else if (isNameAvailable(user)) {
			if (recordUser(user,password, "", true, true, true)) {
				Globals globals = Globals.getSharedInstance();
globals.logInUser(user, true, true, true);				
this.finish();

			} else ALog.e("Error", "Error registrando usuario");
								
		}			
	}
	
	// ** DataBase functions
		
	private boolean isNameAvailable(String userName) {
DataManager data = DataManager.getSharedInstance();		
		return data.checkUserNameAvailability(userName);
	}
	
	private boolean recordUser(String userName,String userPassword, String userMail, boolean uText, boolean uImage, boolean uVideo) {
		int cText = 0;
		int cVideo = 0;
		int cImage = 0;
		if (uText) cText = 1;
		if (uVideo) cVideo = 1;
		if (uImage) cImage= 1;
DataManager data = DataManager.getSharedInstance();
return data.saveNewUser(userName,userPassword,userMail,cText, cImage, cVideo);
	}
	
	
	
	
}
