package com.technosite.virtuassist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.technosite.BluetoothCommunicator.BluetoothCommunicator;
import com.technosite.BluetoothCommunicator.BluetoothCommunicator.BluetoothConnectionInterface;
import com.technosite.LogManager.LogManager;

public class PDTBluetoothService implements BluetoothConnectionInterface {

	public static final String idDevice = "S";
	
	public interface PDTBluetoothInterface {
		
		public void receivedLoginDataFromBluetooth(String data);
		public void receivedLoginStatusFromBluetooth(boolean status);
		public void receivedDataFromBluetooth(String data);
		public void changeStatusConnection(boolean connected);
	}
	
	private static Context context = null;
	private static PDTBluetoothService  _instance = null;
	public PDTBluetoothInterface delegate= null;
	private Globals globals = null;
	public BluetoothCommunicator bluetoothDriver = null;

    public PDTBluetoothService(Context ct) 
    {
    	context = ct;
                globals = Globals.getSharedInstance();
            	Log.w("*PDTBluetoothService*", "Initializing service");
            	bluetoothDriver = BluetoothCommunicator.getSharedInstance(context);
            	bluetoothDriver.delegate = this;
    }
    
    public static PDTBluetoothService getSharedInstance(Context ct) {
    	if (_instance == null) _instance = new PDTBluetoothService(ct);
    	return _instance;
    }
    
    // ** Main methods
    
    public boolean connect() {
    	String address = globals.BluetoothDeviceID;
    	if (address!=null) {
    		bluetoothDriver.connectDevice(address, false);    		
    		return true;
    	} else return false;
    }
    
    public boolean connect(String address) {
    	    	if (address!=null) {
    		bluetoothDriver.connectDevice(address, false);    		
    		return true;
    	} else return false;
    }
    
    public void disconnect() {
    	bluetoothDriver.disconnectDevice();    	
    }
    
    public void send(String data) {
    	bluetoothDriver.sendString(data);
    }
    
    public void chooseDevice(Activity activity, int requestCode) {
bluetoothDriver.chooseDevice(activity, requestCode);    	
    }

    
    public boolean isHardwareAvailable() {
    	boolean result = false;
    	if (bluetoothDriver.isAvailable) result = true;
    	return result;
    }
    // ** BluetoothConnection interface


	public void bluetoothDeviceConnecting() {
		Log.w("*Delegate*", "Connecting");
	}
	
	public void bluetoothDeviceConnected() {
		Log.w("*Delegate*", "Connected");
		if (delegate!=null) delegate.changeStatusConnection(true);
	}
	
	public void bluetoothDeviceDisconnected() {
		Log.w("*Delegate*", "Disconnected");
		if (delegate!=null) delegate.changeStatusConnection(false);
	}
	
	public void bluetoothDeviceDataReceived(String data) {

Log.w("*Delegate*", "Data received " + data);
parseData(data);
	}
	
	
	// ** Parse management

	private void sendToDelegate(String data) {
		if (delegate!=null) delegate.receivedDataFromBluetooth(data);		
	}
	
	
	
// ** PDT Protocol management
    
    public final String kPDTCOM_ROL_QUERY = "Q";
    public final String kPDTCOM_ROL_ANSWER = "A";
    public final String kPDTCOM_ROL_ERROR = "E";
    public final String kPDTCOM_ROL_OK = "O";
    public final String kPDTCOM_ROL_DOCOMMAND = "D";
    
    public final String kPDTCOM_ACTION_NONE = "N";
    public final String kPDTCOM_ACTION_QR = "Q";
    public final String kPDTCOM_ACTION_LOGIN = "L";
    public final String kPDTCOM_ACTION_REGISTER = "R";
    public final String kPDTCOM_ACTION_ACTIVITYLOG = "A";
    
    public final String kPDTCOM_FORMAT_TEXT = "T";
    public final String kPDTCOM_FORMAT_IMAGE = "I";
    public final String kPDTCOM_FORMAT_ARRAY = "R";
    
    public String getRol(String data) {
    	return String.valueOf(data.charAt(1));
    }
    
    public String getAction(String data) {
    	return String.valueOf(data.charAt(2));
    }
    
    public String getFormat(String data) {
    	return String.valueOf(data.charAt(3));
    }
    
    public String getBody(String data) {
    	if (data.length() <=4) return null;
    	else return data.substring(4);
    }
    
    public String getHeader(String rol, String action, String format) {
return idDevice + rol + action + format;
    }
 
 // ** Events **

    private void parseData(String data) {
		if (kPDTCOM_ROL_DOCOMMAND.equals(getRol(data))) {
			if (kPDTCOM_ACTION_LOGIN.equals(getAction(data))) {
				this.receiveLogin(getBody(data));			
			}			
		}
				if (kPDTCOM_ROL_QUERY.equals(getRol(data))) {
					if (kPDTCOM_ACTION_LOGIN.equals(getAction(data))) this.responseForLogin();
			if (kPDTCOM_ACTION_ACTIVITYLOG.equals(getAction(data))) {
				String command= getBody(data);
				LogManager lm = LogManager.getSharedInstance();
				if (command.equalsIgnoreCase("send")) {
					lm.sendLogToRemoteDevice();
				} else {
					lm.resetLog();
				}


			}
		}
		if (kPDTCOM_ROL_ANSWER.equals(getRol(data))) {
			if (kPDTCOM_ACTION_LOGIN.equals(getAction(data))) {
				String result = getBody(data);
				if (delegate!=null) {
if (result.equalsIgnoreCase("YES")) delegate.receivedLoginStatusFromBluetooth(true);					
else delegate.receivedLoginStatusFromBluetooth(false);
				}
				
			}
			if (kPDTCOM_ACTION_ACTIVITYLOG.equals(getAction(data))) {
				this.receiveActivityLog(getBody(data));
			}
		}
		
	}
    
    private void receiveActivityLog(String data) {
LogManager lm = LogManager.getSharedInstance();
lm.addRemoteItem(data);
    }
    
    public void sendActivityLog(String data) {
String message = getHeader(kPDTCOM_ROL_ANSWER, kPDTCOM_ACTION_ACTIVITYLOG , kPDTCOM_FORMAT_TEXT);
if (data!=null) message = message + data;
bluetoothDriver.sendString(message);
    }
    
    public void askForActivityLog() {
String message = getHeader(kPDTCOM_ROL_QUERY, kPDTCOM_ACTION_ACTIVITYLOG , kPDTCOM_FORMAT_TEXT);
message = message + "send";
bluetoothDriver.sendString(message);
    }
    
    public void askForResetActivityLog() {
String message = getHeader(kPDTCOM_ROL_QUERY, kPDTCOM_ACTION_ACTIVITYLOG , kPDTCOM_FORMAT_TEXT);
message = message + "reset";
bluetoothDriver.sendString(message);
    }
    
    private void receiveLogin(String user) {
    	if (delegate!=null) delegate.receivedLoginDataFromBluetooth(user);
    }
    
    public void sendLogin(String user) {
String message = getHeader(kPDTCOM_ROL_DOCOMMAND, kPDTCOM_ACTION_LOGIN , kPDTCOM_FORMAT_TEXT);
if (user!=null) message = message + user;
bluetoothDriver.sendString(message);
    }
    
    public void askForLogin(String user) {
String message = getHeader(kPDTCOM_ROL_QUERY, kPDTCOM_ACTION_LOGIN , kPDTCOM_FORMAT_TEXT);
bluetoothDriver.sendString(message);
    }
    
    public void responseForLogin() {
String message = getHeader(kPDTCOM_ROL_ANSWER, kPDTCOM_ACTION_LOGIN , kPDTCOM_FORMAT_TEXT);
if (globals.isLogin) message = message + "YES";
else message = message + "NO";
bluetoothDriver.sendString(message);
    }
    
	public void initApplication() {
		Intent i = new Intent(context, MainActivity.class);
		context.startActivity(i);
	}
    

}
