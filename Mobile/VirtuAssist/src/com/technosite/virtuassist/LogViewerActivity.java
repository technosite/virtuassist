package com.technosite.virtuassist;



import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technosite.LogManager.LogManager;

public class LogViewerActivity extends Activity {

	private LinearLayout table = null;
	private LogManager lm = null;
	private PDTBluetoothService bluetoothService = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lm = LogManager.getSharedInstance();
		bluetoothService  = PDTBluetoothService.getSharedInstance(this);
		setContentView(R.layout.activity_log_viewer);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		table = (LinearLayout) findViewById(R.id.logTable);
		loadTable();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// without menu
		// getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}
	
    @Override
    public void onPause()
    {
        super.onPause();
        keepUpdating = false;
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        keepUpdating = true;
        startUpdating();
    }
	private int loaded = 0;
	public void loadTable() {
		bluetoothService .askForActivityLog();		
		Log.w("LogViewerActivity", "Updating table");
if (lm.remoteItems.size()>0) {
	int c = 0;
		for (int i=loaded ;i<lm.remoteItems.size();i++) {
			String item = lm.remoteItems.get(i);
			addItem(""+ (i+1) + ". " +item);
			c++;
		}
		loaded = loaded+c; 
} 

	}
	
	private void addItem(String text) {
TextView t = new TextView(this);
t.setText(text);
table.addView(t);
	}
	
	
	public void reset(View view) {
		bluetoothService .askForResetActivityLog();		
	}
	public void export(View view) {
LogManager.exportRemoteToEMail("jchacon@technosite.es", this);		
	}
	
	// ** Updating timer
	
	public static boolean keepUpdating = false;
	public void updateAgain() {
		if (keepUpdating ) startUpdating();		
	}
	
    public void startUpdating() {
    	Timer timer = new Timer();
    	TimerTask task = new TimerTask()
    	{
    		@Override
    		public void run()
    		{
    					runOnUiThread(new Runnable()
    {
    @Override
    	public void run()
    	{
    	loadTable();
    	updateAgain();
    	    	} 
    	});
    				}
    		};
    		
    	timer.schedule(task, 5000);
    }
    

	
	
}
