package com.technosite.virtuassist;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class Globals {
	public static final String SDCardPath = Environment.getExternalStorageDirectory().getAbsolutePath(); // "file:///sdcard/";
	private static Globals _instance = null;
	public static Context context = null;
	public Globals(Context ct) {
		// TODO Auto-generated constructor stub
		context = ct;
				this.init(); 
	}
	
	public static Globals getSharedInstance(Context ct) {
		if (_instance == null) _instance = new Globals(ct);
		return _instance;
	}
	
	public static Globals getSharedInstance() {
		return _instance;		
	}
	
	public static Context getContext() {
		return Globals.context;
	}

	
	// ** globalattributes
public boolean isDebugMode = true;	
	public boolean isLogin = false;
	

	
	public String getValueForKey(String key, String defaultValue) {
		SharedPreferences prefs = context.getSharedPreferences("AccesibilizadorPDT", Context.MODE_PRIVATE);
return prefs.getString(key, defaultValue);		
	}
	
	public void setValueForKey(String key, String value) {
		SharedPreferences prefs = context.getSharedPreferences("AccesibilizadorPDT", Context.MODE_PRIVATE);		
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	public void logInUser(String user, boolean pText, boolean pImage, boolean pVideo) {
		Log.w("Globals", "logInUser");
		isLogin = true;
		setValueForKey(Persistence.kUser, user);
		setValueForKey(Persistence.kTextPreference , String.valueOf(pText));
		setValueForKey(Persistence.kImagePreference , String.valueOf(pImage));
		setValueForKey(Persistence.kVideoPreference , String.valueOf(pVideo));
		setValueForKey(Persistence.kSpeechPreference , String.valueOf(false));
		
				
	}
	
	public void logout() {
setValueForKey(Persistence.kUser,null);
this.isLogin = false;
	}
	
	
	public void init() {
		BluetoothDeviceID = getValueForKey(Persistence.kPairedDevice, null);
if (getValueForKey(Persistence.kUser,null) == null) {
	this.isLogin = false;
	setValueForKey(Persistence.kTextPreference , String.valueOf("true"));
setValueForKey(Persistence.kImagePreference , String.valueOf(false));
setValueForKey(Persistence.kVideoPreference , String.valueOf(false));		
setValueForKey(Persistence.kSpeechPreference , String.valueOf(false));
} else {
	this.isLogin = true;
}
calculateScreenSize();
	}
	
	// ** Bluetooth device management
	
	public String BluetoothDeviceID = null;
	
	public void saveBluetoothDeviceID(String id) {
		BluetoothDeviceID = id;
		setValueForKey(Persistence.kPairedDevice, id);
	}
	
	// ** Screen management
	
	public int screenWidth = 0;
	public int screenHeight = 0;
	
	@SuppressLint("NewApi")
	public void calculateScreenSize() {
    	WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	if (Build.VERSION.SDK_INT<=16) {
    		Point size = new Point();
    		display.getSize(size);
    		this.screenWidth = size.x;
    		this.screenHeight = size.y;
    	} {
    		DisplayMetrics displayMetrics = new DisplayMetrics();
        	display.getRealMetrics(displayMetrics);
        	this.screenWidth = displayMetrics.widthPixels;
        	this.screenHeight = displayMetrics.heightPixels;
    	}
    }

	
}
