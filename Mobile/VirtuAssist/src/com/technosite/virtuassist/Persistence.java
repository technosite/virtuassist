package com.technosite.virtuassist;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

public class Persistence {

	/*
	 * Address devices
	 Nexus 5: CC:FA:00:6F:FF:79
Galaxy nexus: 9C:02:98:F8:69:85
GoogleGlass: F8:8F:CA:11:18:DD
	 */
	public static final String kUser = "user";

	// ** Preferences
	public static final String kTextPreference = "prefText";
	public static final String kImagePreference = "prefImage";
	public static final String kVideoPreference = "prefVideo";
	public static final String kSpeechPreference = "prefSpeech";
	
	public static final String kPairedDevice= "BTDeviceID";
	
	
	public static final String kInfoSteps = "infoAboutSteps";
	public static final String kInfoSurfaces = "infoAboutSurfaces";
	
	public static final String kLastDevice = "lastDevice";
	public static final String kDataOfStoredDevice = "binaryArrayOfDevice";

}
