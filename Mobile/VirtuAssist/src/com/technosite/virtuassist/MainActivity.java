package com.technosite.virtuassist; 
/*
 * © Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an ÒAS ISÓ BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.technosite.ALog.ALog;
import com.technosite.BluetoothCommunicator.BluetoothCommunicator;
import com.technosite.BluetoothCommunicator.DeviceListActivity;
import com.technosite.TTS.TTS;
import com.technosite.model.PDTManager;
import com.technosite.virtuassist.PDTBluetoothService.PDTBluetoothInterface;

public class MainActivity extends Activity implements PDTBluetoothInterface {

	
	
	private Globals globals = null;
	private TTS tts = null;
	private PDTBluetoothService bluetoothService = null;
	private Button btnConnect = null;
	private Button btnLogin= null;
	private boolean loginRemoteStatus = false;
	
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PDTManager.getSharedInstance();
tts = TTS.getSharedInstance(this);
bluetoothService  = PDTBluetoothService.getSharedInstance(this);
bluetoothService .delegate = this;

		ALog.setContext(this);
	ALog.debug = true;
		globals = Globals.getSharedInstance(this);
		globals.isDebugMode = false;
		loginRemoteStatus  = globals.isLogin;
		
		
		// if (BluetoothCommunicator.isAvailableService() ) bluetoothService.connect(globals.BluetoothDeviceID);
		Log.w("Bluetooth pairing", "This app is paired with " + globals.BluetoothDeviceID +" and its connection name is " + BluetoothCommunicator.getDeviceNameForConnection());
		checkLoginStatus();
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		btnConnect = (Button) findViewById(R.id.connect_button);
		btnLogin = (Button) findViewById(R.id.login_button);
		if (!BluetoothCommunicator.isAvailableService() ) {
			btnConnect .setEnabled(false);
			btnLogin.setEnabled(false);
		} else {
			btnConnect .setEnabled(true);
			btnLogin.setEnabled(true);			
		}
		
		if (globals.isLogin) btnLogin.setText(R.string.logout_button);
		else btnLogin.setText(R.string.login_button);

				
	}
    @Override
    public void onResume()
    {
        super.onResume();
        if (!BluetoothCommunicator.isAvailableService() ) {
			btnConnect .setEnabled(false);
			btnLogin.setEnabled(false);
		} else {
			btnConnect .setEnabled(true);
			btnLogin.setEnabled(true);			
		}
		
		
		
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// without menu
		// getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}

	
	// ** Options
	
	public void connectToDevice(View view) {
		if (bluetoothService .isHardwareAvailable()) {
			Log.w("MainActivity", "Hardware available");
			if (BluetoothCommunicator.isConnected) stopBluetoothConnection();
			else startBluetoothConnection();			
		} else {
String message = getResources().getString(R.string.BTOff);
Toast.makeText(this, message, 
		 Toast.LENGTH_SHORT).show();
		}
		
				
	}
	
	public void doLogin(View view) {
		if (checkLoginStatus()) {
		if (loginRemoteStatus==false) {
			btnLogin.setText(R.string.logout_button);
			bluetoothService.sendLogin(globals.getValueForKey(Persistence.kUser, null));
		} else {
			btnLogin.setText(R.string.login_button);
			bluetoothService.sendLogin(null);
			globals.isLogin = false;
		}
		bluetoothService.askForLogin(null);
		}
	}

	public void showActivityLog(View view) {
		bluetoothService .askForActivityLog();		
		Intent i = new Intent(this, LogViewerActivity.class);
		startActivity(i);		
	}

	public void showAboutInfo(View view) {
// Show the interface with information about the app
				Intent i = new Intent(this, AboutActivity.class);
		startActivity(i);
					}

	// ** Login management 
  	
	public boolean checkLoginStatus() {
		if (globals.isLogin == false) {
			Intent i = new Intent(this, LoginActivity .class);
			startActivityForResult(i, 0);
			return false;
		}
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode==100) {
	    	String address = null;
			if (data!=null) address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
			if (address!=null) {
				Log.w("*Bluetooth*" , "De vuelta de seleccionar el dispositivo " + address );
				globals.saveBluetoothDeviceID(address);
				bluetoothService.connect(); 				
			}
			
		
		} else
		if (data != null) { 
			String userName = data.getStringExtra("userName");
			if (userName != null) {
				globals.isLogin = true;
btnLogin.setText(R.string.logout_button);				
							} else {
								finish();
							}
			
		}
	}
	
// ** Bluetooth management
	
	private void stopBluetoothConnection() {
		Log.w("MainActivity","Stop bluetooth connection");
		if (BluetoothCommunicator.isConnected) {
			
			bluetoothService.disconnect();
					}
	}
	
	private void startBluetoothConnection() {
		Log.w("MainActivity","Start bluetooth connection");
		if (!BluetoothCommunicator.isConnected) { 
			if (globals.BluetoothDeviceID == null) {
				bluetoothService .chooseDevice(this, 100);			 
						} else {
							bluetoothService.connect(globals.BluetoothDeviceID);
						}
		}
				
	}
	
	public void receivedDataFromBluetooth(String data) {
tts.speak(data);		
	}

	public void receivedLoginDataFromBluetooth(String data) {
		if (data==null) {
			globals.logout();
			tts.speak("You are logged out");
		} else {
			globals.logInUser(data, true, true, true);
			tts.speak("You are logged in");
		}
	}
	
	public void changeStatusConnection(boolean connected) {
		if (connected) {
			tts.speak("Connected");
			bluetoothService .askForLogin(null);
			btnConnect .setText(R.string.disconnect_button);
			btnLogin.setEnabled(true);
								} else {
									tts.speak("Disconnected"); 
						btnConnect .setText(R.string.connect_button);
						btnLogin.setEnabled(false);
								}
	}
	
	public void receivedLoginStatusFromBluetooth(boolean status) {
		loginRemoteStatus = status;
		if (status) {
			btnLogin.setText(R.string.logout_button);
			tts.speak("Glass is loged in");
		}
		else {
			btnLogin.setText(R.string.login_login_button);
			tts.speak("Glass is loged out");
		}
	}


}
