package com.technosite.virtuassist;
/*
 * © Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an ÒAS ISÓ BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.technosite.virtuassist.DBManager.DataManager;

public class LoginActivity extends Activity {
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				setContentView(R.layout.activity_login);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.login, menu);
		return false;
	}

	// ** Interface management
	
	public void login(View view) {
		DataManager dm = DataManager.getSharedInstance();
				EditText txtUser = (EditText) findViewById(R.id.userfield); 
		EditText txtPassword = (EditText) findViewById(R.id.passwordField);
				String user = txtUser.getText().toString(); 
		String password = txtPassword.getText().toString();
		if (user.equals("") || password.equals("")) {
			String message = getResources().getString(R.string.error_empty_fields_message);
			Toast.makeText(this, message,
					 Toast.LENGTH_SHORT).show();			
		} else {
			if (dm.logInUser(user,password)) {
				Intent resultData = new Intent();
				resultData.putExtra("userName", user);
				setResult(0, resultData);
				finish();
			} else {
				String message = getResources().getString(R.string.login_error_message);
				Toast.makeText(this, message,
						 Toast.LENGTH_SHORT).show();			
			}			
		}
			}

	public void register(View view) {
		finish();
		Intent i = new Intent(this, RegistrationActivity.class);
		startActivity(i);				
	}
	

}
