package com.technosite.TTS;
/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import java.util.Locale;

import android.content.Context;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;

public class TTS 
implements TextToSpeech.OnInitListener {

	private Context context;
	private TextToSpeech TTSEngine = null;
	
	// ** Constructor and destructor
	
	public TTS(Context ct) {
		super();
		context = ct;
		if (TTSEngine == null) TTSEngine = new TextToSpeech(context, this); 	
	}
	
	public void finalize() {
		if (TTSEngine != null) {
						TTSEngine.stop();
			TTSEngine.shutdown();
					}
	}
	
	// ** Class interface methods
	
@Override	
	public void onInit(int status) {
		// TTS initialization
	if (status == TextToSpeech.SUCCESS) {
		this.speak("");
	} else {
		Log.e("TTS engine error", "Error initializing the TTS engine.");
	}
	}

	// ** Voice management
	
	public void speak(String text) {
		if (TTSEngine != null) TTSEngine.speak(text, TextToSpeech.QUEUE_ADD, null);
		else Log.e("TTS speak", "TTS engine is not initialized")
;	}
	
	public void speakNow(String text) {
		if (TTSEngine != null) TTSEngine.speak(text, TextToSpeech.QUEUE_FLUSH, null);
		else Log.e("TTS speakNow", "TTS engine is not initialized");
	}
	
	public void speakWithDelay(String text, int time) {
		textForDelay = text;
		mHandler.removeCallbacks(timerEvent);
		mHandler.postDelayed(timerEvent, time);
	}
	
	public void stop() {
		this.speakNow("");
if (TTSEngine != null) TTSEngine.stop();		
	}
	
	// Normal value = 1.0
	public void setPitch(float value) {
		if (TTSEngine != null) TTSEngine.setPitch(value);
		else Log.e("TTS setPitch", "TTS engine is not initialized");
	}
	
	// Normal value = 1.0
	public void setRate(float value) {
		if (TTSEngine != null) TTSEngine.setSpeechRate(value);
		else Log.e("TTS setRate", "TTS engine is not initialized");
	}
	
	public void setLanguage(Locale idLang) {
		if (TTSEngine != null) 
			if (TTSEngine.setLanguage(idLang) == TextToSpeech.LANG_MISSING_DATA) {
Log.e("TTS engine error", "This language is not supported for TTS engine.");			
		}
	}

	
	// ** timer management
	
	private String textForDelay;
	private Handler mHandler = new Handler();

	
	private Runnable timerEvent = new Runnable() {
		public void run() {
			speak(textForDelay);
			mHandler.removeCallbacks(timerEvent);
		}
	};	
	
	public boolean isSpeaking() {
		return TTSEngine.isSpeaking();
	}
	
	public boolean isServiceAvailable() {
		boolean result = false;
		if (TTSEngine != null ) {
			String engineName = TTSEngine.getDefaultEngine();
if (engineName != null) result = true;			
		}
		return result;
	}
	
	
	
	
	// ** Midle singleton solution
	
	
	private static TTS singleton_instance = null;
	
	public static TTS getSharedInstance(Context context) {
if (singleton_instance == null) {
	singleton_instance  = new TTS(context);
}
return singleton_instance; 
	}
	
	public static void destroySharedInstance() {
		singleton_instance  .finalize();
		singleton_instance   = null;
	}
	
	}
