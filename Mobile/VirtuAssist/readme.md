#VirtuaGlass

This application is designed to be a server for GoogleGlass users in VirtuAssist service and a client of VirtuAssist services for users without GoogleGlass.. 
This application runs on Android devices..

## License

This project is under Apache V2 license. All files and resources should be under this license.
Please, read the [Apache V2](http://www.apache.org/licenses/LICENSE-2.0) license before any action with any content of this project.

