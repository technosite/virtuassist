package com.technosite.Sensors;
 
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class MovementManager implements SensorEventListener {

	public final int SensorTypeAccelerometer = 1;
	public final int SensorTypeRotation = 2;
	public final int SensorTypeGravity = 3;
	
	public interface MovementManagerListener {
	
		void sensorUpdated(int sensorType);
	}
	
	private Context ct = null;
	private MovementManagerListener  listener;
	public MovementManager(Context context, MovementManagerListener  objectListener) {
		ct = context;
		listener = objectListener;
		initSensors();
	}
	
	public void register(MovementManagerListener  objectListener) {
		listener = objectListener;
	}
	
	private void initSensors() {
		SensorManager mSensorManager = (SensorManager) ct.getSystemService(Context.SENSOR_SERVICE);
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_NORMAL);
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	public void stopSensors() {
		SensorManager mSensorManager = (SensorManager) ct.getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.unregisterListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
 
        mSensorManager.unregisterListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));
 
        mSensorManager.unregisterListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR));
	}

	// ** Sensors values
	
	public float accelerometerX;
	public float accelerometerY;
	public float accelerometerZ;
	
	public float rotationX;
	public float rotationY;
	public float rotationZ;
	
	public float gravityX;
	public float gravityY;
	public float gravityZ;
	
	public String toString() {
		return "Accelerometer (" + accelerometerX + "," + accelerometerY + "," + accelerometerZ + ")\nRotation (" + rotationX + ","+ rotationY + "," + rotationZ + ")\nGravity (" + gravityX + "," + gravityY + "," + gravityZ + ")";
		
	}
	
	
	// ** Listener
	
    @Override
    public void onSensorChanged(SensorEvent event) {
    	int type = 0;
    	synchronized (this) {
    		switch (event.sensor.getType()) {
    		case Sensor.TYPE_ACCELEROMETER:
    			type = SensorTypeAccelerometer;
preaccelerometerX = event.values[0];    			
preaccelerometerY = event.values[1];
preaccelerometerZ = event.values[2];
    			break;
    		case Sensor.TYPE_ROTATION_VECTOR:
    			type = SensorTypeRotation;
    			prerotationX = event.values[0];
    			prerotationY = event.values[1];
    			prerotationZ = event.values[2];
    			break;
    		case Sensor.TYPE_GRAVITY:
    			type = SensorTypeGravity;
    			pregravityX = event.values[0];
    			pregravityY = event.values[1];
pregravityZ = event.values[2];
    			break;
    		}
    		if (filter(type)) listener.sensorUpdated(type);
    		
    	}
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int value) {
    	
    }
    
    // ** Filtering
   
    public float filter = 0.5f;
    
    private float preaccelerometerX;
    private float preaccelerometerY;
    private float preaccelerometerZ;
	
    private float prerotationX;
    private float prerotationY;
    private float prerotationZ;
	
    private float pregravityX;
    private float pregravityY;
    private float pregravityZ;
    
    private float calculateFilter(float value1, float value2) {
float v1, v2;
if (value1 > value2) {
	v1 = value1;
	v2 = value2;
} else {
	v1 = value2;
	v2 = value1;
}
return v1 - v2;
    }
    
    private boolean filter(int sensor) {
    	boolean result = false;
    	float lx, ly, lz;
    	switch (sensor) {
    	case SensorTypeAccelerometer  :
    		lx = calculateFilter(accelerometerX , preaccelerometerX);
    		ly = calculateFilter(accelerometerY , preaccelerometerY);
    		lz = calculateFilter(accelerometerZ , preaccelerometerZ);
    		
    		if (lx>filter) {
    			accelerometerX = preaccelerometerX;
    			result = true;
    		}
    		if (ly>filter) {
    			accelerometerY = preaccelerometerY;
    			result = true;
    		}
    		if (lz>filter) {
    			accelerometerZ = preaccelerometerZ;
    			result = true;
    		}
    		break;
    	case SensorTypeRotation  :
    		lx = calculateFilter(rotationX , prerotationX);
    		ly = calculateFilter(rotationY , prerotationY);
    		lz = calculateFilter(rotationZ , prerotationZ);
    		
    		if (lx>filter) {
    			rotationX = prerotationX;
    			result = true;
    		}
    		if (ly>filter) {
    			rotationY = prerotationY;
    			result = true;
    		}
    		if (lz>filter) {
    			rotationZ = prerotationZ;
    			result = true;
    		}
    	    		break;
    	case    		 SensorTypeGravity  :
    		lx = calculateFilter(gravityX , pregravityX);
    		ly = calculateFilter(gravityY , pregravityY);
    		lz = calculateFilter(gravityZ , pregravityZ);
    		
    		if (lx>filter) {
    			gravityX = pregravityX;
    			result = true;
    		}
    		if (ly>filter) {
    			gravityY = pregravityY;
    			result = true;
    		}
    		if (lz>filter) {
    			gravityZ = pregravityZ;
    			result = true;
    		}
    	    		break;
    	}
    	return result;
    }
    
	
	
}
