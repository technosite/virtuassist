package com.technosite.testerglass;

import java.util.HashMap;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technosite.Bluetooth.BTCommunicator;
import com.technosite.Sensors.MovementManager;
import com.technosite.Sensors.MovementManager.MovementManagerListener;
import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;

public class MainActivity extends Activity implements MovementManagerListener {

	private TTS tts = null; 
	private SoundFX snd_ok = null;
	private SoundFX snd_error = null;
	private SoundFX snd_microphoneon = null;
	private SoundFX snd_tic = null;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tts = TTS.getSharedInstance(this);
    	snd_ok = new SoundFX(this, R.raw.ok);
        snd_microphoneon = new SoundFX(this, R.raw.microphoneon);
        snd_error = new SoundFX(this, R.raw.error);
        snd_tic = new SoundFX(this, R.raw.tic);
        setContentView(R.layout.activity_main);
                view0 = (LinearLayout) findViewById(R.id.view0);
        view1 = (LinearLayout) findViewById(R.id.view1);
        view2 = (LinearLayout) findViewById(R.id.view2);
        view3 = (LinearLayout) findViewById(R.id.view3);
        title = (TextView) findViewById(R.id.title);
        
        showCurrentView();
                        
    }
 
    // ** Menu management
private TextView title = null;
    private LinearLayout view0 = null;
    private LinearLayout view1 = null;
    private LinearLayout view2 = null;
    private LinearLayout view3 = null;
    private int currentView = 1;
    private int maxViews = 3;
    
    private void showCurrentView() {
    	view0.setVisibility(View.GONE);
    	view1.setVisibility(View.GONE);
    	view2.setVisibility(View.GONE);
    	view3.setVisibility(View.GONE);
    	showData = false;
    	switch (currentView) {
    	case 1 :
view1.setVisibility(View.VISIBLE);
initSensors();
showData = true;
    		break;
    	case 2 :
    		initSystemView();
    		view2.setVisibility(View.VISIBLE);
    		break;
    	case 3 :
    		view3.setVisibility(View.VISIBLE);
    		    		    		break;
    	}
    	
    }
    

    public void previousTesterEvent(View view) {
    	title.setText("Back gesture");
    	currentView--;
    	if (currentView<1) currentView = maxViews;
    	showCurrentView();
    	snd_ok.play();
    	    }
    
    public void nextTesterEvent(View view) {
    	title.setText("forward gesture");
    	currentView++;
    	if (currentView>maxViews) currentView = 1;
    	showCurrentView();
    	snd_ok.play();
    	    }
    
    public void changeEvent(View view) {
    	title.setText("Tapgesture");
    	tts.speak("Tap");
    	switch (currentView) {
    	case 1 : 
    		nextSensor();
    		break;
    	}
    	snd_ok.play();
    	
    }
    
    // ** Sensor test
    private String sensorName = "Accelerometer";
    private int sensorNumber = 1;
    private MovementManager mm = null;
    private boolean showData = false;
    
    private void initSensors() {
    	if (mm==null) {
    		mm = new MovementManager(this, this);
    		showData = true;
    		nextSensor();
    	}
    	    }
    
    public void sensorUpdated(int sensorType) {
		if (!showData) return;
		TextView textArea = (TextView) findViewById(R.id.sensorResult);
		
		switch (sensorNumber) {
		case 1 :
			textArea.setText("X:" + mm.accelerometerX + "\nY:" + mm.accelerometerY + "\nZ:" + mm.accelerometerZ);
			break;
		case 2 :
			textArea.setText("X:" + mm.rotationX + "\nY:" + mm.rotationY + "\nZ:" + mm.rotationZ);
			break;
		case 3 :
			textArea.setText("X:" + mm.gravityX + "\nY:" + mm.gravityY + "\nZ:" + mm.gravityZ);
			break;
		}
    }
    
    private void nextSensor() {
    	sensorNumber++; 
    	if (sensorNumber>3) sensorNumber=1;
    	switch (sensorNumber) {
    	case 1:
    		sensorName = "Accelerometer";
    		break;
    	case 2 :
    		sensorName = "Giroscope";
    		break;
    	case 3 :
    		sensorName="Compas";
    		break;
    	}
    	TextView sensorNameField = (TextView) findViewById(R.id.sensorName);
    	sensorNameField .setText(sensorName);
    	    	    }
    
    // ** SystemInformationView test

    private boolean isGoogleGlass() {
    	boolean isGlassResult = false;
    	HashMap<String, String> wereables = new HashMap<String, String>();
    	wereables.put("Google", "glass");
    	if (wereables.containsKey(Build.BRAND) && Build.PRODUCT.contains(wereables.get(Build.BRAND))) {
    		isGlassResult = true;
    		requestWindowFeature(Window.FEATURE_NO_TITLE);
    		getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);
    	}
    	return isGlassResult;
    }
    
    
    private BTCommunicator  bt = null;
    
    public void initSystemView() {

    	// bt = BTCommunicator .getSharedInstance(this);
    	TextView textArea = (TextView) findViewById(R.id.textArea);
    	String isGlass = "No";
    	String isTTS = "No";
    	String isBluetooth = "No";
    	String isBluetoothActive = "No";
    	// if (isGoogleGlass()) isGlass = "Yes";
    	// if (tts!=null) isTTS = "Yes";
    	// if (bt.isBluetoothAvailable()) isBluetooth = "Yes";
    	String result = "GoogleGlass device:" + isGlass;
    	result = result + "\nBluetooth available:" + isBluetooth;
    	result = result + "\nBluetooth active:" + isBluetoothActive;
    	result = result + "\nTTS available:" + isTTS;
    	textArea.setText(result);
    	
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_DPAD_CENTER) {
            tts.speak("Tap");
            return true;
        }
        if (keycode == KeyEvent.KEYCODE_DPAD_LEFT) {
            tts.speak("DPA Left");
            return true;
        }
        if (keycode == KeyEvent.KEYCODE_DPAD_DOWN) {
            tts.speak("Down");
            return true;
        }
        if (keycode == KeyEvent.KEYCODE_DPAD_UP) {
            tts.speak("Up");
            return true;
        }
        if (keycode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            tts.speak("Right");
            return true;
        }
        if (keycode == KeyEvent.KEYCODE_DPAD_LEFT) {
            tts.speak("Left");
            return true;
        }
        
        super.onKeyDown(keycode, event);
        return false;
    }
}
