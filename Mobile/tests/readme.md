#VirtuAssist tests

## License

This project is under Apache V2 license. All files and resources should be under this license.
Please, read the [Apache V2](http://www.apache.org/licenses/LICENSE-2.0) license before any action with any content of this project.

## Objetive

This folder is used to store all tests about experimental modules and some tests for mobile devices.