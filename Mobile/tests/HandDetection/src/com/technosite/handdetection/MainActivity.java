package com.technosite.handdetection;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.video.Video;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */

public class MainActivity extends Activity implements CvCameraViewListener2
{

	// menu options.
	private boolean showConvexHull ;
	private boolean showConvexityDefects;
	private boolean fixPointForTouching;
	private boolean useDepts;
private boolean detect;
private boolean showGrayImage;

	// reduce noise
	private boolean tracking;
	private boolean medianBlur;
	private boolean equalizeHist;
	private boolean equalizeMedian;
	private boolean equalizeThresholding;
	private boolean gaussianThresholding;
private boolean gaussianEq;
	private File externalStorageDirectory;
	
	
	
	private boolean isLoad;


	private static final Scalar RED = new Scalar(255, 0, 0, 255);
	private static final Scalar GREEN = new Scalar(0, 255, 0, 255);
	private static final Scalar BLUE = new Scalar(0, 0, 255, 255);
	private static final Scalar BLACK = new Scalar(0,0,0,255);

	private Mat                    mRgba;
    private Mat                    mGray;

    private File                   mCascadeFile;
    private CascadeClassifier      mJavaDetector;

    private float                  mRelativeHandSize   = 0.2f;
    private int                    mAbsoluteHandSize   = 0;

    private HandDetectionView mOpenCvCameraView;

// Haar Cascades
        private static int cascade1 = R.raw.hand_cascade_1;
    private static int cascade2 = R.raw.hand_cascade_2;
    private static int cascade3 = R.raw.hand_cascade_other;
    private static int cascade5 = R.raw.hand_cascade_5;
    
    private static Point center = new Point(600, 400);

    // Tracking.
    private Mat mPreviousGray;
private List<MatOfPoint2f> points2f; // firstly list of MatOfPoint objects
private MatOfPoint initial;


private static int maxDetectionCount = 500;
private static double qualityLevel = 0.1;
private static double minDistance = 10;


private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status)
        {
            switch (status)
            {
                case LoaderCallbackInterface.SUCCESS:
                {

                	initial = new MatOfPoint();
                	
                	loadCascadeClassifier(cascade5);
                mOpenCvCameraView.enableView();
                	                }// end case.
                break;
                default:
                {
                	super.onManagerConnected(status);
                }
            break;
            }// end switch.
        }// end onManagerConnected.
    };

    public MainActivity()
    {
    detect = true;
    showGrayImage = false;
    	showConvexHull = true;
    showConvexityDefects = true;
    fixPointForTouching = false;
    useDepts = false;
    tracking = false;
	medianBlur = false;
	equalizeHist = false;
	equalizeMedian = false;
	equalizeThresholding = false;
	gaussianThresholding = false;
	gaussianEq = false;
    
	points2f = new ArrayList<MatOfPoint2f>();


    }// end constructor.


    @Override
	protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);



		setContentView(R.layout.main);
		externalStorageDirectory =  Environment.getExternalStorageDirectory();
		
		mOpenCvCameraView = (HandDetectionView) findViewById(R.id.handdetection_activity_java_surface_view);


		mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		mOpenCvCameraView.setCvCameraViewListener(this);


    }// end onCreate.

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }// end onPause.

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }// end onResume.

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }// end onDestroy.


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{

		menu.add(Menu.NONE, 1, Menu.NONE, "Show Convex Hull");
		menu.add(Menu.NONE, 2, Menu.NONE, "Show Convexity Defects");
		menu.add(Menu.NONE, 3, Menu.NONE, "Use Depts");
		menu.add(Menu.NONE, 4, Menu.NONE, "Fix Point for Touching");

		// reduce noise
		menu.add(Menu.NONE, 5, Menu.NONE, "Median Blur");
		menu.add(Menu.NONE, 6, Menu.NONE, "Equalize Hist");
		menu.add(Menu.NONE, 7, Menu.NONE, "Equalize Median");
		menu.add(Menu.NONE, 8, Menu.NONE, "Equalize Thresholding");
		menu.add(Menu.NONE, 9, Menu.NONE, "Gaussian Thresholding");
		menu.add(Menu.NONE, 10, Menu.NONE, "Gaussian Equalize");
			menu.add(Menu.NONE, 11, Menu.NONE, "Active Tracking");
		menu.add(Menu.NONE, 12, Menu.NONE, "Show Gray Image");
		
		
		/*
		SubMenu sub = menu.addSubMenu(Menu.NONE, 5, Menu.NONE, "Change Classifier");
		sub.add(Menu.NONE, 6, Menu.NONE, "Classifier 1");
		sub.add(Menu.NONE, 7, Menu.NONE, "Classifier 2");
		sub.add(Menu.NONE, 8, Menu.NONE, "Classifier 3 (Default)");
		*/


		return true;
	}// end onCreateOptionsMenu

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		switch(item.getItemId())
		{
		case 1:
		{
if(!showConvexHull)
{
showConvexHull = true;
tracking = false;			
item.setTitle("Hide Convex Hull");
}// end if.
else
{
	showConvexHull = false;
	item.setTitle("Show Convex Hull");
}// end else.
}// end case 1.
		break;
		case 2:
		{
			if(!showConvexityDefects)
			{
			showConvexityDefects = true;
						item.setTitle("Hide Convexity Defects");
			}// end if.
			else
			{
				showConvexityDefects= false;
				item.setTitle("Show Convexity Defects");
			}// end else.
	}// end case 2.
		break;
		case 3:
		{
			if(!useDepts)
			{
			useDepts= true;
						item.setTitle("Use depts");
			}// end if.
			else
			{
				useDepts = false;
				item.setTitle("Leave to Use Depts");
			}// end else.
	}// end case 3.
		break;
		case 4:
		{
			if(!fixPointForTouching)
			{
			fixPointForTouching = true;
						item.setTitle("Fix Point for Touching");
			}// end if.
			else
			{
				fixPointForTouching = false;
				item.setTitle("Delete Point for Touching");
			}// end else.
				}// end case 4.
		break;
		case 5:
		{
			if(!medianBlur)
			{
				medianBlur = true;
				item.setTitle("No Median Blur");
			}// end if.
			else
			{
				medianBlur = false;
				item.setTitle("Median Blur");
			}// end else.
		}// end case 5.
		break;
		case 6:
		{
			if(!equalizeHist)
			{
				equalizeHist = true;
				item.setTitle("No Equalize Hist");
			}// end if.
			else
			{
				equalizeHist = false;
				item.setTitle("Equalize Hist");
			}// end else.
		}// end case 6.
		break;
		case 7:
		{
			if(!equalizeMedian)
			{
				equalizeMedian = true;
				item.setTitle("No Equalize Median");
			}// end if.
			else
			{
				equalizeMedian = false;
				item.setTitle("Equalize Median");
			}// end else.
		}// end case 7.
		break;
		case 8:
		{
			if(!equalizeThresholding)
			{
				equalizeThresholding = true;
				item.setTitle("No Equalize Thresholding");
			}// end if.
			else
			{
				equalizeThresholding = false;
				item.setTitle("Equalize Thresholding");
			}// end else.
		}// end case 8.
		break;
		case 9:
		{
			if(!gaussianThresholding)
			{
				gaussianThresholding= true;
				item.setTitle("No Gaussian Thresholding");
			}// end if.
			else
			{
				gaussianThresholding= false;
				item.setTitle("Gaussian Thresholding");
			}// end else.
		}// end case 9.
		break;
		case 10:
		{
			if(!gaussianEq)
			{
				gaussianEq= true;
				item.setTitle("No Gaussian Equalize");
			}// end if.
			else
			{
				gaussianEq= false;
				item.setTitle("Gaussian Equalize");
			}// end else.
					}// end case 10.
		break;
		case 11:
		{
			if(!tracking)
			{
				tracking= true;
		showConvexHull = false;
				item.setTitle("Desactive Tracking");
			}// end if.
			else
			{
				tracking= false;
				item.setTitle("Active Tracking");
			}// end else.
		}// end case 10.
		break;
		case 12:
		{
			if(!showGrayImage)
			{
				showGrayImage = true;
				item.setTitle("Hide Gray Image");
			}// end if.
			else
			{
				showGrayImage = false;
				item.setTitle("Show Gray Image");
			}// end else.
		}// end case 11.
		break;
		/*
		case 6:
		{
		loadCascadeClassifier(cascade1);
		}// end case 6.
		break;
		case 7:
		{
			loadCascadeClassifier(cascade2);
		}// end case 7.
		break;
		case 8:
	{
		loadCascadeClassifier(cascade3);
	}// end case 7.
	break;
		*/
		}// end switch.
	return  true;
	}// end onOptionsItemSelected.



	@Override
	public void onCameraViewStarted(int width, int height)
	{
if(android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.ICE_CREAM_SANDWICH)
{
mOpenCvCameraView.googleGlassXE10WorkAround();
mOpenCvCameraView.enableFpsMeter();
}// end if.

		mGray = new Mat();
		mPreviousGray = new Mat();
        mRgba = new Mat();

	}// end onCameraViewStarted.


	@Override
	public void onCameraViewStopped()
	{

    	mGray.release();
    	mPreviousGray.release();
        mRgba.release();

	}// end onCameraViewStopped.


    public Mat onCameraFrame(CvCameraViewFrame inputFrame)
    {

    
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        if (mAbsoluteHandSize == 0)
        {
            int height = mGray.rows();

            if (Math.round(height * mRelativeHandSize) > 0)
            {
                mAbsoluteHandSize = Math.round(height * mRelativeHandSize);
            }// end if comprobacion del redondeo.
                   }// end if absoluteHandsize == 0.

        if(medianBlur)
        {
        	Imgproc.medianBlur(mGray, mGray, 3);        	
                }// end if medianBlur.
        else if(equalizeHist)
        {
        	Imgproc.equalizeHist(mGray, mGray);
	       }// end if equalizeHist.
        else if(equalizeMedian)
        {
        	Imgproc.equalizeHist(mGray, mGray);
        	Imgproc.medianBlur(mGray, mGray, 3);
        }// end equalizeMedian.
        else if(equalizeThresholding)
        {
        	Imgproc.equalizeHist(mGray, mGray);
            Imgproc.threshold(mGray, mGray, 0, 255, Imgproc.THRESH_OTSU);
        	       }// end equalizeThresholding.
        else if(gaussianThresholding)
        {
        	Imgproc.GaussianBlur(mGray, mGray, new Size(3, 3), 0);
            Imgproc.threshold(mGray, mGray, 0, 255, Imgproc.THRESH_OTSU);
            	               }// end if gaussianThresholding.
        else if(gaussianEq)
        {
        	Imgproc.GaussianBlur(mGray, mGray, new Size(3, 3), 0);
        Imgproc.equalizeHist(mGray, mGray);
        }// end if gaussianEq.
        
    if(showGrayImage)
    {
 return mGray;
    }// end showGrayImage.        
    
    if(detect)
{
        detectHand(mGray, 0);
        
       if(tracking)
       {
        	         	if(points2f.size() > 1)
             {
                 opticalFlow(mGray);
             }// end if.
       }// end tracking.
       
       
}// end if detect.               

    return mRgba;
    
    }// end onCameraFrame.



/* Helper methods */

private void detectHand(Mat mGray, int mode)
{


    MatOfRect fingers = new MatOfRect();

    mJavaDetector.detectMultiScale(mGray, // imagen de entrada
    		fingers, // objeto donde se almacenan las coincidencias
    		1.1, // factor de escala.
    		1, // minimo de vecinos
    		2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
            new Size(mAbsoluteHandSize, mAbsoluteHandSize),// tamano minimo el de la cara.
            new Size()); // tamano maximo, sin tamano maximo

List<MatOfPoint> listContours = getPointsFromRect(fingers);
    List<MatOfPoint> hullContours = new ArrayList<MatOfPoint>();
List<MatOfInt4> allDefects = new ArrayList<MatOfInt4>();
List<Point> startDefectsLog = new ArrayList<Point>();
for(int i = 0; i < listContours.size(); i++)
{
MatOfPoint points = listContours.get(i);
MatOfInt hull = new MatOfInt();
Imgproc.convexHull(points, hull);

List<Point> hullPoints = new ArrayList<Point>();
List<Integer> hulls = hull.toList();
for(int j = 0; j < hulls.size(); j++)
{
hullPoints.add(points.toList().get(hulls.get(j)));
}// end for j convexHull.

MatOfPoint hullContour = new MatOfPoint();
hullContour.fromList(hullPoints);


MatOfInt4 convexityDefects = new MatOfInt4();
if(!hull.empty())
{
if(hull.rows() >= 3)
{
Imgproc.convexityDefects(points, hull, convexityDefects);
allDefects.add(convexityDefects);
List<Integer> convexityData = convexityDefects.toList();
List<Integer> listStartIndex = new ArrayList<Integer>();
for(int j = 0; j < convexityData.size(); j++)
{

	if(j == 0)
{
int 	startIndex = convexityData.get(j);
listStartIndex.add(startIndex);
	}// end if j 0.
else if(j%4 == 0)
{
int 		startIndex = convexityData.get(j);
listStartIndex.add(startIndex);
	}// end if %4.

}// end for j convexityData.



//Add convexity defects  starting points to points2f for tracking.
List<Point> listStartDefects = new ArrayList<Point>();
for(int j = 0; j < listStartIndex.size(); j++)
{
Point startDefect = points.toList().get(listStartIndex.get(j));
listStartDefects.add(startDefect);
startDefectsLog.add(startDefect);

if(showConvexityDefects)
{
 Core.circle(mRgba, startDefect, 10, RED);
}// end if showConvexityDefects
if(fixPointForTouching)
{
// touchPoint(startDefect);
}// end if.
}// end for j listStartIndex.

// detect fingers.
// listStartDefects = detectFingers(listStartDefects);


MatOfPoint startDefects = new MatOfPoint();
startDefects.fromList(listStartDefects);
MatOfPoint2f startDefects2f = new MatOfPoint2f();
startDefects.convertTo(startDefects2f, CvType.CV_32FC2);
points2f.add(startDefects2f);

}// end if convexity no empty.
}// end if convexity rows.

    hullContours.add(hullContour);
}// end for i.

createLog(fingers.toList().size(), hullContours.size(), startDefectsLog);

    if(hullContours.size() > 0)
    {
if(showConvexHull)
{
    	 Imgproc.drawContours(mRgba, hullContours, -1, GREEN);
}// end if showConvexHull.
}// end if hullContours size > 0.

     if(useDepts)
     {
    	 useDepts(listContours, allDefects);
     }// end if useDepts.


    if(fixPointForTouching)
    {
    	Core.circle(mRgba, center, 100, BLUE);
    }// end if fixPointFortouching.

    // tracking.

    
    
}// end detectHand.
    
    
private List<MatOfPoint> getPointsFromRect(MatOfRect fingers)
{
	List<MatOfPoint> listContours = new ArrayList<MatOfPoint>();

	List<Rect> rects = fingers.toList();
	List<Point> points = new ArrayList<Point>();

	for(int i = 0; i < rects.size(); i++)
	{
	Rect rect = rects.get(i);

	// obtenemos las 4 esquinas.
	Point tl = rect.tl();
	Point br = rect.br();
	Point tr = new Point(br.x, tl.y);
	Point bl = new Point(tl.x, br.y);

	// obtenemos todos los puntos del eje x.
	// se incrementa la componente x manteniendo la componente y.
	for(double j =  tl.x; j <= tr.x; j++)
	{
		Point pt1 = new Point(j, tl.y);
		Point pt2 = new Point(j, br.y);
points.add(pt1);
points.add(pt2);
	}// end for j.

	// puntos del eje y.
	// se incrementa la componente y dejando fija la componente x.
	for(double j = br.y; j <= tl.y; j++)
	{
		Point pt1 = new Point(bl.x, j);
		Point pt2 = new Point(br.x, j);
	points.add(pt1);
	points.add(pt2);
	}// end for j.

	MatOfPoint contour = new MatOfPoint();
	contour.fromList(points);

	listContours.add(contour);
	}// end for i.


	return listContours;

}// end getPointsFromRect.

private void useDepts(List<MatOfPoint> listContours, List<MatOfInt4> allDefects)
{
    List<Integer> listDefects = new ArrayList<Integer>();
    for(int i = 0; i < allDefects.size(); i++)
    {
    MatOfInt4 matDefects = allDefects.get(i);
List<Integer> listIntegers = matDefects.toList();
for(int j = 0; j < listIntegers.size(); j++)
{
int data4 = listIntegers.get(j);
listDefects.add(data4);
}// end for j.
   }// end for i allDefects.

    List<Integer> depts = new ArrayList<Integer>();
    for(int i = 0;i < listDefects.size(); i++)
    {
    	if(i == 3)
    	{
    		depts.add(listDefects.get(i));
    	}// end if i 3.
    	else if(i%4 == 0)
    	{
    		depts.add(listDefects.get((i+3)));
    	}// end for i%4.
    }// end for i.

    int dept1 = 0, dept2 = 0, dept3 = 0, dept4 = 0;

    if(depts.size() > 3)
    {
    dept1 = depts.get((depts.size()-1));
    dept2 = depts.get((depts.size()-2));
    dept3 = depts.get((depts.size()-3));
    dept4 = depts.get((depts.size()-4));
    }// end if.

    List<Integer> startIndexes = new ArrayList<Integer>();
    for(int i = 0; i < listDefects.size(); i++)
    {
    	int data4 = listDefects.get(i);
    	        if(data4 == dept1 || data4 == dept2 || data4 == dept3 || data4 == dept4)
    	{
    	int startIndex = listDefects.get((i-3));
    	startIndexes.add(startIndex);
    	}// end if.
    }// end for i startIndexes.

    for(int i = 0; i < listContours.size(); i++)
    {
    	MatOfPoint mtp = listContours.get(i);
    	List<Point> points = mtp.toList();
    	for(int j = 0; j < startIndexes.size(); j++)
    	{
    		int startIndex = startIndexes.get(j);
    		if(startIndex < points.size())
    				{
    		Point p = points.get(startIndex);

    	Core.circle(mRgba, p, 50, BLACK);

    				}// end if.
    				}// end for j.
    	        }// end for i.

}// end useDepts.

private void loadCascadeClassifier(int idCascade)
{
    // Copy the resource into a temp file so OpenCV can load it
InputStream is = getResources().openRawResource(idCascade);

    File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
    File mCascadeFile = new File(cascadeDir, "hand.xml");
    try
    {
		FileOutputStream os = new FileOutputStream(mCascadeFile);

		byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = is.read(buffer)) != -1)
        {
            os.write(buffer, 0, bytesRead);
        }// end while.
        is.close();
        os.close();

        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
        if (mJavaDetector.empty())
        {
Log.e("", "Cascade Classifier Load Failed");
mJavaDetector = null;
                                   }// end mJavaDetector empty.
        else
        {
            cascadeDir.delete();
              }// end else.
                           }// end try.
    catch (FileNotFoundException e)
    {
						e.printStackTrace();
	}// end catch.
catch (IOException e)
{
						e.printStackTrace();
	}// end catch.

}// end loadCascadeClassifier.

private void opticalFlow(Mat m)
{
	
	if(mPreviousGray.empty())
	{
		m.copyTo(mPreviousGray);
	}// end if mPreviousGray empty.

	if(points2f.size() > 1)
	{
		if(points2f.get(0).total() > 0)
		{
			MatOfByte status = new MatOfByte();
			MatOfFloat err = new MatOfFloat();

			Video.calcOpticalFlowPyrLK(mPreviousGray,
					m,
					points2f.get(0), // input vector of point for tracking.
					points2f.get(1),// output vector of points
					status,
					err);

					
//					new Size(15, 15),
//1);		

			
			for(int i = 0; i < points2f.get(1).toList().size();i++)
{
Core.circle(mRgba, points2f.get(1).toList().get(i), 10, RED);
// Log.d("", "knda5 tracking");
}// end for i.

			m.copyTo(mPreviousGray);

		}// end if.
	}// end if points2f size > 1.

}// end opticalFlow.

private List<Point> detectFingers(List<Point> listPoints)
{
	
	List<Point> twoPoints = new ArrayList<Point>();
	
	if(listPoints.size() > 1)
	{

		List<Double> listX = new ArrayList<Double>();
			for(int i = 0; i < listPoints.size(); i++)
		{
												listX.add(listPoints.get(i).x);
					}// end for i.
		
		Collections.sort(listX);
		double minX = listX.get((listX.size()-1));

		List<Double> listY = new ArrayList<Double>();
		for(int i = 0; i < listPoints.size(); i++)
	{
												listY.add(listPoints.get(i).y);
			}// end for i.
	
	Collections.sort(listY);
	double maxY = listX.get(0);

	
	for(int i = 0; i < listPoints.size(); i++)
	{
		if(listPoints.get(i).x == minX)
		{
			twoPoints.add(listPoints.get(i));
		}// end if minX.

		if(listPoints.get(i).y == maxY)
		{
			twoPoints.add(listPoints.get(i));
		}// end if maxY.
			}// end for i.
	}// end if listPoints > 1.

	
	return twoPoints;
	
}// end detectFingers.

private void createLog(int numContours, int numHulls, List<Point> startDefectsLog)
{

long currentMillis = 	System.currentTimeMillis();
Date date = new Date();
date.setTime(currentMillis);
int day = date.getDay();
			int hours = date.getHours();
			int minutes = date.getMinutes();
			int seconds = date.getSeconds();
			String timestamp = day+" "+hours+":"+minutes+":"+seconds;
			
			if(!medianBlur && !equalizeHist && !equalizeMedian && !equalizeThresholding && !gaussianThresholding && !gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
						"\n";
				writeSDCard(log, "no_reduce_noise.txt");
				
							}// end no reduce noise.
			else if(medianBlur && !equalizeHist && !equalizeMedian && !equalizeThresholding && !gaussianThresholding && !gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
"\n"						;
				writeSDCard(log, "median_blur.txt");
				
							}// end medianBlur.
			else if(!medianBlur && equalizeHist && !equalizeMedian && !equalizeThresholding && !gaussianThresholding && !gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
						"\n";
				writeSDCard(log, "equalize_hist.txt");
				
							}// end equalizeHist.
			else if(!medianBlur && !equalizeHist && equalizeMedian && !equalizeThresholding && !gaussianThresholding && !gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
						"\n";
				writeSDCard(log, "equalize_median.txt");
				
							}// end equalizeMedian
			else if(!medianBlur && !equalizeHist && !equalizeMedian && equalizeThresholding && !gaussianThresholding && !gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
						"\n";
				writeSDCard(log, "equalize_thresholding.txt");
				
							}// end equalizeThresholding.
			else if(!medianBlur && !equalizeHist && !equalizeMedian && !equalizeThresholding && gaussianThresholding && !gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
						"\n";
				writeSDCard(log, "gaussian_thresholding.txt");
				
							}// end gaussianThresholding.
			else if(!medianBlur && !equalizeHist && !equalizeMedian && !equalizeThresholding && !gaussianThresholding && gaussianEq)
			{

				String log = timestamp+
						"\n"+
						"contours: "+numContours+
						"\n"+
						"hulls: "+numHulls+
						"\n"+
						"defects points: "+startDefectsLog.size()+
						"\n"+
						timestamp+
						"\n";
				writeSDCard(log, "gaussianEq.txt");
				
							}// end if gaussianEq.

			

			
}// end createLog.

private void writeSDCard(String text, String fileName)
{

	if(tracking)
	{
		fileName = "tracking_"+fileName;
	}// end if.
	
	byte[] bytes = text.getBytes();
File file = new File(externalStorageDirectory, fileName);
	FileWriter fw = null;
	try 
	{
		fw = new FileWriter(file, true);
				fw.write(text);
		

	
	}// end try. 
	catch (FileNotFoundException e) 
	{
				e.printStackTrace();
	}// end catch.
 catch (IOException e) 
 {
		e.printStackTrace();
	}// end catch.

	finally
	{
		if(fw!= null)
		{
		try 
		{
			fw.close();
		}// end try. 
		catch (IOException e) 
		{
						e.printStackTrace();
		}// end catch.
		}// end if.
}// end finally.
}// end writeSDCard.
}// end class.
