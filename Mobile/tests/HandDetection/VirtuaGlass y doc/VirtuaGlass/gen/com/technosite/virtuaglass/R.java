/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.technosite.virtuaglass;

public final class R {
    public static final class attr {
        /** <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
         */
        public static final int camera_id=0x7f010001;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int show_fps=0x7f010000;
    }
    public static final class color {
        public static final int white=0x7f060000;
        public static final int yellow=0x7f060001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f070000;
        public static final int activity_vertical_margin=0x7f070001;
    }
    public static final class drawable {
        public static final int arrow=0x7f020000;
        public static final int ic_launcher=0x7f020001;
        public static final int instructionexploration=0x7f020002;
        public static final int instructionoperations=0x7f020003;
    }
    public static final class id {
        public static final int any=0x7f050000;
        public static final int arrowExploreImageView=0x7f050006;
        public static final int arrowOperationsImageView=0x7f050009;
        public static final int arrowPathImageView=0x7f050015;
        public static final int back=0x7f050001;
        public static final int controlDetailsImageView=0x7f05000c;
        public static final int controlDetailsTextView=0x7f05000b;
        public static final int descriptionStepTextView=0x7f05001c;
        public static final int exploreMainLayout=0x7f050005;
        public static final int exploreMainTextView=0x7f050007;
        public static final int front=0x7f050002;
        public static final int glassListenerImageView=0x7f05000f;
        public static final int glassListenerTextView=0x7f05000e;
        public static final int handdetection_activity_java_surface_view=0x7f05000d;
        public static final int imageStepImageView=0x7f05001b;
        public static final int mainHeaderTextView=0x7f050003;
        public static final int mainInstructionsTextView=0x7f050004;
        public static final int notificationViewerHeaderTextView=0x7f050010;
        public static final int notificationViewerImageView=0x7f050011;
        public static final int operationNameTextView=0x7f050019;
        public static final int operationsMainLayout=0x7f050008;
        public static final int operationsMainTextView=0x7f05000a;
        public static final int pathFinishedImageView=0x7f050013;
        public static final int pathFinishedTextView=0x7f050012;
        public static final int pathRowLayout=0x7f050014;
        public static final int pathSelectorInstructionsTextView=0x7f050017;
        public static final int pathSelectorTextView=0x7f050016;
        public static final int pathTable=0x7f050018;
        public static final int stepLayout=0x7f05001a;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int control_details=0x7f030001;
        public static final int device_explorer=0x7f030002;
        public static final int glass_listener=0x7f030003;
        public static final int notification_viewer=0x7f030004;
        public static final int path_finished=0x7f030005;
        public static final int path_row=0x7f030006;
        public static final int path_selector=0x7f030007;
        public static final int step_viewer=0x7f030008;
    }
    public static final class string {
        public static final int Border=0x7f080074;
        public static final int ChangeSurface=0x7f08006b;
        public static final int Close=0x7f080049;
        public static final int Device=0x7f08005b;
        public static final int Locate=0x7f08003c;
        public static final int Next=0x7f08002e;
        public static final int No=0x7f080069;
        public static final int Previous=0x7f08002f;
        public static final int ShowImage=0x7f080031;
        public static final int Speak=0x7f080030;
        public static final int Type=0x7f08005c;
        public static final int Yes=0x7f080068;
        public static final int Zoom=0x7f08006a;
        public static final int about_option=0x7f08000d;
        public static final int about_this_application=0x7f08001c;
        public static final int andthe=0x7f080054;
        public static final int app_name=0x7f080000;
        public static final int barcodereader_message=0x7f080067;
        public static final int barcodereader_title=0x7f080066;
        public static final int cancel=0x7f080047;
        public static final int close_button=0x7f080048;
        public static final int commathe=0x7f080055;
        public static final int description_progressbar=0x7f080070;
        public static final int device_identificator_button=0x7f08002d;
        public static final int device_identificator_header=0x7f08002b;
        public static final int device_identificator_instructions=0x7f08002c;
        public static final int device_menu_device_information=0x7f080027;
        public static final int device_menu_explore=0x7f080028;
        public static final int device_menu_instructions=0x7f080026;
        public static final int device_menu_paths=0x7f080029;
        public static final int down=0x7f08004c;
        public static final int elementin=0x7f080053;
        public static final int error_device_not_found=0x7f080061;
        public static final int error_empty_fields_message=0x7f080073;
        public static final int error_image_not_found=0x7f080062;
        public static final int explore_instructions=0x7f080006;
        public static final int explore_option=0x7f080004;
        public static final int firstStep=0x7f080058;
        public static final int focus_sticker=0x7f080002;
        public static final int hello_world=0x7f08000e;
        public static final int identify_a_device_option=0x7f08000b;
        public static final int image_of=0x7f08006f;
        public static final int isin=0x7f080052;
        public static final int isthe=0x7f080051;
        public static final int isthefirst=0x7f08004f;
        public static final int isthelast=0x7f080050;
        public static final int language=0x7f080075;
        public static final int left=0x7f08004d;
        public static final int live_card_tag=0x7f080009;
        public static final int loading_device=0x7f080063;
        public static final int loading_image=0x7f080064;
        public static final int login_error_message=0x7f08001b;
        public static final int login_instruction_message=0x7f080015;
        public static final int login_login_button=0x7f080019;
        public static final int login_mail_label=0x7f080018;
        public static final int login_password_label=0x7f080017;
        public static final int login_register_button=0x7f08001a;
        public static final int login_user_label=0x7f080016;
        public static final int logout_button=0x7f08003f;
        public static final int noHelp=0x7f080042;
        public static final int noHelp_note=0x7f080043;
        public static final int no_paths=0x7f080071;
        public static final int no_surfaces=0x7f080072;
        public static final int ok=0x7f080046;
        public static final int operations_instructions=0x7f080007;
        public static final int operations_option=0x7f080005;
        public static final int ordinal=0x7f08005d;
        public static final int path_finished_text=0x7f080008;
        public static final int path_selector_instructions=0x7f08002a;
        public static final int please_wait=0x7f080060;
        public static final int position0=0x7f080032;
        public static final int position1=0x7f080033;
        public static final int position2=0x7f080034;
        public static final int position3=0x7f080035;
        public static final int position4=0x7f080036;
        public static final int position5=0x7f080037;
        public static final int position6=0x7f080038;
        public static final int position7=0x7f080039;
        public static final int position8=0x7f08003a;
        public static final int position9=0x7f08003b;
        public static final int pref_image=0x7f080022;
        public static final int pref_image_note=0x7f080023;
        public static final int pref_text=0x7f080020;
        public static final int pref_text_note=0x7f080021;
        public static final int pref_voice=0x7f080024;
        public static final int pref_voice_note=0x7f080025;
        public static final int registration_instruction_message=0x7f08001e;
        public static final int restoreNotifications_button=0x7f080056;
        public static final int restoredNotifications=0x7f080059;
        public static final int right=0x7f08004e;
        public static final int save_settings_button=0x7f080040;
        public static final int say_option=0x7f080003;
        public static final int settings_option=0x7f08000c;
        public static final int settings_registration_instruction_message=0x7f08001f;
        public static final int start_virtuaglass=0x7f080001;
        public static final int stepEnd=0x7f080057;
        public static final int stepViewer_info=0x7f080044;
        public static final int step_viewer_image_description=0x7f080041;
        public static final int surfaceDrawer_info=0x7f080045;
        public static final int surface_selector_instructions=0x7f08003e;
        /** pdt 
         */
        public static final int title_activity_about=0x7f08000a;
        public static final int title_activity_device_identificator=0x7f080013;
        public static final int title_activity_device_menu=0x7f08001d;
        public static final int title_activity_information=0x7f08004a;
        public static final int title_activity_login=0x7f08000f;
        public static final int title_activity_path_selector=0x7f080011;
        public static final int title_activity_registration=0x7f080014;
        public static final int title_activity_step_viewer=0x7f080012;
        public static final int title_activity_surface_drawer=0x7f080010;
        public static final int title_activity_surface_selector=0x7f08003d;
        public static final int title_activity_waiting=0x7f08005f;
        public static final int title_settings=0x7f08005e;
        public static final int touchToExplore=0x7f08005a;
        public static final int up=0x7f08004b;
        public static final int wrong_code_device=0x7f080065;
        public static final int zoom_mensaje=0x7f08006c;
        public static final int zoom_off=0x7f08006e;
        public static final int zoom_on=0x7f08006d;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090001;
    }
    public static final class xml {
        public static final int voice_trigger_start=0x7f040000;
    }
    public static final class styleable {
        /** Attributes that can be used with a CameraBridgeViewBase.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_camera_id com.technosite.virtuaglass:camera_id}</code></td><td></td></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_show_fps com.technosite.virtuaglass:show_fps}</code></td><td></td></tr>
           </table>
           @see #CameraBridgeViewBase_camera_id
           @see #CameraBridgeViewBase_show_fps
         */
        public static final int[] CameraBridgeViewBase = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.technosite.virtuaglass.R.attr#camera_id}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
          @attr name com.technosite.virtuaglass:camera_id
        */
        public static final int CameraBridgeViewBase_camera_id = 1;
        /**
          <p>This symbol is the offset where the {@link com.technosite.virtuaglass.R.attr#show_fps}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.technosite.virtuaglass:show_fps
        */
        public static final int CameraBridgeViewBase_show_fps = 0;
    };
}
