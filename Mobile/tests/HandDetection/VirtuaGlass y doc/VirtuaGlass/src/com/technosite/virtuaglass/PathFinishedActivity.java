package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;

public class PathFinishedActivity extends Activity {

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.path_finished);
		
		// play positive sound.
		MediaPlayer mp = new MediaPlayer();
		AssetFileDescriptor descriptor = null;
		
		try 
		{
			descriptor = getAssets().openFd("sound.mp3");
			mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
	    mp.prepare();    	
		mp.start();
		}// end try. 
		catch (IOException e) 
		{
				e.printStackTrace();
		}// end catch.
	
		finally
		{
			if(descriptor != null)
			{
			try {
				descriptor.close();
			}// end try. 
			catch (IOException e) {
						e.printStackTrace();
			}// end catch.
			}// end if.
			}// end finally.
	
		}// end onCreate.

	
}// end class.
