package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity
{

	private TextView exploreMainTextView;
	private TextView operationsMainTextView;

	private ImageView arrowExploreImageView;
	private ImageView arrowOperationsImageView;



	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

	exploreMainTextView = (TextView) findViewById(R.id.exploreMainTextView);
	operationsMainTextView = (TextView) findViewById(R.id.operationsMainTextView);

	arrowExploreImageView = (ImageView) findViewById(R.id.arrowExploreImageView);
	arrowOperationsImageView = (ImageView) findViewById(R.id.arrowOperationsImageView);


	exploreMainTextView.setOnFocusChangeListener(new OnFocusChangeListener()
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus) {

			if(hasFocus)
		{
			arrowExploreImageView.setVisibility(View.VISIBLE);
			exploreMainTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
	exploreMainTextView.setTypeface(Typeface.DEFAULT_BOLD);
				}// end if hasFocus.
		else
		{
			arrowExploreImageView.setVisibility(View.GONE);
			exploreMainTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
	exploreMainTextView.setTypeface(Typeface.DEFAULT);
						}// end else hasFocus.
		}// end onFocusChange.
		});


	operationsMainTextView.setOnFocusChangeListener(new OnFocusChangeListener()
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
		if(hasFocus)
		{
			arrowOperationsImageView.setVisibility(View.VISIBLE);
			operationsMainTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
	operationsMainTextView.setTypeface(Typeface.DEFAULT_BOLD);
				}// end if hasFocus.
		else
		{
			arrowOperationsImageView.setVisibility(View.GONE);
			operationsMainTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
	operationsMainTextView.setTypeface(Typeface.DEFAULT);
						}// end else hasFocus.
		}// end onFocusChange.
		});

exploreMainTextView.setOnClickListener(new OnClickListener()
{
	@Override
	public void onClick(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
	   	intent.setClassName(MainActivity.this, DeviceExplorerActivity.class.getName());
	MainActivity.this.startActivity(intent);
		}// end onClick.
	});

operationsMainTextView.setOnClickListener(new OnClickListener()
{
	@Override
	public void onClick(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
	   	intent.setClassName(MainActivity.this, PathSelectorActivity.class.getName());
	MainActivity.this.startActivity(intent);
		}// end onClick.
	});

	}// end onCreate.

// ** System events methods.

	/* (non-Javadoc)
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Method used for checking focus change.

		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
	    {
if(exploreMainTextView.isFocused())
{
operationsMainTextView.requestFocus();
}// end exploreMainTextView isFocused.
else if(operationsMainTextView.isFocused())
{
	exploreMainTextView.requestFocus();
}// end if operationsTextView isFocused
	    }// end if volume_down.

		return super.onKeyDown(keyCode, event);
	}// end onKeyDown.



}// end class.
