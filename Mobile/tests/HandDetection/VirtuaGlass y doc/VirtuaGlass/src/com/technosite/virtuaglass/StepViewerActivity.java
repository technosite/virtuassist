package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.technosite.virtuaglass.beans.Step;
import com.technosite.virtuaglass.utils.Utils;

public class StepViewerActivity extends Activity {

	private TextView operationNameTextView;
	private TextView descriptionStepTextView;

	private ImageView imageStepImageView;

	private List<Step> steps;
private int totalSteps;
	private int index;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	setContentView(R.layout.step_viewer);

	operationNameTextView = (TextView) findViewById(R.id.operationNameTextView);
	descriptionStepTextView = (TextView) findViewById(R.id.descriptionStepTextView);
	imageStepImageView = (ImageView) findViewById(R.id.imageStepImageView);

		// checking
	steps = new ArrayList<Step>();
							checkingData();

							if(steps != null)
							{
							Step step1 = steps.get(0);
if(step1 != null)
{
	totalSteps = step1.getTotalSteps();
	operationNameTextView.setText(step1.getOperationName()+" "+step1.getNumStep()+"/"+step1.getTotalSteps());
			descriptionStepTextView.setText(step1.getDescriptionStep());
							imageStepImageView.setImageBitmap(step1.getImageStep());

	// only in first step.
		Resources res = this.getResources();
		String text = res.getString(R.string.operations_instructions);
		long time = 5000;
		Utils.launchActivity(StepViewerActivity.this, NotificationViewerActivity.class.getName(), text, time);
}// end if step1.
							}// end if steps.

	}// end onCreate.

	// ** System events methods.

	// Detect trackpad events from GoogleGlass
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Code below is used for checking focus change.

		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
	    {
index--;
            	if(index >= 0)
            {
            	           updateData();
                       }// end if index 1.
            	else
            	{
            		index = 0;
            	}// end else index 0.
            }// end if volume_up
            else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                index++;
            	if(index < totalSteps)
{
	updateData();
}// end if.
            	else
            	{
            		index = totalSteps;
            		Intent intent = new Intent(Intent.ACTION_VIEW);
            	   	intent.setClassName(StepViewerActivity.this, PathFinishedActivity.class.getName());
            	startActivity(intent);
            		            }// end else.
}// end if volume_down
		         else if (keyCode == KeyEvent.KEYCODE_CAMERA && event.getAction() == KeyEvent.ACTION_DOWN) {
            //HANDLE CAMERA BUTTON EVENT
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            //HANDLE CLICK EVENT
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_TAB) { //glass and keyboards
            if (event.isShiftPressed()) { //left/reverse
                    //HANDLE SWIPE BACKWARDS

            	index--;
            	if(index >= 0)
            {
            	           updateData();
                       }// end if index 1.
            	else
            	{
            		index = 0;
            	}// end else index 1.
            } else { //right
                //HANDLE SWIPE FORWARD

            	index++;
            	if(index < totalSteps)
{
	updateData();
}// end if.
            	else
            	{
            		index = totalSteps;
            		Intent intent = new Intent(Intent.ACTION_VIEW);
            	   	intent.setClassName(StepViewerActivity.this, PathFinishedActivity.class.getName());
            	startActivity(intent);
            		            }// end else.

            }
        }
  


        return super.onKeyDown(keyCode, event);
    }// end onKeyDown.

	private void updateData()
	{
		Step step = steps.get(index);
        operationNameTextView.setText(step.getOperationName()+" "+step.getNumStep()+"/"+step.getTotalSteps());
		descriptionStepTextView.setText(step.getDescriptionStep());
						imageStepImageView.setImageBitmap(step.getImageStep());

	}// end updateData.

	// ** Helper methods.

private void checkingData()
{
	Resources res = this.getResources();
	Bitmap imageStep = BitmapFactory.decodeResource(res, R.drawable.instructionoperations);

	// Step: bean created for check purposes.
		Step step1 = new Step();
	step1.setOperationName("Operation 1");
			step1.setTotalSteps(3);
			step1.setNumStep(1);
			step1.setDescriptionStep("Description 1");
			step1.setImageStep(imageStep);
			steps.add(step1);

			Step step2 = new Step();
			step2.setOperationName("Operation 2");
					step2.setTotalSteps(3);
					step2.setNumStep(2);
					step2.setDescriptionStep("Description 2");
					step2.setImageStep(imageStep);
					steps.add(step2);

					Step step3 = new Step();
					step3.setOperationName("Operation 3");
							step3.setTotalSteps(3);
							step3.setNumStep(3);
							step3.setDescriptionStep("Description 3");
							step3.setImageStep(imageStep);
							steps.add(step3);

}// end checkingData.

}// end class.
