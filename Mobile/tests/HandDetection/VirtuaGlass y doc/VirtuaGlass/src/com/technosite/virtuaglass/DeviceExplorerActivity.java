package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

import com.technosite.virtuaglass.opencv.CameraView;
import com.technosite.virtuaglass.utils.Utils;

public class DeviceExplorerActivity extends Activity implements CvCameraViewListener2 {

    private Mat                  mRgba;

    private CameraView mOpenCvCameraView;

private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status)
        {
            switch (status)
            {
                case LoaderCallbackInterface.SUCCESS:
                {

                	mOpenCvCameraView.enableView();

                }// end case.
                break;
                default:
                {
                	super.onManagerConnected(status);
                }
            break;
            }// end switch.
        }// end onManagerConnected.
    };

    public DeviceExplorerActivity()
    {
          }// end constructor.


    @Override
	protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.device_explorer);

		Resources res = this.getResources();
		String text = res.getString(R.string.explore_instructions);
		long time = 5000;
		Utils.launchActivity(DeviceExplorerActivity.this, NotificationViewerActivity.class.getName(), text, time);
		
		mOpenCvCameraView = (CameraView) findViewById(R.id.handdetection_activity_java_surface_view);mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		mOpenCvCameraView.setCvCameraViewListener(this);

		// for checking
		mOpenCvCameraView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
			   	intent.setClassName(DeviceExplorerActivity.this, ControlDetailsActivity.class.getName());
			DeviceExplorerActivity.this.startActivity(intent);
						}// end onClick.

		});
    }// end onCreate.

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }// end onPause.

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }// end onResume.

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }// end onDestroy.

    // **CvCameraViewListener2 interface methods (OpenCV)

    @Override
	public void onCameraViewStarted(int width, int height)
	{

		if(android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT)
{
mOpenCvCameraView.googleGlassXE10WorkAround();
}// end if.

mRgba = new Mat();

	}// end onCameraViewStarted.


	@Override
	public void onCameraViewStopped()
	{

    	mRgba.release();

	}// end onCameraViewStopped.

    public Mat onCameraFrame(CvCameraViewFrame inputFrame)
    {

    mRgba = inputFrame.rgba();

    	return mRgba;
    }// end onCameraFrame 

}// end class.