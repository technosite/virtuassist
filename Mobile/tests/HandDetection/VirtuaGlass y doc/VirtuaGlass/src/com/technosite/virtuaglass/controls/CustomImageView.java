package com.technosite.virtuaglass.controls;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CustomImageView extends ImageView {

// ** Constructors
	
	public CustomImageView(Context context, AttributeSet attrs, int defStyle){
	    super(context, attrs,defStyle);
	}
	 
	public CustomImageView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	}
	 
	public CustomImageView(Context context) {
	    super(context);
	}
	
	// ** Size management
	
	public int currentWidth = 0;
	public int currentHeight = 0;
	public boolean resized = false;
	
    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        this.currentWidth = xNew;
        this.currentHeight = yNew;
        this.resized  = true;
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        }
    
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		if (!this.resized  ) {
			this.currentWidth = this.getWidth();
	        this.currentHeight = this.getHeight();
	        this.resized  = true;			
	         
		}
		
		}

}
