package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import android.content.Context;

import com.google.android.glass.timeline.LiveCard;

public class GlassListener extends LiveCard {

	public GlassListener(Context context, String tag) {
		super(context, tag);

	}// end constructor.


}// end class.
