package com.technosite.virtuaglass.model;

public class PDTPersistence {

	// Geography constants

	public static final int kpos_NONE = 0;
public static final int kpos_TOP = 2;
public static final int kpos_TOPRIGHT = 3;
public static final int kpos_RIGHT = 6;
public static final int kpos_BOTTOMRIGHT = 9;
public static final int kpos_BOTTOM = 8;
public static final int kpos_BOTTOMLEFT = 7;
public static final int kpos_LEFT = 4;
public static final int kpos_TOPLEFT = 1;
public static final int kpos_CENTER = 5;
}
