package com.technosite.virtuaglass.opencv;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

public class HandGesture {
	public List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
	public int cMaxId = -1;
	public Mat hie = new Mat();
	public List<MatOfPoint> hullP = new ArrayList<MatOfPoint>();
	public MatOfInt hullI = new MatOfInt();
	public Rect boundingRect;
	public MatOfInt4 defects = new MatOfInt4();
	
	public ArrayList<Integer> defectIdAfter = new ArrayList<Integer>();
	
	
	public List<Point> fingerTips = new ArrayList<Point>();
	public List<Point> fingerTipsOrder = new ArrayList<Point>();
	public Map<Double, Point> fingerTipsOrdered = new TreeMap<Double, Point>();
	public List<Point> mFingerTips = new ArrayList<Point>();
	
	public MatOfPoint2f defectMat = new MatOfPoint2f();
	public List<Point> defectPoints = new ArrayList<Point>();
	public Map<Double, Integer> defectPointsOrdered = new TreeMap<Double, Integer>();
	
	public Point palmCenter = new Point();
	public MatOfPoint2f hullCurP = new MatOfPoint2f();
	public MatOfPoint2f approxHull = new MatOfPoint2f();
	
	public MatOfPoint2f approxContour = new MatOfPoint2f();
	
	public MatOfPoint palmDefects = new MatOfPoint();
	
	public Point momentCenter = new Point();
	public double momentTiltAngle;
	
	public Point inCircle = new Point();

	public double inCircleRadius;
	
	public List<Double> features = new ArrayList<Double>();
	
	private boolean isHand = false;
	
	private float[] palmCircleRadius = {0};
	
	
	public void findBiggestContour() 
	{
		int idx = -1;
		int cNum = 0;
		
		for (int i = 0; i < contours.size(); i++)
		{
			int curNum = contours.get(i).toList().size();
			if (curNum > cNum) {
				idx = i;
				cNum = curNum;
			}
		}
		
		cMaxId = idx;
	}
	
	public boolean detectIsHand(Mat img)
	{
		int centerX = 0;
		int centerY = 0;
		if (boundingRect != null) {
			centerX = boundingRect.x + boundingRect.width/2;
			centerY = boundingRect.y + boundingRect.height/2;
		}
		if (cMaxId == -1)
			isHand = false;
		else if (boundingRect == null) {
			isHand = false;
		} else if ((boundingRect.height == 0) || (boundingRect.width == 0))
			isHand = false;
		else if ((centerX < img.cols()/4) || (centerX > img.cols()*3/4))
			isHand = false;
		else
			isHand = true;
		return isHand;
	}
	
	String feature2SVMString(int label)
	{
		String ret = Integer.toString(label) + " ";
		int i;
		for (i = 0; i < features.size(); i++)
		{
			int id = i + 1;
			ret = ret + id + ":" + features.get(i) + " ";
		}
		ret = ret + "\n";
		return ret;
	}
	
	public String featureExtraction(Mat img, int label)
	{
/////////////
//  Aqui hace su filtrado
		
/*		String ret = null;
		
//		mFingerTips.clear();
		if ((detectIsHand(img))) {
		
			
			defectMat.fromList(defectPoints);
			
			List<Integer> dList = defects.toList();
			Point[] contourPts = contours.get(cMaxId).toArray();
			Point prevDefectVec = null;
			int i;
			for (i = 0; i < defectIdAfter.size(); i++)
			{
				int curDlistId = defectIdAfter.get(i);
				int curId = dList.get(curDlistId);
				
				Point curDefectPoint = contourPts[curId];
				Point curDefectVec = new Point();
				curDefectVec.x = curDefectPoint.x - inCircle.x;
				curDefectVec.y = curDefectPoint.y - inCircle.y;
				
				if (prevDefectVec != null) {
					double dotProduct = curDefectVec.x*prevDefectVec.x +
							curDefectVec.y*prevDefectVec.y;
					double crossProduct = curDefectVec.x*prevDefectVec.y - 
							prevDefectVec.x*curDefectVec.y;
					
					if (crossProduct <= 0)
						break;
				}
				
				
				prevDefectVec = curDefectVec;
				
			}
			
			int startId = i;
			int countId = 0;
			
			ArrayList<Point> finTipsTemp = new ArrayList<Point>();
			
			if (defectIdAfter.size() > 0) {
				boolean end = false;
				
				for (int j = startId; ; j++)
				{
					if (j == defectIdAfter.size())
							{
						
						if (end == false) {
							j = 0;
							end = true;
						}
						else 
							break;
					}
					
					
					
					if ((j == startId) && (end == true))
						break;
					
					int curDlistId = defectIdAfter.get(j);
					int curId = dList.get(curDlistId);
					
					Point curDefectPoint = contourPts[curId];
					Point fin0 = contourPts[dList.get(curDlistId-2)];
					Point fin1 = contourPts[dList.get(curDlistId-1)];
					finTipsTemp.add(fin0);
					finTipsTemp.add(fin1);
					
					//Valid defect point is stored in curDefectPoint
					Core.circle(img, curDefectPoint, 2, new Scalar(0, 0, 255), -5);
				
					countId++;
				}
			
			}
			
			int count = 0;
			features.clear();
			for (int fid = 0; fid < finTipsTemp.size(); )
			{
				if (count > 5)
					break;
				
				Point curFinPoint = finTipsTemp.get(fid);
				
				if ((fid%2 == 0)) {
					
				    if (fid != 0) {
				    	Point prevFinPoint = finTipsTemp.get(fid-1);
						curFinPoint.x = (curFinPoint.x + prevFinPoint.x)/2;
						curFinPoint.y = (curFinPoint.y + prevFinPoint.y)/2;
				    }
					
					
					if (fid == (finTipsTemp.size() - 2) )
						fid++;
					else
						fid += 2;
				} else
					fid++;
				
				
				Point disFinger = new Point(curFinPoint.x-inCircle.x, curFinPoint.y-inCircle.y);
				double dis = Math.sqrt(disFinger.x*disFinger.x+disFinger.y*disFinger.y);
				Double f1 = (disFinger.x)/inCircleRadius;
				Double f2 = (disFinger.y)/inCircleRadius;
				features.add(f1);
				features.add(f2);
				
				//curFinPoint stores the location of the finger tip
				Core.line(img, inCircle, curFinPoint, new Scalar(24, 77, 9), 2);
				Core.circle(img, curFinPoint, 2, Scalar.all(0), -5);
				
				Core.putText(img, Integer.toString(count), new Point(curFinPoint.x - 10, 
									curFinPoint.y - 10), Core.FONT_HERSHEY_SIMPLEX, 0.5, Scalar.all(0));
//				mFingerTips.add(curFinPoint);
				
				count++;
							
			}
			
			ret = feature2SVMString(label);
			
		
		}
		
		return ret;
		*/
		return "";
	}
	
	
	
	public native double findInscribedCircleJNI(long imgAddr, double rectTLX, double rectTLY,
			double rectBRX, double rectBRY, double[] incircleX, double[] incircleY, long contourAddr);
	
	public void findInscribedCircle(Mat img)
	{
		
		Point tl = boundingRect.tl();
		Point br = boundingRect.br();
		
		double[] cirx = new double[]{0};
		double[] ciry = new double[]{0};
		
		inCircleRadius = findInscribedCircleJNI(img.getNativeObjAddr(), tl.x, tl.y, br.x, br.y, cirx, ciry, 
				approxContour.getNativeObjAddr());
		inCircle.x = cirx[0];
		inCircle.y = ciry[0];
		
		Core.circle(img, inCircle, (int)inCircleRadius, new Scalar(240,240,45,0), 2);
		Core.circle(img, inCircle, 3, Scalar.all(0), -2);
	}

	
	
	public void drawFingerTips(Mat img)
	{
//		mFingerTips = filterClosePoints(mFingerTips, distanceX, distanceY);
		
		Scalar[] color = new Scalar[3];
		color[0] = new Scalar(255,0,0,0);
		color[1] = new Scalar(0,255,0,0);
		color[2] = new Scalar(0,0,255,0);
		Scalar[] color2 = new Scalar[6];
		color2[0] = new Scalar(255,255,0,0);
		color2[1] = new Scalar(0,255,255,0);
		color2[2] = new Scalar(255,0,255,0);
		color2[3] = new Scalar(255,255,128,0);
		color2[4] = new Scalar(128,255,255,0);
		color2[5] = new Scalar(255,128,255,0);

		// Se pintan en rojo todos los radios
		drawVectors(img, mFingerTips, inCircle, color[0]);
		
		
		double closeRadius = Math.pow(inCircleRadius * 1.5, 2);
		double farRadius   = Math.pow(inCircleRadius * 4.5, 2);
		double minAngle    = 315;
		double maxAngle    = 135;
		
		List<Point>         tempFingerTips = filterTipsByDistance (mFingerTips, farRadius, closeRadius);
		// Se pintan en verde los radios filtrados por la longitud
		drawVectors(img, tempFingerTips, inCircle, color[1]);
		List<Double>        angles         = calculateAngles (tempFingerTips, inCircle, true);
		double[][]          mAngles        = calculateSmallestAngleBetwenFingerTips (angles);
		List<List<Integer>> listClusters   = clusterizeByAngles (mAngles);

		if (!listClusters.isEmpty())
		{
			int c = 0;
			for (List<Integer> li : listClusters)
			{
				List<Point> tempFingerTips2 = new ArrayList<Point>();
				for (int i : li)
				{
					if (i < tempFingerTips.size())
						tempFingerTips2.add(tempFingerTips.get(i));
				}
				// Se pintan en distintos colores los diferentes clusters
				drawVectors(img, tempFingerTips2, inCircle, color2[c % 6]);
				c++;
			}
		}
		
		listClusters = filterClustersByDirectorAngle(listClusters, tempFingerTips, inCircle, minAngle, maxAngle);
		
		if (!listClusters.isEmpty())
		{
			int id = 0;
			for (List<Integer> li : listClusters)
			{
				List<Point> tempFingerTips2 = new ArrayList<Point>();
				for (int i : li)
				{
					if (i < tempFingerTips.size())
						tempFingerTips2.add(tempFingerTips.get(i));
				}
				tempFingerTips.clear();
				tempFingerTips = tempFingerTips2;
				mFingerTips = tempFingerTips;
				// Se pintan en azul los vertices que estan en el angulo deseado
//				drawVectors(img, mFingerTips, inCircle, color[2]);

				for (Point p : mFingerTips)
					Core.circle(img, p, 10, color[id % 3], -5);
				id++;
			}
		}
	}
	
	private List<Point> filterClosePoints(List<Point> fingerTips, int distanceX, int distanceY)
	{
		List<Point> out = new ArrayList<Point>();
		
		for (Point p : fingerTips)
		{
			for (Point p2 : out)
			{
				
			}
		}
		return out;		
	}
	
	private void drawVectors(Mat img, List<Point> fingerTips, Point origin, Scalar color)
	{
		for (Point p : fingerTips)
			Core.line(img, origin, p, color, 2);
	}
	
		
	private List<Point> filterTipsByDistance (List<Point> fingerTips, double farRadius, double closeRadius)
	{
		List<Point> out = new ArrayList<Point>();
		
		// Filtrado por la longitud solo pasan los mayores de 2*r y menores de 3*r
		for (Point p : mFingerTips)
		{
			double length = Math.pow(p.x - inCircle.x, 2) + Math.pow(p.y - inCircle.y, 2);
			if (length > closeRadius/* && length < farRadius*/)
				out.add(p);
		}
		return out;
	}
	
	private List<Double> calculateAngles(List<Point> fingerTips, Point origin, boolean normalize)
	{
		List<Double> out = new ArrayList<Double>();
		for (Point p : fingerTips)
		{
			double angle = 180 * Math.atan2(origin.y - p.y, origin.x - p.x) / Math.PI;
			if (normalize)
				angle = angle < 0 ? angle + 360 : angle > 360 ? angle - 360 : angle;
			out.add(angle);
		}
		return out;
	}
	
	private double[][] calculateSmallestAngleBetwenFingerTips (List<Double> angles)
	{
		int anglesSize = angles.size();
		double[][] mAngles = new double[anglesSize][anglesSize];
		for (int i = 0; i < anglesSize;i++)
		{
			for (int j = i + 1; j < anglesSize;j++)
			{
				double angle = Math.abs(angles.get(i) - angles.get(j));
				double angle2 = Math.abs(360 - angle);
				angle = angle < angle2 ? angle : angle2;
				mAngles[i][j] = mAngles[j][i] = angle;
			}
		}
		return mAngles;
	}
	
	private List<List<Integer>> clusterizeByAngles(double[][] mAngles)
	{
		double groupingAngle = 50;
		
		List<List<Integer>> out = new ArrayList<List<Integer>>();
		out.add(new ArrayList<Integer>());
		if (mAngles.length > 0 && mAngles[0].length > 0)
		{
			out.get(0).add(0);
			
			List<Integer> pendingTips = new ArrayList<Integer>();
			for (int i = 1; i < mAngles[0].length; i++)
				pendingTips.add(i);
			
			while (!pendingTips.isEmpty())
			{
				List<Integer> addedTips = new ArrayList<Integer>();
				for (Integer i : pendingTips)
				{
					for (List<Integer> li : out)
					{
						boolean added = false;
						for (Integer i2 : li)
						{
							int min = i <  i2 ? i : i2;
							int max = i >= i2 ? i : i2;
							if (mAngles[min][max] < groupingAngle)
							{
								li.add(i);
								addedTips.add(i);
								added = true;
								break;
							}
						}
						if (added) break;
					}
				}
				if (addedTips.isEmpty())
				{
					List<Integer> newCluster = new ArrayList<Integer>();
					newCluster.add(pendingTips.get(0));
					pendingTips.remove(0);
					out.add(newCluster);
				}
				else
				{
					for(Integer i : addedTips)
					{
						pendingTips.remove(i);
					}
				}
			}
		}
		return out;
	}

	private List<List<Integer>> filterClustersByDirectorAngle(List<List<Integer>> clusters, List<Point> fingerTips, Point origin, double minAngle, double maxAngle)
	{
		boolean ordered = minAngle < maxAngle;
		List<List<Integer>> out = new ArrayList<List<Integer>>();
		for (List<Integer> li : clusters)
		{
			List<Point> clusterFingerTips = new ArrayList<Point>();
			for (Integer i : li)
				clusterFingerTips.add(fingerTips.get(i));
			
/*			List<Double> angles = calculateAngles (clusterFingerTips, origin, false);
			
			double directorAngle = 0;
			for (double d : angles)
				directorAngle += d;
			directorAngle /= angles.size();
			while (directorAngle > 360)
				directorAngle -= 360;
			while (directorAngle < 0)
				directorAngle += 360;
*/
			Point p = new Point(0, 0);
			for (Point p2 : clusterFingerTips)
			{
				p.x += p2.x;
				p.y += p2.y;
			}
			p.x /= clusterFingerTips.size();
			p.y /= clusterFingerTips.size();
			double directorAngle = 180 * Math.atan2(origin.y - p.y, origin.x - p.x) / Math.PI;
			directorAngle = directorAngle < 0 ? directorAngle + 360 : directorAngle > 360 ? directorAngle - 360 : directorAngle;
			
			if (ordered)
			{
				if (directorAngle >= minAngle && directorAngle <= maxAngle)
					out.add(li);
			}
			else
			{
				if (directorAngle >= minAngle || directorAngle <= maxAngle)
					out.add(li);
			}
		}
		return out;
	}

	
}
