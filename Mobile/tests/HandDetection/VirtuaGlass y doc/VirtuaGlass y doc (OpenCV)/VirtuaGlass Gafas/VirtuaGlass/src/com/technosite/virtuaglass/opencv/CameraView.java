package com.technosite.virtuaglass.opencv;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import org.opencv.android.JavaCameraView;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;

public class CameraView extends JavaCameraView
{

public CameraView(Context context, int cameraId)
{
	super(context, cameraId);
}
	
    public CameraView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }// end constructor.

    public void googleGlassXE10WorkAround()
    {

    	Camera.Parameters params = mCamera.getParameters();
 params.setPreviewFpsRange(30000, 30000);
        params.setPreviewSize(640,360);

    	params.setAutoExposureLock(true);
        params.setAutoWhiteBalanceLock(true);
                mCamera.cancelAutoFocus();

        mCamera.setParameters(params);

    }// end googleGlassXE10WorkAround
}// end class.
