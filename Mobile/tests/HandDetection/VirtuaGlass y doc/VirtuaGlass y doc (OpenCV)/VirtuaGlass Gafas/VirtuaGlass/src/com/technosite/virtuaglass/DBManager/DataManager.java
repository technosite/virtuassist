package com.technosite.virtuaglass.DBManager;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.ksoap2.serialization.SoapObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.technosite.virtuaglass.ALog.ALog;
import com.technosite.virtuaglass.model.Globals;
import com.technosite.virtuaglass.model.PDTControl;
import com.technosite.virtuaglass.model.PDTDevice;
import com.technosite.virtuaglass.model.PDTManager;
import com.technosite.virtuaglass.model.PDTPath;
import com.technosite.virtuaglass.model.PDTStep;
import com.technosite.virtuaglass.model.PDTSurface;
import com.technosite.virtuaglass.model.PDTTypes;


public class DataManager {
	private Globals globals = null;

	
	public DataManager() {
		// TODO Auto-generated constructor stub
		globals = Globals.getSharedInstance();
	}
	
	private static DataManager _instance = null;
	
	public static DataManager getSharedInstance() {
		if (_instance == null) _instance = new DataManager();
		return _instance;
	}
	
	// ** Users
	
	public boolean saveNewUser(String userName,String userPassword, String userMail, int uText, int uImage, int uVideo) {
		Log.w("Datamanager method","Saving a user with data:(" +userName + "," + userPassword +"," + uText+"," + uImage+"," + uVideo + ")");
		
// save a new user in the data base
		boolean result = false;
		SoapManager soap = new SoapManager();
		soap.createCommunicationWithMethod("registrationRequest", "http://accesibilizadorpdt.org/registrationRequest" );
		soap.addParameter("username", userName);
		soap.addParameter("password", userPassword);
		soap.addParameter("mail", userMail);
		soap.addParameter("prefText", uText);
		soap.addParameter("prefImage", uImage);
		soap.addParameter("prefVideo", uVideo);
		soap.execute();
		SoapResult r = soap.getResult();
		
		if (!r.getString(0).equalsIgnoreCase("error")) {
result = true;			
		}
		return result;
	}
	
	public boolean checkUserNameAvailability(String userName) {
		Log.w("Datamanager method","Checking availability for a user name");
		// checks if the user is not in the data base
		boolean result = false;
		// get data from database
			result = true;
		
		return result;
	}
	
	public boolean logInUser(String userName, String userPassword) {
		Log.w("Datamanager method","Login user");
// checks if user and password are right
		boolean result = false;

			// get data from database
			SoapManager soap = new SoapManager();
			soap.createCommunicationWithMethod("loginRequest", "http://accesibilizadorpdt.org/loginRequest" );
			soap.addParameter("username", userName);
			soap.addParameter("password", userPassword);
			soap.execute();
			SoapResult r = soap.getResult();
			
			if (!r.getString("user").equals("null")) {
				result = true;
				Log.w("Login ok", r.toString());
				boolean pT = false;
				boolean pI = false;
				boolean pV = false;
				if (r.data.getProperty(2).toString().equalsIgnoreCase("1")) pT = true;
				if (r.data.getProperty(3).toString().equalsIgnoreCase("1")) pI = true;
				if (r.data.getProperty(4).toString().equalsIgnoreCase("1")) pV = true;
				
				globals.logInUser(r.data.getProperty(1).toString(), pT, pI, pV);
				// debug for demo
				// globals.logInUser(r.data.getProperty(1).toString(), true, true, true);
			} else Log.e("Login error", "Wrong login data:" + r.toString());
		
		return result;
	}
	
	// ** Device
	
	public PDTDevice getDeviceforCode(String code) {
		Log.w("Datamanager method","Getting a device from QRCode");
// gets the device for a specific QR code string from data base
		PDTDevice result = null;
		

// Get data from database
			SoapManager soap = new SoapManager();
			soap.createCommunicationWithMethod("infoDeviceRequest", "http://accesibilizadorpdt.org/infoDeviceRequest" );
			soap.addParameter("qrID", code);
			soap.execute();
			SoapResult r = soap.getResult();
						if (!r.isErrorQweryWithField()) {
				result = new PDTDevice(r.getInteger("id"), r.getString("name"), r.getString("description"), r.getInteger("category"), r.getString("qrID"));				
			}
		return result;
	}
	
	public ArrayList<PDTControl> getControlsForSurface(int id) {
		Log.w("Datamanager method","Getting controls from a surface");
		ArrayList<PDTControl> result = null;
// PDTControl(int pId, String pName, String pDescription, int pType, int pFigure, int pWidth, int pHeight, int pX, int pY, int pRadius, String pColor, String pBorderColor, int pLineBorder , String pText, int pFontSize, String pFontFamily, Object pImage,  PDTSurface pSurface)

// get data from database
		SoapManager soap = new SoapManager();
	soap.createCommunicationWithMethod("controlRequest", "http://accesibilizadorpdt.org/controlRequest" );
	soap.addParameter("surfaceId", id);
	soap.execute();
	SoapResult r = soap.getResult();
			
	if (!r.isErrorQweryWithField()) {
		result = new ArrayList<PDTControl>();
		for (int i=2;i<r.size();i++) {
			SoapResult o = new SoapResult((SoapObject) r.data.getProperty(i));
			
			result.add(new PDTControl(o.getInteger(0), o.getString(1), o.getString(15), o.getInteger(2), o.getInteger(3), o.getInteger(9), o.getInteger(10), o.getInteger(7), o.getInteger(8), o.getInteger(11), o.getString(4), o.getString(5), o.getInteger(6), o.getString(12), o.getInteger(13), o.getString(14), null, null));
		}
	}

return result;
	}
	
	public ArrayList<PDTSurface> getSurfaceForId(int id) {
		Log.w("Datamanager method","Getting surfaces from a device");
		ArrayList<PDTSurface> result = null;
			// get data from database
			String m = "surfaceRequest";      
			SoapManager soap = new SoapManager();
			soap.createCommunicationWithMethod(m, "http://accesibilizadorpdt.org/" + m);
			soap.addParameter("deviceId", id);  
					soap.execute();
			SoapResult r = soap.getResult();
			if (!r.isErrorQweryWithField()) {
				result = new ArrayList<PDTSurface>();
				for (int i=2;i<r.size();i++) {
					SoapResult o = SoapManager.createResult((SoapObject) r.get(i));
					result.add(new PDTSurface(o.getInteger(0), o.getString(1), o.getInteger(2), o.getInteger(3), o.getInteger(4), null, o.getInteger(5), null));
				}
			}
				return result;
	}
	
	public Object getImageForSurfaceId(int id) {
		Log.w("Datamanager method","Getting a image from surface" + id);
		Object result = null;
		
			// Get data from database
			String m = "imageFromSurfaceRequest";     
			SoapManager soap = new SoapManager();
			soap.createCommunicationWithMethod(m, "http://accesibilizadorpdt.org/" + m);
			soap.addParameter("surface_ID", id);  
					soap.execute();
			SoapResult r = soap.getResult();
			Bitmap bMap = null;
			String stringResult = r.data.getProperty("image").toString();
			if (stringResult ==null) ALog.e("Error getting a image for a surface", "Error parsing the image for surface " + id +": no data in the string");
			byte[] arrayResult = null;
			try {
				arrayResult = stringResult.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e) {
				ALog.e("UnsupportedEncodingException loading a image from cloud", "Error decoding buffer: " +e);
			}
			
			byte[] b = null;
			try {
												
				b= Base64.decode(arrayResult, arrayResult.length);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
				bMap = BitmapFactory.decodeByteArray(b, 0, b.length, options);
				
												
			}
						catch (Exception e) {
								e.printStackTrace();
			}
			
			
						if (bMap!=null) result = bMap;
						else {
							Log.w("Getting image from cloud", "The image shoud be converted to PNG format");
							try {
								ByteArrayOutputStream baos = new ByteArrayOutputStream(b.length);
								baos.write(b, 0, b.length);
								 // bMap = new Bitmap(null);
								bMap = BitmapFactory.decodeByteArray(b, 0, b.length);
								
								bMap.compress(Bitmap.CompressFormat.PNG, 100, baos);
							} catch (Exception epng) {
								Log.e("PNG conversion", "Error converting to PNG format: " +epng);
							       epng.printStackTrace();
							}
							result = stringResult;							
						}
							
		return result;
			}
	
	public boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        return true;
	    }
	    return false;
	}
	
	public ArrayList<PDTPath> getPathForId(int id) {
		Log.w("Datamanager method","Getting paths from a device");
		ArrayList<PDTPath> result = null;
			// get data from database
			String m = "pathRequest";      
			SoapManager soap = new SoapManager();
			soap.createCommunicationWithMethod(m, "http://accesibilizadorpdt.org/" + m);
			soap.addParameter("deviceId", id);  
					soap.execute();
			SoapResult r = soap.getResult();
			if (!r.isErrorQweryWithField()){
				result = new ArrayList<PDTPath>();
				for (int i=2;i<r.size();i++) {
					SoapResult o = new SoapResult( (SoapObject) r.get(i));
					result.add(new PDTPath(o.getInteger(0),o.getString(1), o.getString(2), null));					
				}
			}
				return result;
	}
	
	public ArrayList<PDTStep> getStepForPath(int id) {
		Log.w("Datamanager method","Getting steps");
		ArrayList<PDTStep>result = null;
			// get data from database
			String m = "stepRequest";      
			SoapManager soap = new SoapManager();
			soap.createCommunicationWithMethod(m, "http://accesibilizadorpdt.org/" + m);
			soap.addParameter("pathId", id);  
					soap.execute();
			SoapResult r = soap.getResult();
			if (!r.isErrorQweryWithField()) {
				result = new ArrayList<PDTStep>();
for (int i=2;i<r.size();i++) {
SoapResult o = new SoapResult((SoapObject) r.get(i));
result.add(new PDTStep(o.getInteger(0),o.getInteger(3),o.getInteger(2),o.getString(4),o.getInteger(1), null, null));
}
			}
				return result;
	}

	// ** Main loading function
	
	public boolean loadDeviceWithQRCode(String code) {
		Log.w("Datamanager method","Load a device from a QRCode");
	
		boolean result = true;
		PDTManager manager = PDTManager.getSharedInstance();
		PDTDevice dev = null;
			// get data from dataBase
						dev = getDeviceforCode(code);
			
if(dev != null)
{		
	dev.addSurfaces(getSurfaceForId(dev.id));
			for (int i=0;i<dev.surfaces.size();i++) {
PDTSurface sItem = dev.surfaces.get(i);
sItem.addControls(getControlsForSurface(sItem.id));
			}
			
			dev.addPaths(getPathForId(dev.id));
			for (int i=0;i<dev.paths.size();i++) {
PDTPath sItem = dev.paths.get(i);

sItem.addSteps(getStepForPath(sItem.id));
			}
			
			manager.device = dev;
}// end if dev != null.
else
{
	result = false;
}// end else.
			
return result;
	}
	
	
	// ** Type tables management
	
	public PDTTypes loadTypeTable(String tableName) {
		
PDTTypes result = new PDTTypes(tableName);
	// get data from database
	String m = null;
	if (tableName.equals("typeusers")) {
result.add(1,"User");
result.add(2,"Editor");
result.add(3,"Administrator");
	} else if (tableName.equals("categories")) {
		m ="passwordRequest5";
	} else if (tableName.equals("typesurfaces")) {
		m ="passwordRequest2";
		} else if (tableName.equals("typecontrols")) {
			m ="passwordRequest";
	} else if (tableName.equals("typesteps")) {
		m ="passwordRequest4";
	} else if (tableName.equals("typefigures")) {
		m ="passwordRequest3";				
	} else {
		ALog.e("Type tables creation", "Table " +tableName+ " not found.");
	}
	
	if (m!=null) {
		SoapManager soap = new SoapManager();
		soap.createCommunicationWithMethod(m, "http://accesibilizadorpdt.org/" + m);
		soap.addParameter("ID", 1);
				soap.execute();
		SoapResult r = soap.getResult();
		SoapObject rr = r.data;
		if (rr.getProperty(0)==null) {
			for (int i=1;i<r.size();i++) {
				SoapObject item = (SoapObject) rr.getProperty(i);
								result.add(Integer.valueOf(item.getProperty(0).toString()),item.getProperty(1).toString());
			}
		
	}
}

return result;
	}

}
