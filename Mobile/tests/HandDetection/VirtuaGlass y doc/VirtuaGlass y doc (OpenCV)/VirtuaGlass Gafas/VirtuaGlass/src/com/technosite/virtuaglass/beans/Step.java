package com.technosite.virtuaglass.beans;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import android.graphics.Bitmap;

public class Step {

private String operationName;
private int totalSteps;
private int numStep;
private Bitmap imageStep;
private String descriptionStep;


	public Step() {
		}// end constructor.


	/**
	 * @return the operationName
	 */
	public String getOperationName() {
		return operationName;
	}


	/**
	 * @param operationName the operationName to set
	 */
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}


	/**
	 * @return the numStep
	 */
	public int getNumStep() {
		return numStep;
	}


	/**
	 * @param numStep the numStep to set
	 */
	public void setNumStep(int numStep) {
		this.numStep = numStep;
	}


	/**
	 * @return the imageStep
	 */
	public Bitmap getImageStep() {
		return imageStep;
	}


	/**
	 * @param imageStep the imageStep to set
	 */
	public void setImageStep(Bitmap imageStep) {
		this.imageStep = imageStep;
	}


	/**
	 * @return the descriptionStep
	 */
	public String getDescriptionStep() {
		return descriptionStep;
	}


	/**
	 * @param descriptionStep the descriptionStep to set
	 */
	public void setDescriptionStep(String descriptionStep) {
		this.descriptionStep = descriptionStep;
	}


	/**
	 * @return the totalSteps
	 */
	public int getTotalSteps() {
		return totalSteps;
	}


	/**
	 * @param totalSteps the totalSteps to set
	 */
	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

}// end class.
