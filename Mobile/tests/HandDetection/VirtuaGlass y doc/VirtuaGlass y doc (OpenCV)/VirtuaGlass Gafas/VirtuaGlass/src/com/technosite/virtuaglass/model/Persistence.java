package com.technosite.virtuaglass.model;

public class Persistence {
	
	public static final String kUser = "user";

	// ** Preferences
	public static final String kTextPreference = "prefText";
	public static final String kImagePreference = "prefImage";
	public static final String kVideoPreference = "prefVideo";
	public static final String kSpeechPreference = "prefSpeech";
	
	
	public static final String kInfoSteps = "infoAboutSteps";
	public static final String kInfoSurfaces = "infoAboutSurfaces";
	
	public static final String kLastDevice = "lastDevice";
	public static final String kDataOfStoredDevice = "binaryArrayOfDevice";

}
