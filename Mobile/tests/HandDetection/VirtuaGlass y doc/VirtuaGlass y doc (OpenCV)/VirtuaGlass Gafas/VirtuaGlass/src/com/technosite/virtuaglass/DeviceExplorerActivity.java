package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

import com.technosite.virtuaglass.opencv.CameraView;
import com.technosite.virtuaglass.opencv.HandGesture;
import com.technosite.virtuaglass.opencv.PontsManager;
import com.technosite.virtuaglass.utils.Utils;

public class DeviceExplorerActivity extends Activity implements CvCameraViewListener2 {


    private static final String    TAG = "OCVSample::Activity";

    private static final int       VIEW_MODE_RGBA     = 0;
    private static final int       VIEW_MODE_GRAY     = 1;
    private static final int       VIEW_MODE_CANNY    = 2;
    private static final int       VIEW_MODE_FEATURES = 5;

    private static final int       FEATURES_STATE_CALIBRATE_BG   = 0;
    private static final int       FEATURES_STATE_CALIBRATE_HAND = 1;
    private static final int       FEATURES_STATE_TEST           = 2;

    private int                    mFeaturesState = FEATURES_STATE_CALIBRATE_BG;

    private int                    mViewMode;
    private Mat                    mRgba;
    private Mat                    mIntermediateMat;
    private Mat                    mGray;
    private Mat                    mRgb;
    private Mat                    mOut;
    private Mat                    mOut2;

    private Mat                    binMat;

    private Mat                    sampleHandMats[];
    private Mat                    sampleBackgroundMats[];

	private Scalar                 lowerBound;
	private Scalar                 upperBound;

    private MenuItem               mItemPreviewRGBA;
    private MenuItem               mItemPreviewGray;
    private MenuItem               mItemPreviewCanny;
    private MenuItem               mItemPreviewFeatures;

//    private CameraBridgeViewBase   mOpenCvCameraView;
    private CameraView   mOpenCvCameraView;

	private HandGesture hg = null;

	// Parte de la inicializaci�n de OpenCV
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("VirtuaGlass");

                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public DeviceExplorerActivity()
    {
          }// end constructor.


    @Override
	protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.device_explorer);

		Resources res = this.getResources();
		String text = res.getString(R.string.focus_printer);
		long time = 5000;
		Utils.launchActivity(DeviceExplorerActivity.this, NotificationViewerActivity.class.getName(), text, time);
mViewMode = VIEW_MODE_FEATURES;

		mOpenCvCameraView = (CameraView) findViewById(R.id.handdetection_activity_java_surface_view);mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		mOpenCvCameraView.setCvCameraViewListener(this);

		// for checking
		mOpenCvCameraView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
			   	intent.setClassName(DeviceExplorerActivity.this, ControlDetailsActivity.class.getName());
			DeviceExplorerActivity.this.startActivity(intent);
						}// end onClick.

		});
    
    }// end onCreate.
   
    
    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

 // **CvCameraViewListener2 interface methods (OpenCV)

    
    public void onCameraViewStarted(int width, int height) {
//    	if(android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT)
//    	{
    	 mOpenCvCameraView.googleGlassXE10WorkAround();
    	//}// end if.

    	mRgba = new Mat(height, width, CvType.CV_8UC4);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
        mRgb  = new Mat();
        mOut  = new Mat();
        mOut2 = new Mat();
        
        binMat = new Mat();
        
        PontsManager pm = PontsManager.getSingleton();
        sampleBackgroundMats = new Mat[pm.BACKGROUND_POINTS];
        for (int i = 0; i < pm.BACKGROUND_POINTS;i++)
        	sampleBackgroundMats[i] = new Mat();
        sampleHandMats = new Mat[pm.HAND_POINTS];
        for (int i = 0; i < pm.HAND_POINTS;i++)
        	sampleHandMats[i] = new Mat();
        
		lowerBound = new Scalar(0,0,0,0);
		upperBound = new Scalar(0,0,0,0);
		
		if (hg == null)
		hg = new HandGesture();
    
    }// end onCameraViewStarted.

    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
        mIntermediateMat.release();
    }// end onCameraViewStopped.
    

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        final int viewMode = mViewMode;
        switch (viewMode) {
        case VIEW_MODE_GRAY:
            // input frame has gray scale format
            Imgproc.cvtColor(inputFrame.gray(), mRgba, Imgproc.COLOR_GRAY2RGBA, 4);
            break;
        case VIEW_MODE_RGBA:
            // input frame has RBGA format
            mRgba = inputFrame.rgba();
            break;
        case VIEW_MODE_CANNY:
        {
            // input frame has gray scale format
            mRgba = inputFrame.rgba();
            
            Mat mBgr  = new Mat();
            Mat mHsv  = new Mat();
            
// En java no hay metodo directo para pasar de RGNA a HSV
            Imgproc.cvtColor(mRgba, mBgr, Imgproc.COLOR_RGBA2BGR    , 0);
            Imgproc.cvtColor(mBgr , mHsv, Imgproc.COLOR_BGR2HSV, 0);
            
            List<Mat> mv = new ArrayList<Mat>();
            Core.split(mHsv, mv);

            Imgproc.cvtColor(mv.get(0), mRgba, Imgproc.COLOR_GRAY2RGBA, 0);

            return processMaterial();
            
/*            Imgproc.Canny(inputFrame.gray(), mIntermediateMat, 80, 100);
            Imgproc.cvtColor(mIntermediateMat, mRgba, Imgproc.COLOR_GRAY2RGBA, 4);
*/  //          break;
        }
        case VIEW_MODE_FEATURES:
        {
            // input frame has RGBA format
            mRgba = inputFrame.rgba();
            mGray = inputFrame.gray();

// La matriz de salida tiene que estar a la misma resoluci�n de la de entrada si no se queda todo negro
            
	        return processMaterial();
            
//            FindFeatures(mGray.getNativeObjAddr(), mRgba.getNativeObjAddr());
//            break;
        }
        }

        return mRgba;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);

        if (item == mItemPreviewRGBA) {
            mViewMode = VIEW_MODE_RGBA;
        } else if (item == mItemPreviewGray) {
            mViewMode = VIEW_MODE_GRAY;
        } else if (item == mItemPreviewCanny) {
            mViewMode = VIEW_MODE_CANNY;
        } else if (item == mItemPreviewFeatures) {
            mViewMode = VIEW_MODE_FEATURES;
        }

        return true;
    }
    
    
    
    
    
    private void handsAddition (Mat imgIn, Mat imgOut)
    {
    	PontsManager pm = PontsManager.getSingleton();
    	
		for (int i = 0; i < pm.HAND_POINTS; i++)
		{
			lowerBound.set(new double[]{pm.avgColor[i][0]-pm.cLower[i][0], pm.avgColor[i][1]-pm.cLower[i][1],
					pm.avgColor[i][2]-pm.cLower[i][2]});
			upperBound.set(new double[]{pm.avgColor[i][0]+pm.cUpper[i][0], pm.avgColor[i][1]+pm.cUpper[i][1],
					pm.avgColor[i][2]+pm.cUpper[i][2]});			
			
			Core.inRange(imgIn, lowerBound, upperBound, sampleHandMats[i]);
		}
		
		imgOut.release();
		sampleHandMats[0].copyTo(imgOut);
	
		for (int i = 1; i < pm.HAND_POINTS; i++)
			Core.add(imgOut, sampleHandMats[i], imgOut);
		
		Imgproc.medianBlur(imgOut, imgOut, 3);    	
    }
    
    private void backgroundAddition (Mat imgIn, Mat imgOut)
    {
    	PontsManager pm = PontsManager.getSingleton();
		for (int i = 0; i < pm.BACKGROUND_POINTS; i++)
		{
			lowerBound.set(new double[]{pm.avgBackColor[i][0]-pm.cBackLower[i][0], pm.avgBackColor[i][1]-pm.cBackLower[i][1],
					pm.avgBackColor[i][2]-pm.cBackLower[i][2]});
			upperBound.set(new double[]{pm.avgBackColor[i][0]+pm.cBackUpper[i][0], pm.avgBackColor[i][1]+pm.cBackUpper[i][1],
					pm.avgBackColor[i][2]+pm.cBackUpper[i][2]});			
			
			Core.inRange(imgIn, lowerBound, upperBound, sampleBackgroundMats[i]);
		}
		
		imgOut.release();
		sampleBackgroundMats[0].copyTo(imgOut);
		
		for (int i = 1; i < pm.BACKGROUND_POINTS; i++)
			Core.add(imgOut, sampleBackgroundMats[i], imgOut);
		
		Core.bitwise_not(imgOut, imgOut);
		Imgproc.medianBlur(imgOut, imgOut, 3);    	
    }
    
    private Mat processMaterial()
    {
        // Se introduce un suavizado
		Imgproc.GaussianBlur(mRgba, mRgba, new Size(5,5), 5, 5);

        PontsManager pm = PontsManager.getSingleton();
        pm.initialize(mRgba);
        
		if (mFeaturesState == FEATURES_STATE_TEST)
		{
            Imgproc.cvtColor(mRgba, mRgb, Imgproc.COLOR_RGBA2RGB, 0);
	        
            handsAddition(mRgb, mOut);
            backgroundAddition(mRgb, mOut2);
            
            Core.bitwise_and(mOut, mOut2, mOut);

    		binMat.release();
    		mOut.copyTo(binMat);
    		
    		Rect r = makeBoundingBox(binMat);
    		
    		if (r != null)
    		{
	    		makeContours();
	    		hg.featureExtraction(mRgba, 0);
	    		hg.drawFingerTips(mRgba);
    		}
            
	        Imgproc.cvtColor(mOut, mOut, Imgproc.COLOR_GRAY2RGBA, 0);
	        
			return mRgba;
		}
		else
		{
			mOut.release();
			mRgba.copyTo(mOut);
		}
		
		if (mFeaturesState == FEATURES_STATE_CALIBRATE_BG)
	        pm.drawBackgroundSamplePoints(mOut);
		else if (mFeaturesState == FEATURES_STATE_CALIBRATE_HAND)
	        pm.drawHandSamplePoints(mOut);
		
        return mOut;
    }
    
    void makeContours()
	{
    	// Se limpian los contornos calculados previamente
		hg.contours.clear();
		// Se calculan unos nuevos contornos a partir de la imagen binaria
		Imgproc.findContours(binMat, hg.contours, hg.hie, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
		
		// Se calcula cual de todos los contornos es el mayor
		hg.findBiggestContour();

		if (hg.cMaxId > -1) {
			
			// Se coge el contorno se simplifica y se vuelve a guardar
			hg.approxContour.fromList(hg.contours.get(hg.cMaxId).toList());
			Imgproc.approxPolyDP(hg.approxContour, hg.approxContour, 2, true);
			hg.contours.get(hg.cMaxId).fromList(hg.approxContour.toList());
			
			// Se pinta el contorno
			Imgproc.drawContours(mRgba, hg.contours, hg.cMaxId, new Scalar(255,0,0,0), 1);
			
			//Se calcula el circulo inscrito que representar� la palma de la mano
			hg.findInscribedCircle(mRgba);
			

			// A partir del controno se calcula la caja que lo contiene
			hg.boundingRect = Imgproc.boundingRect(hg.contours.get(hg.cMaxId));
			
			// Se calcula el poligono convexo m�s aproximado a partir del contorno
			Imgproc.convexHull(hg.contours.get(hg.cMaxId), hg.hullI, false);
			
			hg.hullP.clear();
			for (int i = 0; i < hg.contours.size(); i++)
				hg.hullP.add(new MatOfPoint());
			
			int[] cId = hg.hullI.toArray();
			List<Point> lp = new ArrayList<Point>();
			Point[] contourPts = hg.contours.get(hg.cMaxId).toArray();
			
			hg.mFingerTips.clear();
			for (int i = 0; i < cId.length; i++)
			{
				lp.add(contourPts[cId[i]]);
				hg.mFingerTips.add(contourPts[cId[i]]);
				//Core.circle(rgbaMat, contourPts[cId[i]], 2, new Scalar(241, 247, 45), -3);
			}
			
////////////
//
// De aqui para abajo esta su criba
//
////////////			
			
/*			
			//hg.hullP.get(hg.cMaxId) returns the locations of the points in the convex hull of the hand
			hg.hullP.get(hg.cMaxId).fromList(lp);
			lp.clear();
			
			
			
			hg.fingerTips.clear();
			hg.defectPoints.clear();
			hg.defectPointsOrdered.clear();
			
			hg.fingerTipsOrdered.clear();
			hg.defectIdAfter.clear();
			
			
			if ((contourPts.length >= 5) 
					&& hg.detectIsHand(mRgba) && (cId.length >=5)){
				Imgproc.convexityDefects(hg.contours.get(hg.cMaxId), hg.hullI, hg.defects);
				List<Integer> dList = hg.defects.toList();
			
							
//				Point prevPoint = null;
				
				for (int i = 0; i < dList.size(); i++)
				{
					int id = i % 4;
					Point curPoint;
					
					if (id == 2) { //Defect point
						double depth = (double)dList.get(i+1)/256.0;
						curPoint = contourPts[dList.get(i)];
						
						Point curPoint0 = contourPts[dList.get(i-2)];
						Point curPoint1 = contourPts[dList.get(i-1)];
						Point vec0 = new Point(curPoint0.x - curPoint.x, curPoint0.y - curPoint.y);
						Point vec1 = new Point(curPoint1.x - curPoint.x, curPoint1.y - curPoint.y);
						double dot = vec0.x*vec1.x + vec0.y*vec1.y;
						double lenth0 = Math.sqrt(vec0.x*vec0.x + vec0.y*vec0.y);
						double lenth1 = Math.sqrt(vec1.x*vec1.x + vec1.y*vec1.y);
						double cosTheta = dot/(lenth0*lenth1);
						
						if ((depth > hg.inCircleRadius*0.7)&&(cosTheta>=-0.7)
								&& (!isClosedToBoundary(curPoint0, mRgba))
								&&(!isClosedToBoundary(curPoint1, mRgba))
								){
							
												
							hg.defectIdAfter.add((i));
							
							
							Point finVec0 = new Point(curPoint0.x-hg.inCircle.x,
									curPoint0.y-hg.inCircle.y);
							double finAngle0 = Math.atan2(finVec0.y, finVec0.x);
							Point finVec1 = new Point(curPoint1.x-hg.inCircle.x,
									curPoint1.y - hg.inCircle.y);
							double finAngle1 = Math.atan2(finVec1.y, finVec1.x);
							
							
							
							if (hg.fingerTipsOrdered.size() == 0) {
								hg.fingerTipsOrdered.put(finAngle0, curPoint0);
								hg.fingerTipsOrdered.put(finAngle1, curPoint1);
							} else {
							  		hg.fingerTipsOrdered.put(finAngle0, curPoint0);
							  
							    	
							    	hg.fingerTipsOrdered.put(finAngle1, curPoint1);
							}
						}
					}
				}
			}
*/		}
		
		if (hg.detectIsHand(mRgba)) {

			//hg.boundingRect represents four coordinates of the bounding box. 
			Core.rectangle(mRgba, hg.boundingRect.tl(), hg.boundingRect.br(), new Scalar(0,255,0,0), 2);
			Imgproc.drawContours(mRgba, hg.hullP, hg.cMaxId, new Scalar(255,255,255,0));
		}
		
	}    

	Rect makeBoundingBox(Mat img)
	{
		hg.contours.clear();
		Imgproc.findContours(img, hg.contours, hg.hie, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
		hg.findBiggestContour();
		
		if (hg.cMaxId > -1)
		{
			hg.boundingRect = Imgproc.boundingRect(hg.contours.get(hg.cMaxId));
		}
		
		if (hg.detectIsHand(mRgba))
		{
			return hg.boundingRect;
		} else
			return null;
	}    

	boolean isClosedToBoundary(Point pt, Mat img)
	{
		int margin = 5;
		if ((pt.x > margin) && (pt.y > margin) && 
				(pt.x < img.cols()-margin) &&
				(pt.y < img.rows()-margin)) {
			return false;
		}
		
		return true;
	}	
    
    public native void FindFeatures(long matAddrGr, long matAddrRgba);

 // ** System events methods.


	// Detect trackpad events from GoogleGlass
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Code below is used for checking focus change.

            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            	PontsManager pm = PontsManager.getSingleton();
		        pm.initialize(mRgba);
		                    	            Resources res = this.getResources();
		                    	            long time = 5000;
		                    	            
		                    	            if (mFeaturesState == FEATURES_STATE_CALIBRATE_BG)
		                			        {
		                				        pm.preSampleBackground  (mRgba);
		                				        mFeaturesState = FEATURES_STATE_CALIBRATE_HAND;
		                			        		                			        		                    	           String text = res.getString(R.string.focus_hand);
        		        	Utils.launchActivity(DeviceExplorerActivity.this, NotificationViewerActivity.class.getName(), text, time);
		                			        }// end if blue points.
		                    	            else if (mFeaturesState == FEATURES_STATE_CALIBRATE_HAND)
		                			        {
		                				        pm.preSampleHand        (mRgba);
		                				        pm.boundariesCorrection();
		                				        mFeaturesState = FEATURES_STATE_TEST;
		                				        String text = res.getString(R.string.explore_instructions);
		                    		        	Utils.launchActivity(DeviceExplorerActivity.this, NotificationViewerActivity.class.getName(), text, time);    		                				       		                			       
		                			        }// end else.
		                				
            }// end if volume_down
		         else if (keyCode == KeyEvent.KEYCODE_CAMERA && event.getAction() == KeyEvent.ACTION_DOWN) {
            //HANDLE CAMERA BUTTON EVENT
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            //HANDLE CLICK EVENT
        
        	PontsManager pm = PontsManager.getSingleton();
	        pm.initialize(mRgba);
	                    	            Resources res = this.getResources();
	                    	            long time = 5000;
	                    	            
	                    	            if (mFeaturesState == FEATURES_STATE_CALIBRATE_BG)
	                			        {
	                				        pm.preSampleBackground  (mRgba);
	                				        mFeaturesState = FEATURES_STATE_CALIBRATE_HAND;
	                			        		                			        		                    	           String text = res.getString(R.string.focus_hand);
    		        	Utils.launchActivity(DeviceExplorerActivity.this, NotificationViewerActivity.class.getName(), text, time);
	                			        }// end if blue points.
	                    	            else if (mFeaturesState == FEATURES_STATE_CALIBRATE_HAND)
	                			        {
	                				        pm.preSampleHand        (mRgba);
	                				        pm.boundariesCorrection();
	                				        mFeaturesState = FEATURES_STATE_TEST;
	                				        String text = res.getString(R.string.explore_instructions);
	                    		        	Utils.launchActivity(DeviceExplorerActivity.this, NotificationViewerActivity.class.getName(), text, time);    		                				       		                			       
	                			        }// end else.
	                				
        
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_TAB) { //glass and keyboards
            if (event.isShiftPressed()) { //left/reverse
                    //HANDLE SWIPE BACKWARDS

            } else { //right
                //HANDLE SWIPE FORWARD
            	
            }
        }
  


        return super.onKeyDown(keyCode, event);
    }// end onKeyDown.
    
    
    }// end class.
