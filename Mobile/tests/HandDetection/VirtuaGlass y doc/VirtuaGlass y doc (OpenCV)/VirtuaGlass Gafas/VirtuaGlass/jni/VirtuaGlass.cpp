#include <jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

using namespace std;
using namespace cv;

extern "C" {
JNIEXPORT jdouble JNICALL Java_com_technosite_virtuaglass_opencv_HandGesture_findInscribedCircleJNI(JNIEnv* env, jobject obj, jlong imgAddr, jdouble rectTLX, jdouble rectTLY, jdouble rectBRX, jdouble rectBRY, jdoubleArray incircleX, jdoubleArray incircleY, jlong contourAddr);

// Probando
void paintCircle (Mat dst, Point p, Scalar clr)
{
/*    for (int i = 0;i < 10;i++)
    {
    	Point orig = Point(p);
    	orig.x -= (5 + i);orig.y -= (5 + i);
    	Point dest = Point(p);
    	dest.x += (5 + i);dest.y += (5 + i);
		rectangle(dst,orig,dest,clr);
    }
*/}

// Probando
JNIEXPORT void JNICALL Java_com_technosite_virtuaglass_MainActivity_FindFeatures(JNIEnv*, jobject, jlong addrGray, jlong addrRgba)
{
/*    Mat& mGr  = *(Mat*)addrGray;
    Mat& mRgb = *(Mat*)addrRgba;
    vector<KeyPoint> v;

    Mat mHsv;


    cvtColor(mRgb,mHsv,CV_BGR2HSV);

///////////
//    inRange(mHsv, Scalar(0., 100., 30.), Scalar(5., 255., 255.), mHsv);
///////////

/*
    FastFeatureDetector detector(50);
    detector.detect(mGr, v);
    for( unsigned int i = 0; i < v.size(); i++ )
    {
        const KeyPoint& kp = v[i];
        circle(mRgb, Point(kp.pt.x, kp.pt.y), 10, Scalar(255,0,0,255));
    }
*/
/*
    cvtColor(mHsv,mRgb,CV_BGR2HSV);

    Point p = Point(mRgb.cols >> 1,mRgb.rows >> 1);
    int   mwidth     = mRgb.cols >> 3;
    int   mHeight    = mRgb.rows >> 3;

    Scalar color = Scalar (255,0,0);

    paintCircle(mRgb,p,color);
    p.x -= mwidth;
    p.y += mHeight;
	paintCircle(mRgb,p,color);
    p.x += mwidth;
    paintCircle(mRgb,p,color);
    p.x += mwidth;
    paintCircle(mRgb,p,color);
    p.x -= mwidth;
    p.y += mHeight;
    paintCircle(mRgb,p,color);
*/}

JNIEXPORT jdouble JNICALL Java_com_technosite_virtuaglass_opencv_HandGesture_findInscribedCircleJNI(JNIEnv* env, jobject obj, jlong imgAddr,
		jdouble rectTLX, jdouble rectTLY, jdouble rectBRX, jdouble rectBRY,
		jdoubleArray incircleX, jdoubleArray incircleY, jlong contourAddr)
{
	Mat& img_cpp  = *(Mat*)imgAddr;

	//vector<Point2f>& contour = *(vector<Point2f> *)contourAddr;
	Mat& contourMat = *(Mat*)contourAddr;
	vector<Point2f> contourVec;
	contourMat.copyTo(contourVec);

	double r = 0;
	double targetX = 0;
	double targetY = 0;

	for (int y = (int)rectTLY; y < (int)rectBRY; y++)
	{
		for (int x = (int)rectTLX; x < (int)rectBRX; x++)
		{
			double curDist = pointPolygonTest(contourVec, Point2f(x, y), true);

			if (curDist > r) {
				r = curDist;
				targetX = x;
				targetY = y;
			}
		}
	}

	jdouble outArrayX[] = {0};
	jdouble outArrayY[] = {0};

	outArrayX[0] = targetX;
	outArrayY[0] = targetY;

	env->SetDoubleArrayRegion(incircleX, 0 , 1, (const jdouble*)outArrayX);
	env->SetDoubleArrayRegion(incircleY, 0 , 1, (const jdouble*)outArrayY);
	//Core.circle(img, inCircle, (int)inCircleRadius, new Scalar(240,240,45,0), 2);


	return r;
}

}
