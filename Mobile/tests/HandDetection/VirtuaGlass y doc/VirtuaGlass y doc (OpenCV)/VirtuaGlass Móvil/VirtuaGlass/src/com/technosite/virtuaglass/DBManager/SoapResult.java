package com.technosite.virtuaglass.DBManager;

import org.ksoap2.serialization.SoapObject;

import android.util.Log;

public class SoapResult {
	public SoapObject data = null;
	
	
	public SoapResult(SoapObject pData) {
		data = pData;
	}
	
	public int size() {
return data.getPropertyCount();				
	}
	
	public Object get(int i) {
		if (i< data.getPropertyCount()) {
			Object o = (Object) data.getProperty(i);
			if (o!=null) return o;
			else return null;
			
		} else {
			Log.e("SoapResult management", "Index out of range too get a String from Soap data");
			return null;
		}
	}
	
	public Object get(String i) {
		return data.getProperty(i);
	}
	
	public String getString(int i) {
					Object o = get(i);
			if (o!=null) return (String) o.toString();
			else return "null";
	}
	
	public String getString(String i) {
		Object o = get(i);
		if (o!=null) return (String) o.toString();
		else return "null";		
	}
	
	public int getInteger(int i) {
		String r = getString(i);
		if (r!=null && !r.equalsIgnoreCase("null")) return Integer.valueOf(r);
		else return -1;
	}
	
	public int getInteger(String i) {
		String r = getString(i);
		if (r!=null && !r.equalsIgnoreCase("null")) return Integer.valueOf(r);
		else return -1;		
	}
	
	public String toString() {
		String result = this.size() + " cols:\n" + data.toString();

		return result;
				}
	
	public boolean isErrorQweryWithField() {
		boolean result = true;
		if (data!=null) {
			// String r = getString("error");
			// if (r.equalsIgnoreCase("null")) result = false;
			result = false;
		} 
		return result;
	}
	
}