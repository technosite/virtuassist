package com.technosite.virtuaglass.model;

import java.io.Serializable;

public class PDTStep implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;

	public int id = 0;
	public int position = 0;
	public int type = 0;
	public String instruction = null;
	public PDTPath path = null;
	public int idControl = 0;
	public PDTControl control = null;
	public Object image = null;


	public PDTStep(int pId,int pos, int pType, String text,int pIdControl, Object pImage, PDTPath pPath) {
		// TODO Auto-generated constructor stub
		this.position = pos;
		this.instruction = text;
		this.type = pType;
		this.idControl = pIdControl;
		this.control = null;
		this.image = pImage;
		this.path = pPath;
	}

	public void update() {

	}

}
