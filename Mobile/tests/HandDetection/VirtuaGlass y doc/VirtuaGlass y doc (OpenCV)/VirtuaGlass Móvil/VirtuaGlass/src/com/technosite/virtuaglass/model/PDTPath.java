package com.technosite.virtuaglass.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PDTPath implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;
	public int id = 0;
	public String name = null;
	public String description = null;
	public PDTDevice device = null;


	public PDTPath(int pId,String pName, String pDescription, PDTDevice pDevice) {
		// TODO Auto-generated constructor stub
		steps = new ArrayList<PDTStep>();
		this.id = pId;
		this.name = pName;
		this.description = pDescription;
		this.device = pDevice;
	}


	private ArrayList<PDTStep> steps = null;

	public int getNumberOfSteps() {
		return steps.size();
	}

	public void addSteps(ArrayList<PDTStep> list) {
for (int i=0;i<list.size();i++) {
	this.addStep(list.get(i));
}
	}

	public void addStep(PDTStep pStep) {
		pStep.path = this;
		steps.add(pStep);
	}

	public void removeStep(PDTStep pStep) {
		steps.remove(pStep);
	}

	public PDTStep getStep(int index) {
		return steps.get(index);
	}

	public PDTStep findStepByPosition(int pos) {
		PDTStep result = null;
for (int i=0;i<steps.size();i++) {
PDTStep tmp = steps.get(i);
if (tmp.position == pos) {
	result = tmp;
	break;
}
}
return result;
	}


	// ** Methods to manage the path

	private boolean shorted = false;
	private void shortSteps () {
		if (shorted ) return;
		ArrayList<PDTStep> tmp = new ArrayList<PDTStep>();
		// Calculate the highest position
		int highest = 0;
		for (int i=0;i<steps.size();i++) {
			PDTStep istep = steps.get(i);
			if (istep.position > highest) highest = istep.position;
		}
		//moving elements
		for (int c=0;c<=highest;c++) {
PDTStep tmpStep = findStepByPosition(c);
if (tmpStep !=null) tmp.add(tmpStep );
		}
steps = tmp;
		shorted  = true;
	}

	public void update() {
		shortSteps ();
		// load reference to a control from its idControl in each step
		for (int i=0;i<steps.size();i++) {
			PDTStep tmp = steps.get(i);
			tmp.control = device.findControlById(tmp.idControl);
			tmp.update();
		}
	}



}