package com.technosite.virtuaglass.opencv;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

public class PontsManager {

	public boolean initialized = false;
	
	// N�mero de muestras para la mano y para el fondo
	public final  int          HAND_POINTS       = 7;
	public final  int          BACKGROUND_POINTS = 8;
	
	// Arrays con los puntos de muestra
	public        Point[]      handPoints        = new Point[HAND_POINTS];
	public        Point[]      backgroundPoints  = new Point[BACKGROUND_POINTS];

	// Color de los puntos en el frame actual
	public        double[][]   avgColor          = new double[HAND_POINTS][3];;
	public        double[][]   avgBackColor      = new double[BACKGROUND_POINTS][3];;
	
	// desviaci�n permitida para cada regi�n
	public        double[][]   cLower            = new double[HAND_POINTS][3];
	public        double[][]   cUpper            = new double[HAND_POINTS][3];
	public        double[][]   cBackLower        = new double[BACKGROUND_POINTS][3];
	public        double[][]   cBackUpper        = new double[BACKGROUND_POINTS][3];

	public static PontsManager instance = null;
	
	public static PontsManager getSingleton ()
	{
		if (instance == null) {
			synchronized (PontsManager.class) {
				PontsManager inst = instance;
				if (inst == null) {
					synchronized (PontsManager.class) {
						inst = new PontsManager();
					}
					instance = inst;
				}
			}
		}
		return instance;
	}
	
	private PontsManager ()
	{
		initCLowerUpper(50, 50, 10, 10, 10, 10);
	    initCBackLowerUpper(50, 50, 3, 3, 3, 3);
	};
	
	public void initialize (Mat img)
	{
		if (!initialized)
		{
			initialBackgroundSamplePointsLocation(img);
			initialHandSamplePointsLocation(img);
			initialized = true;
		}
	}

	//Inicializaci�n de los umbrales de la primera muestra
	void initCLowerUpper(double cl1, double cu1, double cl2, double cu2, double cl3,
		double cu3)
	{
		cLower[0][0] = cl1;
		cUpper[0][0] = cu1;
		cLower[0][1] = cl2;
		cUpper[0][1] = cu2;
		cLower[0][2] = cl3;
		cUpper[0][2] = cu3;
	}
	
	void initCBackLowerUpper(double cl1, double cu1, double cl2, double cu2, double cl3,
		double cu3)
	{
		cBackLower[0][0] = cl1;
		cBackUpper[0][0] = cu1;
		cBackLower[0][1] = cl2;
		cBackUpper[0][1] = cu2;
		cBackLower[0][2] = cl3;
		cBackUpper[0][2] = cu3;
	}
	
	//Calcular los valores de color para cada muestra
	public void preSampleHand(Mat img)
	{
		for (int i = 0; i < HAND_POINTS; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				avgColor[i][j] = (img.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[j];
			}
		}
	}	

	//Calcular los valores de color para cada muestra
	public void preSampleBackground(Mat img)
	{
		for (int i = 0; i < BACKGROUND_POINTS; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				avgBackColor[i][j] = (img.get((int)(backgroundPoints[i].y), (int)(backgroundPoints[i].x)))[j];
			}
		}
	}	

	//Reubica los puntos de muestra de la mano
	public void initialHandSamplePointsLocation(Mat img)
	{
    	int midX = img.cols() >> 1;
    	int midY = img.rows() >> 1;
    	
    	int wOct = midX >> 3;
    	int hOct = midY >> 2;

    	for (int i = 0; i < HAND_POINTS;i++)
    	{
    		handPoints[i] = new Point();
    	}
    	
    	handPoints[0].x = midX       ;
    	handPoints[1].x = midX       ;
    	handPoints[2].x = midX + wOct;
    	handPoints[3].x = midX - wOct;
    	handPoints[4].x = midX + wOct;
    	handPoints[5].x = midX - wOct;
    	handPoints[6].x = midX       ;
    	handPoints[0].y = midY     - hOct         ;
    	handPoints[1].y = midY + hOct       ;
    	handPoints[2].y = midY /*+ hOct*/ - (hOct / 2);
    	handPoints[3].y = midY /*+ hOct*/ - (hOct / 2);
    	handPoints[4].y = midY + hOct + (hOct / 2);
    	handPoints[5].y = midY + hOct + (hOct / 2);
    	handPoints[6].y = midY + hOct + hOct;
	}
	
	//Reubica los puntos de muestra del fondo
	public void initialBackgroundSamplePointsLocation(Mat img)
	{
    	int offsetX = img.cols() >> 4;
    	int offsetY = img.rows() >> 3;
    	int hWidth  = img.cols() >> 1;
    	int qWidth  = img.cols() >> 2;
    	int hHeight = img.rows() >> 1;
    	
    	for (int i = 0; i < BACKGROUND_POINTS;i++)
    	{
    		backgroundPoints[i] = new Point();
    	}
    	
    	backgroundPoints[0].x =              offsetX;
    	backgroundPoints[1].x = img.cols() - offsetX;
    	backgroundPoints[2].x =              offsetX;
    	backgroundPoints[3].x = img.cols() - offsetX;
    	backgroundPoints[4].x = hWidth              ;
    	backgroundPoints[5].x = hWidth              ;
    	backgroundPoints[6].x =              qWidth ;
    	backgroundPoints[7].x = hWidth     + qWidth ;
    	backgroundPoints[0].y =              offsetY;
    	backgroundPoints[1].y =              offsetY;
    	backgroundPoints[2].y = img.rows() - offsetY;
    	backgroundPoints[3].y = img.rows() - offsetY;
    	backgroundPoints[4].y =              offsetY;
    	backgroundPoints[5].y = img.rows() - offsetY;
    	backgroundPoints[6].y = hHeight             ;
    	backgroundPoints[7].y = hHeight             ;
	}
	
	//Pinta en el mapa que se pasa por parametro un circulo azul en la posici�n de las muestras de la mano
	public void drawHandSamplePoints(Mat img)
	{
		for (int i = 0; i < HAND_POINTS; i++)
		{
			Core.circle(img, handPoints[i], 10, new Scalar (0,0,255), -1);
		}
	}
	
	//Pinta en el mapa que se pasa por parametro un circulo rojo en la posici�n de las muestras del fondo
	public void drawBackgroundSamplePoints(Mat img)
	{
		for (int i = 0; i < BACKGROUND_POINTS; i++)
		{
			if (backgroundPoints[i] != null)
				Core.circle(img, backgroundPoints[i], 10, new Scalar (255,0,0), -1);
		}
	}
	
	// Recalcula los umbrales
	public void boundariesCorrection()
	{
		for (int j = 0; j < 3; j++)
		{
			for (int i = 1; i < HAND_POINTS; i++)
			{
				cLower[i][j] = cLower[0][j];
				cUpper[i][j] = cUpper[0][j];
			}
			for (int i = 1; i < BACKGROUND_POINTS; i++)
			{
				cBackLower[i][j] = cBackLower[0][j];
				cBackUpper[i][j] = cBackUpper[0][j];
			}
		}
		
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < HAND_POINTS; i++)
			{
				if (avgColor[i][j] - cLower[i][j] < 0)
					cLower[i][j] = avgColor[i][j];
				
				if (avgColor[i][j] + cUpper[i][j] > 255)
					cUpper[i][j] = 255 - avgColor[i][j];
			}
			for (int i = 0; i < BACKGROUND_POINTS; i++)
			{
				if (avgBackColor[i][j] - cBackLower[i][j] < 0)
					cBackLower[i][j] = avgBackColor[i][j];
				
				if (avgBackColor[i][j] + cBackUpper[i][j] > 255)
					cBackUpper[i][j] = 255 - avgBackColor[i][j];
			}
		}
	}
	
}
