package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.technosite.virtuaglass.DBManager.DataManager;
import com.technosite.virtuaglass.model.PDTManager;
import com.technosite.virtuaglass.model.PDTPath;

public class PathSelectorActivity extends Activity
{

	private TableLayout pathTable;
    private List<TextView> textViews;
private int index;
private List<String> paths;



	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	setContentView(R.layout.path_selector);

	pathTable = (TableLayout) findViewById(R.id.pathTable);	

	// load device for checking.
	DataManager dataManager = new DataManager();
	boolean result = dataManager.loadDeviceWithQRCode("Pdt-8");
	
	if(result)
	{
	loadPaths();	// it is needed to handle focus dynamically.
	textViews = new ArrayList<TextView>();

	for(int i = 0; i < paths.size(); i++)
	{
        TableRow row = new TableRow(this);
    row.setLayoutParams(new LayoutParams(
                   LayoutParams.MATCH_PARENT,
                   LayoutParams.WRAP_CONTENT));

    View v = View.inflate(this, R.layout.path_row, null);

    final ImageView arrowPathImageView = (ImageView) v.findViewById(R.id.arrowPathImageView);
        final TextView pathSelectorTextView = (TextView) v.findViewById(R.id.pathSelectorTextView);
pathSelectorTextView.setText(paths.get(i));
pathSelectorTextView.setId((i+1));

pathSelectorTextView.setOnFocusChangeListener(new OnFocusChangeListener()
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus) {

			if(hasFocus)
		{
			arrowPathImageView.setVisibility(View.VISIBLE);
			pathSelectorTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
	pathSelectorTextView.setTypeface(Typeface.DEFAULT_BOLD);
				}// end if hasFocus.
		else
		{
			arrowPathImageView.setVisibility(View.GONE);
			pathSelectorTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
	pathSelectorTextView.setTypeface(Typeface.DEFAULT);
						}// end else hasFocus.
		}// end onFocusChange.
		});

pathSelectorTextView.setOnClickListener(new OnClickListener()
{
@Override
	public void onClick(View v) {

	showPath(pathSelectorTextView.getId());
			
}// end onClick.
	});

textViews.add(pathSelectorTextView);

row.addView(v);
pathTable.addView(row);

	}// end for i paths.
	
	TextView pathSelectorInstructionsTextView = (TextView) findViewById(R.id.pathSelectorInstructionsTextView);
	pathSelectorInstructionsTextView.requestFocus();
	}// end if.
	else
	{
		TextView text = new TextView(this);
		text.setText(R.string.no_device);
		text.setFocusable(true);
		
		pathTable.addView(text);
	 	}// end else.
	

	}// end onCreate.

// ** System events methods.

	/* (non-Javadoc)
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Method used for checking focus change.

		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
	    {
index--;
			if(index >= 0)
{
	textViews.get(index).requestFocus();
}
else
{
	index = 0;
}

	    }// end if volume_up.
		else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
	    {
index++;
			if(index < textViews.size())
{
	textViews.get(index).requestFocus();
}
else
{
	index = (textViews.size()-1);
}

	    }// end if volume_up.


		return super.onKeyDown(keyCode, event);
	}// end onKeyDown.

    
	private void loadPaths() {
		PDTManager manager = PDTManager.getSharedInstance();
paths = new ArrayList<String>();
boolean isTherePaths = false;
if (manager.device.getNumberOfPaths() > 0 ) {
	for (int i=0;i<manager.device.getNumberOfPaths();i++) {
		PDTPath tmpP = manager.device.getPath(i);
		if (tmpP.getNumberOfSteps()>0) {
			isTherePaths = true;
			break;
		}// end if numberOfSteps.
	}// end for.
 	}// end if numberOfPaths.
	
if (isTherePaths) {
for (int i=0;i<manager.device.getNumberOfPaths();i++) {
PDTPath item = manager.device.getPath(i);
if (item.name!=null && !item.name.equalsIgnoreCase("null") && item.getNumberOfSteps()>0) {
paths.add(item.name);
	
}// end if paths.
}// end for.
} else {
	
	TextView text = new TextView(this);
	text.setText(R.string.no_paths);
	text.setFocusable(true);
	
	pathTable.addView(text);
}// end else.
	}// end loadPaths.
	
	public void showPath(int pathNumber) {
		PDTManager manager = PDTManager.getSharedInstance();
manager.path = manager.device.getPath(pathNumber);
manager.step = manager.path.getStep(0);
    			Intent i = new Intent(this, StepViewerActivity.class);
    			// send item id to the next activity
    			startActivity(i);		
	}// end showPath.

}// end class.
