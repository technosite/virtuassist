package com.technosite.virtuaglass;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ControlDetailsActivity extends Activity {

	private TextView controlDetailsTextView;
	private ImageView controlDetailsImageView;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	setContentView(R.layout.control_details);

	controlDetailsTextView = (TextView) findViewById(R.id.controlDetailsTextView);
	controlDetailsImageView = (ImageView) findViewById(R.id.controlDetailsImageView);
	controlDetailsTextView.setText("Control");
	controlDetailsImageView.setImageResource(R.drawable.arrow);
	    
	
	}// end onCreate.

}// end class.
