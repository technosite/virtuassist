package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class NotificationViewerActivity extends Activity {

	private TextView notificationViewerHeaderTextView;
	private ImageView notificationViewerImageView;

	private String text;
	private long time;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
setContentView(R.layout.notification_viewer);

// get data from MainActivity
Bundle extras = getIntent().getExtras();

if(extras != null)
{
	text = extras.getString("text");
	time = extras.getLong("time");
}// end if extras.

notificationViewerHeaderTextView = (TextView) findViewById(R.id.notificationViewerHeaderTextView);
	notificationViewerImageView = (ImageView) findViewById(R.id.notificationViewerImageView);

	notificationViewerHeaderTextView.setText(text);
	Resources res = this.getResources();

	// Depending on selected option, we will display operations or explore image
	if(text.equals(res.getString(R.string.operations_instructions)))
	{
		notificationViewerImageView.setImageDrawable(res.getDrawable(R.drawable.instructionoperations));
}// end if operations.
	else 	if(text.equals(res.getString(R.string.explore_instructions)))
	{
		notificationViewerImageView.setImageDrawable(res.getDrawable(R.drawable.instructionexploration));
}// end if explore.
	else if(text.equals(R.string.focus_printer))
	{
		
	}// end if.
	// This activity will be hidden after set time (5 seconds).
	Timer timer = new Timer();
	TimerTask task = new TimerTask()
	{
		@Override
		public void run()
		{
					runOnUiThread(new Runnable()
{
@Override
	public void run()
	{

NotificationViewerActivity.this.finish();

	}// end run.
	});
				}// end run.
		};
	timer.schedule(task, time);

	}// end onCreate.

}// end class.
