package com.technosite.TTS;

import java.util.Locale;

import android.content.Context;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;

public class TTS 
implements TextToSpeech.OnInitListener {

	private Context context;
	private TextToSpeech TTSEngine = null;
	
	// ** Constructor and destructor
	
	public TTS(Context ct) {
		super();
		context = ct;
		if (TTSEngine == null) TTSEngine = new TextToSpeech(context, this); 	
	}
	
	public void finalize() {
		if (TTSEngine != null) {
						TTSEngine.stop();
			TTSEngine.shutdown();
					}
	}
	
	// ** Class interface methods
	
@Override	
	public void onInit(int status) {
		// TTS initialization
	if (status == TextToSpeech.SUCCESS) {
		if (TTSEngine != null) TTSEngine.speak("", TextToSpeech.QUEUE_ADD, null);
	} else {
		Log.e("TTS engine error", "Error initializing the TTS engine.");
	}
	}

	// ** Voice management

private boolean muted = false;

public void setMuteStatus(boolean value) {
	muted = value;
}

public boolean getMuteStatus() {
return muted;	
}
	
	public void speak(String text) {
		if (!muted) {
			if (TTSEngine != null) TTSEngine.speak(text, TextToSpeech.QUEUE_ADD, null);
			else Log.e("TTS speak", "TTS engine is not initialized");			
		}
;	}
	
	public void speakNow(String text) {
		if (!muted) {
			if (TTSEngine != null) TTSEngine.speak(text, TextToSpeech.QUEUE_FLUSH, null);
			else Log.e("TTS speakNow", "TTS engine is not initialized");			
		}
	}
	
	public void speakWithDelay(String text, int time) {
		if (!muted) {
			textForDelay = text;
			mHandler.removeCallbacks(timerEvent);
			mHandler.postDelayed(timerEvent, time);
		}
	}
	
	public void stop() {
		this.speakNow("");
if (TTSEngine != null) TTSEngine.stop();		
	}
	
	// Normal value = 1.0
	public void setPitch(float value) {
		if (TTSEngine != null) TTSEngine.setPitch(value);
		else Log.e("TTS setPitch", "TTS engine is not initialized");
	}
	
	// Normal value = 1.0
	public void setRate(float value) {
		if (TTSEngine != null) TTSEngine.setSpeechRate(value);
		else Log.e("TTS setRate", "TTS engine is not initialized");
	}
	
	public void setLanguage(Locale idLang) {
		if (TTSEngine != null) 
			if (TTSEngine.setLanguage(idLang) == TextToSpeech.LANG_MISSING_DATA) {
Log.e("TTS engine error", "This language is not supported for TTS engine.");			
		}
	}

	
	// ** timer management
	
	private String textForDelay;
	private Handler mHandler = new Handler();

	
	private Runnable timerEvent = new Runnable() {
		public void run() {
			speak(textForDelay);
			mHandler.removeCallbacks(timerEvent);
		}
	};	
	
	public boolean isSpeaking() {
		return TTSEngine.isSpeaking();
	}
	
	public boolean isServiceAvailable() {
		boolean result = false;
		if (TTSEngine != null ) {
			String engineName = TTSEngine.getDefaultEngine();
if (engineName != null) result = true;			
		}
		return result;
	}
	
	
	
	
	// ** Midle singleton solution
	
	
	private static TTS singleton_instance = null;
	
	public static TTS getSharedInstance(Context context) {
if (singleton_instance == null) {
	singleton_instance  = new TTS(context);
}
return singleton_instance; 
	}
	
	public static void destroySharedInstance() {
		singleton_instance  .finalize();
		singleton_instance   = null;
	}
	
	}
