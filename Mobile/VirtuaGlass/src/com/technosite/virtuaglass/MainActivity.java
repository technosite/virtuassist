/*
 * � Copyright 2014 Fundosa Technosite S.A.
 * 
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may 
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * This software includes methods of the openCV library.
 */
package com.technosite.virtuaglass;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.technosite.BluetoothCommunicator.BluetoothCommunicator;
import com.technosite.BluetoothCommunicator.PDTBluetoothService;
import com.technosite.BluetoothCommunicator.PDTBluetoothService.PDTBluetoothInterface;
import com.technosite.Interface.GlassActivity;
import com.technosite.Interface.NavigationManager;
import com.technosite.LogManager.LogManager;
import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;
import com.technosite.virtuaglass.model.Globals;
import com.technosite.virtuaglass.model.PDTManager;
import com.technosite.virtuaglass.model.Persistence;
import com.technosite.virtuaglass.opencv.ImageManager;

public class MainActivity extends GlassActivity implements PDTBluetoothInterface {
private boolean loadDevice = true; // For debugging
	private TTS tts = null;
	private Globals globals = null;
	private PDTManager manager = null;
	private PDTBluetoothService bluetoothService   = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tts = TTS.getSharedInstance(this);
		tts.speakWithDelay("", 300);
		this.stopLockScreen(true);
		LogManager.getSharedInstance();
		LogManager.resetLog();
		LogManager.addSystemTask("Virtuassist application runs");
		
		globals = Globals.getSharedInstance(this);
		Globals.activity = this;
		globals.isDebugMode = false;
		ImageManager.getSharedInstance();
		
		bluetoothService  = PDTBluetoothService.getSharedInstance(this);
		bluetoothService .delegate = this;
				loadSounds();
				manager = PDTManager.getSharedInstance();
				// if (BluetoothCommunicator.isAvailableService() ) bluetoothService.connect(globals.BluetoothDeviceID);
				if (BluetoothCommunicator.isAvailableService() ) bluetoothService.connect(Persistence.kPairedDeviceAddressByDefault);

				
				checkDeviceAvailable();
				
		setContentView(R.layout.activity_main);
				checkIsLogIn();
		initInterface();
		sndDone.play();
				updateHeader(); 		
	}
	@Override
	public void onPause() {
		super.onPause();
		this.stopLockScreen(false);
	}
	
    @Override
    public void onResume()
    {
        super.onResume();
        this.stopLockScreen(true);
        checkDeviceAvailable();
        updateHeader();
               
    }

	// ** Visual interface management
	
	private void updateHeader() {
		TextView title = (TextView) findViewById(R.id.mainHeaderTextView);
		if (manager .device!=null) title.setText(manager.device.name);
		
	}
	/*
	private void speakInitialization() {
		Button v = (Button) nav.getCurrent();
		if (v!=null) tts.speakWithDelay("Virtuassist." + v.getText().toString(), 300);
		else tts.speakWithDelay("Virtuassist", 300);
	}
*/
	
	// ** Sound management

	private SoundFX sndDone = null;
	private SoundFX sndOk = null;
	private SoundFX sndNotify = null;
	private SoundFX sndMovement = null;
	

	private void loadSounds() {
		sndDone = new SoundFX(this, R.raw.done);
		sndOk = new SoundFX(this, R.raw.ok);
		sndNotify = new SoundFX(this, R.raw.notify);
		sndMovement = new SoundFX(this, R.raw.tic);
			}

	// ** GoogleGlass d-pad management

	@Override 
	protected void onGlassGestureEvent(int code) { 
		switch(code) {
		case GlassActivity.GLASSGESTURE_TAB :
			doClick();
			break;
		case GlassActivity.GLASSGESTURE_RIGHT :
			doLeft();
			break;
		case GlassActivity.GLASSGESTURE_LEFT :

			doRight();
			break;
		case GlassActivity.GLASSGESTURE_DOWN :
			// sndNotify.play();
			onBackPressed();
			break;
		case GlassActivity.GLASSGESTURE_UP :
			sndNotify.play();
			break;
		case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :

			break;
		}
	}

	// ** Interface management
	private NavigationManager  nav = null;
	private ImageView opt_exploreChecker = null;
	private ImageView opt_operationsChecker = null;
	private Button opt_exploreButton = null;
	private Button opt_operationsButton = null;
	private Button focusedControl = null;

	private void doClick() {
		onClickedOption(focusedControl);     	
	}

	private void doLeft() {
		Button v = (Button) nav.moveFocusToNext();
		v.requestFocus();
		
		sndMovement.play();	
	}

	private void doRight() {
		Button v = (Button) nav.moveFocusToPrevious();
		v.requestFocus();

		sndMovement.play(); 
	}

	private void initInterface() {
		nav = new NavigationManager ();
		nav.alwaysReturnControl = true;
		nav.loopingNavegationMode  = true;
		opt_exploreChecker = (ImageView) findViewById(R.id.arrowExploreImageView); 
		opt_exploreButton = (Button) findViewById(R.id.exploreMainTextView);
		opt_operationsChecker = (ImageView) findViewById(R.id.arrowOperationsImageView);
		opt_operationsButton = (Button) findViewById(R.id.operationsMainTextView);
		opt_exploreButton.setTag(1);
		opt_operationsButton.setTag(2);
		opt_exploreButton.setOnFocusChangeListener(onFocusManager);
		opt_operationsButton.setOnFocusChangeListener(onFocusManager);
		opt_exploreButton.requestFocus();
		nav.add(opt_exploreButton);
		nav.add(opt_operationsButton);
		focusedControl = (Button) nav.moveFocusToFirst();
	}



	public void onClickedOption(View view) {
		tts.stop();
		if (!checkIsLogIn()) {
			return;
		}
		
		sndOk.play();
		Button v = (Button) view;
		switch ((int)Integer.valueOf(v.getTag().toString())) {
		case 1 : // Explore
			LogManager.addUserTask("Option 'Explore' selected");
		
				if (manager.device!= null) {
					Intent ii = new Intent(this, DeviceExplorerActivity.class );
					startActivity(ii);			
		} else {
			checkDeviceAvailable();
		}
		break;
		
		case 2 : // Operations
			LogManager.addUserTask("Option 'Explore' selected");
			if (manager.device!= null) {
								Intent i = new Intent(this, PathSelectorActivity.class); 
				startActivity(i);
				
							} else {
				// NotificationViewerActivity.show(this, "Sorry, this function is not available yet because there is no loaded device.", 5000, R.drawable.nodevice);
				checkDeviceAvailable();
								
			}
			break;
		}



	}

	private OnFocusChangeListener onFocusManager = new OnFocusChangeListener() {
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (hasFocus == true) {
				Button control = (Button) v;
				focusedControl  = control;
				String label = control.getText().toString();
				int opt = Integer.valueOf(control.getTag().toString());
				tts.speakNow(label);

				switch(opt) {
				case 1: // Explore
				showOptionExplore(true);
				showOptionOperations(false);
				break;
				case 2 : // operations
					showOptionExplore(false);
					showOptionOperations(true);
					break;
				}
			}
		}
	};

	private void showOptionExplore(boolean visible) {
		if (visible) {
			opt_exploreChecker.setVisibility(View.VISIBLE);
			opt_exploreButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 23);
			opt_exploreButton.setTypeface(Typeface.DEFAULT_BOLD);
			opt_exploreButton.setPadding(10, 5, 10, 5);

		} else {
			opt_exploreChecker.setVisibility(View.GONE);
			opt_exploreButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
			opt_exploreButton.setTypeface(Typeface.DEFAULT);
			opt_exploreButton.setPadding(10,5,10,5);
		}
	}

	private void showOptionOperations(boolean visible) {
		if (visible) {
			opt_operationsChecker.setVisibility(View.VISIBLE);
			opt_operationsButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 23);
			opt_operationsButton.setTypeface(Typeface.DEFAULT_BOLD);
			opt_operationsButton.setPadding(10, 5, 10, 5);
		} else {
			opt_operationsChecker.setVisibility(View.GONE);
			opt_operationsButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
			opt_operationsButton.setTypeface(Typeface.DEFAULT);
			opt_exploreButton.setPadding(10,5,10,5);
		}
	}

	// ** QRCode management

	private String code = null;
	private final int REQUEST_SCAN = 20140622;

	private void readQRCode() {
		//this.loadDataFromDevice("Pdt-1");
		Intent ii = new Intent(this, QRCapturerActivity.class );
		startActivityForResult(ii,REQUEST_SCAN);
				
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
	    switch (requestCode)
	    {
	        case REQUEST_SCAN:
	            	            	String scanResult= data.getStringExtra("SCAN_RESULT");	                	                
	                	                if (scanResult!=null) {
	                	                	String result= scanResult; // scanResult.getContents();
	                	                		                	                			                	                manageData(result);	
	                	                } else {
	                	                	LogManager.addSystemTask("Error in QR code reading process");
	                	                		                	                	NotificationViewerActivity.show(this, "Resultado nulo", 5000, R.drawable.nodevice);
	                	                }
	            
	            break;
	        }
	}
	

	public void manageData(String result) {
LogManager.addSystemTask("Processing QR value '" + result + "' to load the machine data.");		
		code = result;


		String parsedCode = PDTManager.parseQrCode(code);
		if (parsedCode!=null) {
			loadDataFromDevice(code);
						
		}
		else {
			Log.e("MainActivity-manageData","QRValue is wrong. Code=" + code);
			String message = getResources().getString(R.string.wrong_code_device ) + "\n code: " + code;
			NotificationViewerActivity.show(this, message, 5000, R.drawable.nodevice);

		}
	}

	// ** Device management

public static boolean isCheckingDeviceAvailability = false;	
	private void checkDeviceAvailable() {
		if (!loadDevice ) return;
		if (isCheckingDeviceAvailability ) return;
		isCheckingDeviceAvailability  = true;
				if (manager.device==null) {
	readQRCode();
 } else {
	 isCheckingDeviceAvailability = false; 
 }
}
	
	private void loadDataFromDevice(String deviceQR) {
				Intent i = new Intent(this, LoadingActivity .class);
		Bundle bundle = new Bundle();
		bundle.putString("qr", deviceQR);
		i.putExtras(bundle); 
		startActivity(i);
	}

	
	// ** Login management
	
	
	public boolean checkIsLogIn() {
		boolean result = true; // globals.isLogin;
		if (result == false) {
			Resources res = this.getResources();
			String text = res.getString(R.string.you_need_login);
			long time = 5000;
			NotificationViewerActivity.show(this, text, time, R.drawable.nodevice);
		} else {
LogManager.setUser("User");
		}
				return result;
		
	}
	
	// ** Bluetooth management
	
	public void receivedLoginDataFromBluetooth(String data) {
Log.w("MainActivity-receivedLoginDataFromBluetooth","Data=" + data);
if (data!=null) {
	globals.logInUser(data, true, true, true);
	LogManager.setUser(data);
LogManager.addUserTask("Logged in from remote device");		
		bluetoothService.initApplication();
} else {
globals.logout();
tts.speak("Log out the user");
LogManager.addSystemTask("User is logged out from remote device");
}
	}
	
	public void receivedDataFromBluetooth(String data) {
		Log.w("MainActivity-receivedDataFromBluetooth","Data=" + data);
		
		
	}
	
	public void receivedLoginStatusFromBluetooth(boolean status) {
 		
	}
	
	public void changeStatusConnection(boolean connected) {
if (connected) {
	Log.w("MainActivity-ChangebluetoothStatus", "Connected");
	LogManager.addSystemTask("Device connected to bluetooth remote device");
	tts.speak("Connected");
}
else {
	Log.w("MainActivity-ChangebluetoothStatus", "Connected");
	LogManager.addSystemTask("Device disconnected to bluetooth remote device");
	}
	}


}
