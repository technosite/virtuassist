package com.technosite.virtuaglass.controls;

import android.annotation.SuppressLint;
import android.os.Build;
import android.widget.ImageView;

public class ImageViewManager {


	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void changeAlpha(ImageView image, int value) {
		if (Build.VERSION.SDK_INT>=16) image.setImageAlpha(value);		
		else image.setAlpha(value);
			
	}
}
