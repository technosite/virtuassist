package com.technosite.virtuaglass.opencv;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.technosite.virtuaglass.model.Globals;

public class ImageManager {
	
	private static ImageManager _instance = null;
	
	public static ImageManager getSharedInstance() {
		if (_instance==null) _instance = new ImageManager();
		return _instance;
	}
	
	public ImageManager () {

		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, Globals.context, mLoaderCallback);
	}
	
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(Globals.activity) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.w("ImageManager", "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("mixed_sample");
                                    } break;
                default:
                {
                	Log.w("ImageManager", "OpenCV not loaded");
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
		
		
		// ** Methods

	
	public Bitmap parse(byte[] data, int width, int height) {
		Log.w("ImageManager", "parse");
		Bitmap result = null;
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		result= BitmapFactory.decodeByteArray(data, 0, data.length,bmpFactoryOptions);

		if (result==null) {
			Log.w("ImageManager", "Saving the byte array to SDCard and load the jpg file using OpenCV functions");
			saveFileToSDCard("imgcv.jpg",data);
			Mat m = Highgui.imread(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +"imgcv.jpg");
			result = convertMatToBitmap(m);
			
		}
		if (result==null) Log.e("*ImageManager*", "error parsing the image from Mat decoding");
return result;		
	}
	
	public Bitmap getRegion(Bitmap bitmap, int x, int y, int width, int height) {
Bitmap result = null;
Mat original = null;
org.opencv.android.Utils.bitmapToMat(bitmap, original);
Rect roi = new Rect(x, y, width, height);
Mat cropped = new Mat(original, roi);
org.opencv.android.Utils.matToBitmap(cropped , result);
return result;
	}
	
	public Mat convertBitmapToMat(Bitmap bmp) {
		Log.w("ImageManager", "convertBitmapToMat");
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		
		Bitmap bmpTmp = BitmapFactory.decodeByteArray(byteArray , 0, byteArray .length,bmpFactoryOptions);
		Mat result  = null;
		if (bmpTmp!=null) {
			Log.w("ImageManager", "convertBitmapToMat using OpenCV functions");
			result= new Mat();
			Utils.bitmapToMat(bmpTmp, result);
		} else Log.e("ImageManager", "Error convertBitmapToMat using OpenCV functions. There is no Bitmap buffer available. BitmapFactory error.");
		
		
		return result;
	}
	
	public Bitmap convertMatToBitmap(Mat mat) {
		Log.w("ImageManager", "convertMatToBitmap");
Bitmap bmp= null;
Mat tmp = new Mat (mat.rows(), mat.cols(), CvType.CV_8U, new Scalar(4));
try {
    //Imgproc.cvtColor(seedsImage, tmp, Imgproc.COLOR_RGB2BGRA);
    Imgproc.cvtColor(mat, tmp, Imgproc.COLOR_GRAY2RGBA, 4);
    bmp = Bitmap.createBitmap(tmp.cols(), tmp.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(tmp, bmp);
}
catch (CvException e){
	Log.e("ImageManager-Exception","Error in convertMatToBitmap:" + e.getMessage());
	}
return bmp;
	}
	
	
	public int[] getColorRegionInHMat(Mat mat, int color) {
		int l = -1;
		int r = -1;
		int t = -1;
		int b = -1;

for (int j=0;j<mat.rows();j++) {
		for (int i=0;i<mat.cols();i++) {
int[] clr = {0}; 
mat.get(j, i, clr);
if (clr[0] == color) {
		if (l==-1) l = i;
	else if(l>i) l=i;
	if (r==-1) r=i;
	else if (r<i) r= i;
	if (t==-1) t=j;
	else if(t>j) t=j;
	if (b==-1) b=j;
	else if (b<j) b=j;
	}
	}
}
int[] result = {l,t,r,b};
return result;
	}
	
	public int[] calculateTonesByHistogram(Bitmap image) {
		int[] error = {-1,-1,-1};
		if (image==null) {
			Log.e("calculateTonesByHistogram", "bitmap image is null");
			return error;
		}
		Log.w("calculateTonesByHistogram", "Calculating tones from a histogram");
		Mat img = convertBitmapToMat((Bitmap) image);
		if (img==null) {
			Log.e("calculateTonesByHistogram", "mat image is null");
			return error;
		}
		Log.w("calculateTonesByHistogram", "Calling OpenCV functions");
		ControlDetection  cd = new ControlDetection ();
		Mat histogram = cd.calculateHistogram(img);
		if (histogram==null) {
			Log.e("calculateTonesByHistogram", "Histogramimage is null");
			return error;
		}
		int[] colors = cd.calculateColoursHistogram(histogram);
				
		return colors;
		
	}

	private void saveFileToSDCard(String fileName, byte[] data) {
		String filename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName;
		
		// Save the file
		File photo=new File(filename);
		if (photo.exists()) photo.delete();
		try {
			FileOutputStream fos=new FileOutputStream(photo.getPath());					
			fos.write(data);
			fos.close();
		} catch (java.io.IOException e) {
Log.e("Saving image to SDCard", "Error saving the image to SDCard memory. " +e);					
		}
	}
	
	
}
