package com.technosite.virtuaglass.opencv;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import android.graphics.Bitmap;
import android.util.Log;

import com.technosite.virtuaglass.ControlDetailsActivity;
import com.technosite.virtuaglass.DeviceExplorerActivity;
import com.technosite.virtuaglass.model.PDTControl;
import com.technosite.virtuaglass.model.PDTManager;

public class HandGesture {
	
	public final static int timeToWait = 20; // Time to wait
	
	public DeviceExplorerActivity delegate = null;
	public List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
	public int cMaxId = -1;
	public Mat hie = new Mat();
	public List<MatOfPoint> hullP = new ArrayList<MatOfPoint>();
	public MatOfInt hullI = new MatOfInt();
	public Rect boundingRect;
	public MatOfInt4 defects = new MatOfInt4();
	public Bitmap controlImg = null;
	public String controlText = "";	
	public List<Point> mFingerTips = new ArrayList<Point>();
	public MatOfPoint2f approxContour = new MatOfPoint2f();
	public Point inCircle = new Point();
	public double inCircleRadius;
	private boolean isHand = false;
	private PDTControl lastControl = null;
	public boolean identifyControl(Mat img, Mat cleanImg, Mat noBlur, Mat mH, Mat mS, Point point, int handSize){//, double z){
		if (DeviceExplorerActivity .CVOperationAvailable == false) return false;;
		if (ControlDetailsActivity.showingAControl) return false;
		Log.w("HandGesture-identifyControl", "Init procedure");
		
		ControlDetection controlDetection = ControlDetection.getSingleton();
		
		//Obtain the rectangle with controls of the printer
		//Log.i("gyroscope", "x="+x*180/Math.PI+", y="+y*180/Math.PI+", z="+z*180/Math.PI);
		controlDetection.foundPrinter=false;
		controlDetection.findUmbralRGB(img, noBlur, point, inCircleRadius);//, z);//rectangle with panel control of the printer
		
		
		////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
		//El limits tiene que tener ya en cuenat los controles y sin offest (cambiarlo e el findUmbral) 
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
		if(controlDetection.foundPrinter){
			Log.w("HG-IdentifyControl", "Found printer");
			List<Point> points = controlDetection.limits;

			// Cartography to detect controls
			Log.w("HG-Identify", "Calling cartography");
			PDTManager m = PDTManager.getSharedInstance();
			PDTControl control  = null;
			if (m.device!=null) {
				if (m.device.geometry!=null) {
					control = m.device.geometry.identifyControlAtPoint(point, points.get(0), points.get(1));		
				} else Log.e("HG-IdentifyControl", "Geometry for PDTDevice is null");
			} else Log.e("HG-IdentifyControl", "PDTDevice is null");
				
			if (control!=null) 
				if (lastControl==null || control.id!= lastControl.id) {
					Log.w("HG-recognized control", "Control " + control.name + " at (" + control.x + "," + control.y +") is found");
					lastControl = control;
										Log.w("HG-recognized control", "Preparing data to show");
					this.controlImg = (Bitmap) control.image;
					String textForControl = control.name;
					if (control.description!=null) 
						if (!control.description.equalsIgnoreCase("null")) textForControl  = control.name + " - " + control.description;
					this.controlText = textForControl ;
					return true;
				} else {
					Log.w("HG-recognized control","Same control" );
					return false;					
				}
			else return false;
		}
		else{
			Log.w("HG-IdentifyControl", "Not found printer");
			return false;
		}
	}
	
	
	public void findBiggestContour() 
	{
		int idx = -1;
		int cNum = 0;
		
		for (int i = 0; i < contours.size(); i++)
		{
			int curNum = contours.get(i).toList().size();
			if (curNum > cNum) {
				idx = i;
				cNum = curNum;
			}
		}
		
		cMaxId = idx;
	}
	
	/*public boolean detectIsHand(Mat img)
	{
		int centerX = 0;
		int centerY = 0;
		if (boundingRect != null) {
			centerX = boundingRect.x + boundingRect.width/2;
			centerY = boundingRect.y + boundingRect.height/2;
		}
		if (boundingRect == null) {
			isHand = false;
		} else if ((boundingRect.height == 0) || (boundingRect.width == 0))
			isHand = false;
		//else if ((centerX < img.cols()/4) || (centerX > img.cols()*3/4))
			//isHand = false;
		else
			isHand = true;
		return isHand;
	}*/
	
	public native double findInscribedCircleJNI(long imgAddr, double rectTLX, double rectTLY,
			double rectBRX, double rectBRY, double[] incircleX, double[] incircleY, long contourAddr);
	
	public void findInscribedCircle(Mat img)
	{
		
		Point tl = boundingRect.tl();
		Point br = boundingRect.br();
		
		double[] cirx = new double[]{0};
		double[] ciry = new double[]{0};
		
		inCircleRadius = findInscribedCircleJNI(img.getNativeObjAddr(), tl.x, tl.y, br.x, br.y, cirx, ciry, approxContour.getNativeObjAddr());
		inCircle.x = cirx[0];
		inCircle.y = ciry[0];
		
		/*if (inCircleRadius<=3){
			int newX = (int)inCircle.x+(int)inCircleRadius+1;
			int newY = (int)inCircle.y+(int)inCircleRadius+1;
			////////////////////////////////////////////////////////////////////////////////////////////
			
			Mat mH = new Mat();
			Imgproc.cvtColor(img, mH, Imgproc.COLOR_RGB2HSV, 0);
			double[] hsv = mH.get(newX,newY);
			PointsManager pm = PointsManager.getSingleton();
			pm.avgColorH[pm.HAND_POINTS-1] = hsv[0];
		}*/
			
		//Core.circle(img, inCircle, (int)inCircleRadius, new Scalar(240,240,45,0), 2);
		//Core.circle(img, inCircle, 3, Scalar.all(0), -2);
	}
	
	public boolean handleFingerTips(Mat img, Mat cleanImg, Mat noBlur, Mat mH, Mat mS, int handSize){//, double z)	{
		List<Point> tempFingerTips2 = filterClosePoints(mFingerTips);
		mFingerTips.clear();
		mFingerTips = tempFingerTips2;
		
		Scalar[] color = new Scalar[4];
		color[0] = new Scalar(255,0,0,0);//Red
		color[1] = new Scalar(0,255,0,0);//Green
		color[2] = new Scalar(0,0,255,0);//Blue
		color[3] = new Scalar(0,0,0,0);//Black
		Scalar[] color2 = new Scalar[6];
		color2[0] = new Scalar(255,255,0,0);
		color2[1] = new Scalar(0,255,255,0);
		color2[2] = new Scalar(255,0,255,0);
		color2[3] = new Scalar(255,255,128,0);
		color2[4] = new Scalar(128,255,255,0);
		color2[5] = new Scalar(255,128,255,0);

		// Paint in red all radious
		//drawVectors(img, mFingerTips, inCircle, color[0]);
		
		
		double closeRadius = Math.pow(inCircleRadius * 1.5, 2);
		double farRadius   = Math.pow(inCircleRadius * 4.5, 2);
		double minAngle    = 315;
		double maxAngle    = 135;
		
		tempFingerTips2 = filterTipsByDistance (mFingerTips, farRadius, closeRadius);
		mFingerTips.clear();
		mFingerTips = tempFingerTips2;
		
		// Paint in green all radious filtered by length
		//drawVectors(img, mFingerTips, inCircle, color[1]);
		
		
		List<Double> angles = calculateAngles (mFingerTips, inCircle, true);
		double[][] mAngles = calculateSmallestAngleBetwenFingerTips (angles);
		List<List<Integer>> listClusters = clusterizeByAngles (mAngles);

		/*if (!listClusters.isEmpty())
		{
			int c = 0;
			for (List<Integer> li : listClusters)
			{
				tempFingerTips2 = new ArrayList<Point>();
				for (int i : li)
				{
					if (i < mFingerTips.size())
						tempFingerTips2.add(mFingerTips.get(i));
				}
				// Different clusters are painted with different colours
				drawVectors(img, tempFingerTips2, inCircle, color2[c % 6]);
				c++;
			}
		}*/
		
		
		listClusters = filterClustersByDirectorAngle(listClusters, mFingerTips, inCircle, minAngle, maxAngle);
		
		/*//Paint circles in the points not filtered
		if (!listClusters.isEmpty())
		{
			int id = 0;
			for (List<Integer> li : listClusters)
			{
				
				for (int i : li)
					if (i < mFingerTips.size())						
						Core.circle(img, mFingerTips.get(i), 10, color[id % 3], -5);
				id++;
			}
		}*/
		
		Point p = calculateIndexFinger(listClusters, mFingerTips);//, palmCenter);//,img);
				
		boolean identifiedControl = false;
		if (p!=null){
			Core.circle(img, p, 10, color[3], -5);
			/*// split img:
			List<Mat> channels = new ArrayList<Mat>(3);
			Core.split(mH, channels);
			// get the channels (dont forget they follow BGR order in OpenCV)
			mH = new Mat();
			mH = channels.get(0);*/
			identifiedControl = identifyControl(img, cleanImg, noBlur, mH, mS, p, handSize);//, z);
		}
		return identifiedControl;
	}
	
	private List<Point> filterClosePoints(List<Point> fingerTips)
	{
		List<Point> out = new ArrayList<Point>();
		
		for (Point p : fingerTips)
		{
			for (Point p2 : fingerTips)
			{
				if(p!=p2){
					if(distanceP2P(p,p2)>=10){
						out.add(p);	
						break;
					}
				}				
			}
		}
		return out;		
			
	}
	
	private double distanceP2P(Point a, Point b){
		double d= Math.sqrt((Math.pow(a.x-b.x,2) + Math.pow(a.y-b.y,2)));
		return d;
	}
	
	private void drawVectors(Mat img, List<Point> fingerTips, Point origin, Scalar color)
	{
		for (Point p : fingerTips)
			Core.line(img, origin, p, color, 2);
	}
		
	private List<Point> filterTipsByDistance (List<Point> fingerTips, double farRadius, double closeRadius)
	{
		List<Point> out = new ArrayList<Point>();
		
		// Filter radious per length. Valid only those longer than 2*r and lower than 3*r
		for (Point p : mFingerTips)
		{
			double length = Math.pow(p.x - inCircle.x, 2) + Math.pow(p.y - inCircle.y, 2);
			if (length > closeRadius/* && length < farRadius*/)
				out.add(p);
		}
		return out;
	}
	
	private List<Double> calculateAngles(List<Point> fingerTips, Point origin, boolean normalize)
	{
		List<Double> out = new ArrayList<Double>();
		for (Point p : fingerTips)
		{
			double angle = 180 * Math.atan2(origin.y - p.y, origin.x - p.x) / Math.PI;
			if (normalize)
				angle = angle < 0 ? angle + 360 : angle > 360 ? angle - 360 : angle;
			out.add(angle);
		}
		return out;
	}
	
	private double[][] calculateSmallestAngleBetwenFingerTips (List<Double> angles)
	{
		int anglesSize = angles.size();
		double[][] mAngles = new double[anglesSize][anglesSize];
		for (int i = 0; i < anglesSize;i++)
		{
			for (int j = i + 1; j < anglesSize;j++)
			{
				double angle = Math.abs(angles.get(i) - angles.get(j));
				double angle2 = Math.abs(360 - angle);
				angle = angle < angle2 ? angle : angle2;
				mAngles[i][j] = mAngles[j][i] = angle;
			}
		}
		return mAngles;
	}
		
	private List<List<Integer>> clusterizeByAngles(double[][] mAngles)
	{
		double groupingAngle = 50;
		
		List<List<Integer>> out = new ArrayList<List<Integer>>();
		out.add(new ArrayList<Integer>());
		if (mAngles.length > 0 && mAngles[0].length > 0)
		{
			out.get(0).add(0);
			
			List<Integer> pendingTips = new ArrayList<Integer>();
			for (int i = 1; i < mAngles[0].length; i++)
				pendingTips.add(i);
			
			while (!pendingTips.isEmpty())
			{
				List<Integer> addedTips = new ArrayList<Integer>();
				for (Integer i : pendingTips)
				{
					for (List<Integer> li : out)
					{
						boolean added = false;
						for (Integer i2 : li)
						{
							int min = i <  i2 ? i : i2;
							int max = i >= i2 ? i : i2;
							if (mAngles[min][max] < groupingAngle)
							{
								li.add(i);
								addedTips.add(i);
								added = true;
								break;
							}
						}
						if (added) break;
					}
				}
				if (addedTips.isEmpty())
				{
					List<Integer> newCluster = new ArrayList<Integer>();
					newCluster.add(pendingTips.get(0));
					pendingTips.remove(0);
					out.add(newCluster);
				}
				else
				{
					for(Integer i : addedTips)
					{
						pendingTips.remove(i);
					}
				}
			}
		}
		return out;
	}

	private List<List<Integer>> filterClustersByDirectorAngle(List<List<Integer>> clusters, List<Point> fingerTips, Point origin, double minAngle, double maxAngle)
	{
		boolean ordered = minAngle < maxAngle;
		List<List<Integer>> out = new ArrayList<List<Integer>>();
		for (List<Integer> li : clusters)
		{
			List<Point> clusterFingerTips = new ArrayList<Point>();
			for (Integer i : li)
				clusterFingerTips.add(fingerTips.get(i));
			
			Point p = new Point(0, 0);
			for (Point p2 : clusterFingerTips)
			{
				p.x += p2.x;
				p.y += p2.y;
			}
			p.x /= clusterFingerTips.size();
			p.y /= clusterFingerTips.size();
			double directorAngle = 180 * Math.atan2(origin.y - p.y, origin.x - p.x) / Math.PI;
			directorAngle = directorAngle < 0 ? directorAngle + 360 : directorAngle > 360 ? directorAngle - 360 : directorAngle;
			
			if (ordered)
			{
				if (directorAngle >= minAngle && directorAngle <= maxAngle)
					out.add(li);
			}
			else
			{
				if (directorAngle >= minAngle || directorAngle <= maxAngle)
					out.add(li);
			}
		}		
		
		return out;
	}

	private Point calculateIndexFinger(List<List<Integer>> clusters, List<Point> fingerTips){
		List<Point> indexFingers = new ArrayList<Point>();
		
		for (List<Integer> li : clusters)
		{
			List<Point> clusterFingerTips = new ArrayList<Point>();
			for (Integer i : li)
				clusterFingerTips.add(fingerTips.get(i));
			
			Point possibleIndexFinger = clusterFingerTips.get(0);
			
			for (int i = 1; i<clusterFingerTips.size();i++)
			{
				if ((clusterFingerTips.get(i).y-possibleIndexFinger.y)<0){
					possibleIndexFinger = clusterFingerTips.get(i);
				}
			}
			indexFingers.add(possibleIndexFinger);			
		}		
		
		if (!clusters.isEmpty()){
			Point indexFinger = indexFingers.get(0);
			for (int i = 1; i<indexFingers.size();i++)
			{
				if ((indexFingers.get(i).y-indexFinger.y)<0){
					indexFinger = indexFingers.get(i);
				}
			}
			return indexFinger;
		}
		else{
			return null;
		}
		
	}

    


    }
