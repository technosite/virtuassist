package com.technosite.virtuaglass.opencv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.util.Log;

//import org.opencv.core.Mat;

public class ControlDetection {
	private final int offset = 10;
	public List<Point> limits = new ArrayList<Point>();
	public boolean foundPrinter = false;	
	public static ControlDetection instance = null;
	
	public static ControlDetection getSingleton ()
	{
		if (instance == null) {
			synchronized (ControlDetection.class) {
				ControlDetection inst = instance;
				if (inst == null) {
					synchronized (ControlDetection.class) {
						inst = new ControlDetection();
					}
					instance = inst;
				}
			}
		}
		return instance;
	}

	public static void reset() {
		instance = null;
	}

	
	public ControlDetection(){
		limits.add(new Point(-1,-1));
	}
	
	public void findUmbralRGB(Mat rgb, Mat noBlurImg, Point pIndexFinger, double inCircleRadious){//, double z){
		Mat	out = new Mat();//Rectangle image
				
		//Log.i("CD","1");
		//Imgproc.GaussianBlur(noBlurImg, noBlurImg, new Size(3,3), 3, 3);
		//Log.i("CD","2");
		// convert RGB image to gray
		Mat gray = new Mat();
		Imgproc.cvtColor(noBlurImg, gray, Imgproc.COLOR_BGR2GRAY);		
		//Log.i("CD","3");
		Mat dst = new Mat(gray.rows(), gray.cols(), CvType.CV_8UC1);
		//Log.i("CD","4");
		
		dst.setTo(new Scalar(0));
		Mat detected_edges = new Mat();
		int lowThreshold = 40;
		int ratio = 3;
		int kernel_size = 3;

		// Canny detector
		Imgproc.Canny(gray, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size, true);
		//Log.i("CD","5");
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hie = new Mat();
		Imgproc.findContours(detected_edges, contours, hie, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
		//Log.i("CD","6");
		//Returns the rotactedRect ordered from left to right of each contour in the rectangle of interest 
		foundPrinter = false;
		limits = filterContoursByPosition (rgb,contours, pIndexFinger, inCircleRadious);
		//Log.i("CD","7");
		
		//Log.i("CD","8");
		
		//out = obtainRectangle(noBlurImg, pIndexFinger);//, z);
		//Log.i("CD","9");
		//return out;
	}
	
	private native void rotateImage(long addrIn, long addrOut, double angle);
	
	private Mat obtainRectangle(Mat rgb, Point pIndexFinger){//, double z){
		Mat rectangle = new Mat();
		
		int width = (int)(rgb.cols()/4.0);
		
		//Log.i("gyroscope", "z="+z*(180/Math.PI));
		//Mat rotImage = Imgproc.getRotationMatrix2D(new Point(rgb.rows()/2, rgb.cols()/2), z*(180/Math.PI), 1.0);
		//Imgproc.warpAffine(rgb, rgb, rotImage, rgb.size(),Imgproc.INTER_LINEAR, Imgproc.BORDER_CONSTANT, new Scalar(122, 22, 22));
		
		Point pLeft = new Point(pIndexFinger.x-width,pIndexFinger.y-30);		
		pLeft.y = Math.min(rgb.rows()-1, pLeft.y);
		pLeft.y = Math.max(0, pLeft.y);
		pLeft.x = Math.min(rgb.cols()-1, pLeft.x);
		pLeft.x = Math.max(0, pLeft.x);
		
		Point pRight = new Point(pIndexFinger.x+width,pIndexFinger.y+30);
		pRight.y = Math.min(rgb.rows()-1, pRight.y);
		pRight.y = Math.max(0, pRight.y);
		pRight.x = Math.min(rgb.cols()-1, pRight.x);
		pRight.x = Math.max(0, pRight.x);
		
		//Draw rectangle
		//Core.rectangle(rgb, pLeft, pRight, new Scalar(255,255,255,0), 2);		
		Rect rectangleRect = new Rect();
		
		//Obtain the submat with the the control panel
		Mat aux = new Mat();
		rgb.copyTo(aux);
		rectangle = aux.submat((int)pLeft.y, (int)pRight.y, (int)pLeft.x, (int)pRight.x);
		
		
		return rectangle;
	}
		
	public List<Point> filterContoursByPosition (Mat rgb, List<MatOfPoint> contours, Point hand, double inCircleRadious) {
		List<Rect> result = new ArrayList<Rect>();
		for (int i=0; i<contours.size(); i++){
			Rect aux = Imgproc.boundingRect(contours.get(i));
			result.add(aux);
		}

		List<Double> pointsX = new ArrayList<Double>();
		List<Point> points = new ArrayList<Point>();
		for (int i=0; i<result.size(); i++){
			Rect aux = result.get(i);
			if ((double)aux.br().y < ((double)hand.y)){
				if((double)aux.br().y>((double)hand.y-30)){	
					if ((aux.br().x<(((double)rgb.cols()*3.0)/4.0))){// && (aux.br().x>((double)rgb.cols()/4.0))){
						pointsX.add(aux.br().x);
						//pointsX.add((aux.br().x-aux.tl().x)/4.0+aux.tl().x);
						//pointsX.add(((aux.br().x-aux.tl().x)/4.0)*2+aux.tl().x);
						//pointsX.add(((aux.br().x-aux.tl().x)/4.0)*3+aux.tl().x);
						pointsX.add(aux.tl().x);
						points.add(new Point(aux.tl().x, aux.br().y));
						points.add(aux.br());					
					}
				}				
			}
		}
		
		
		
		//for (int i=0; i<points.size(); i++)
			//Core.circle(rgb, points.get(i), 2, new Scalar(255,255,0,0), 2);
		
		Collections.sort(pointsX);
		//double[][] distances = calculateSmallestDistanceBetwenPoints (pointsX);
		List<List<Double>> listClusters = clusterizeByDists (rgb.cols(),pointsX,50);//distances, inCircleRadious*2.0);
		double[] out = selectClustersByHandPoint(rgb, listClusters, hand);
		
		if(foundPrinter){
			Core.circle(rgb, new Point(out[0],hand.y), 10, new Scalar(0,0,0,0), -5);
			Core.circle(rgb, new Point(out[1],hand.y), 10, new Scalar(0,0,0,0), -5);
		}
		
		List<Point> outPoints = new ArrayList<Point>();
		outPoints.add(new Point(out[0],hand.y));
		outPoints.add(new Point(out[1],hand.y));
		return outPoints;
		//Collections.sort(pointsX);
	}
	
	
	/*private double[][] calculateSmallestDistanceBetwenPoints (List<Double> points)
	{
		int distancesSize = points.size();
		double[][] mDists = new double[distancesSize][distancesSize];
		for (int i = 0; i < distancesSize;i++)
		{
			for (int j = i + 1; j < distancesSize;j++)
			{
				double dist = Math.abs(points.get(i) - points.get(j));
				mDists[i][j] = mDists[j][i] = dist;
			}
		}
		return mDists;
	}
	*/
	private double distanceP2P(Point a, Point b){
		double d= Math.sqrt((Math.pow(a.x-b.x,2) + Math.pow(a.y-b.y,2)));
		return d;
	}
		
	private List<List<Double>> clusterizeByDists(int cols, List<Double> points, double rangeCluster){//double[][] mDists, double rangeCluster, List<Point> points)
	
		
		List<List<Double>> clusters = new ArrayList<List<Double>>();
		
		for(int i=0;i<points.size();i++){
			//Point p = points.get(i);
			boolean inserted = false;
			for(int j =0;(j<clusters.size() && (!inserted));j++){
				for(int k=0; (k<clusters.get(j).size() && (!inserted));k++){
					if(Math.abs(points.get(i)-clusters.get(j).get(k))<rangeCluster){
						//if((points.get(i)>((double)cols/4.0)) && (points.get(i)<(((double)cols*3.0)/4.0))){
							clusters.get(j).add(points.get(i));
							inserted=true;
						//}
					}
				}
			}
			if(!inserted){
				List<Double> newCluster = new ArrayList<Double>();
				newCluster.add(points.get(i));
				clusters.add(newCluster);
			}
		}
		
		
		return clusters;
		
		/*List<List<Integer>> out = new ArrayList<List<Integer>>();
		out.add(new ArrayList<Integer>());
		if (mDists.length > 0 && mDists[0].length > 0)
		{
			out.get(0).add(0);
			
			List<Integer> pendingPoints = new ArrayList<Integer>();
			for (int i = 1; i < mDists[0].length; i++)
				pendingPoints.add(i);
			
			while (!pendingPoints.isEmpty())
			{
				List<Integer> addedTips = new ArrayList<Integer>();
				for (Integer i : pendingPoints)
				{
					for (List<Integer> li : out)
					{
						boolean added = false;
						for (Integer i2 : li)
						{
							int min = i <  i2 ? i : i2;
							int max = i >= i2 ? i : i2;
							if (mDists[min][max] < rangeCluster)
							{
								li.add(i);
								addedTips.add(i);
								added = true;
								break;
							}
						}
						if (added) break;
					}
				}
				if (addedTips.isEmpty())
				{
					List<Integer> newCluster = new ArrayList<Integer>();
					newCluster.add(pendingPoints.get(0));
					pendingPoints.remove(0);
					out.add(newCluster);
				}
				else
				{
					for(Integer i : addedTips)
					{
						pendingPoints.remove(i);
					}
				}
			}
		}*/
		
	}
		
	private double[] selectClustersByHandPoint(Mat rgb, List<List<Double>> clusters, Point hand)
	{
		double[] out = new double[2];
		out[0] = Double.MAX_VALUE;
		out[1] = 0.0;
		
		Scalar[] colors = new Scalar[20]; 
		colors[0] = new Scalar(0,255,255,0);
		colors[1] = new Scalar(255,0,255,0);
		colors[2] = new Scalar(255,255,0,0);
		colors[3] = new Scalar(255,255,255,0);
		colors[4] = new Scalar(0,0,0,0);
		colors[5] = new Scalar(150,255,255,0);
		colors[6] = new Scalar(255,50,255,0);
		colors[7] = new Scalar(255,100,255,0);
		colors[8] = new Scalar(255,150,255,0);
		colors[9] = new Scalar(255,255,50,0);
		colors[10] = new Scalar(255,255,100,0);
		colors[11] = new Scalar(255,255,150,0);
		colors[12] = new Scalar(50,50,50,0);
		colors[13] = new Scalar(100,50,50,0);
		colors[14] = new Scalar(50,100,50,0);
		colors[15] = new Scalar(50,50,100,0);
		colors[16] = new Scalar(150,50,50,0);
		colors[17] = new Scalar(50,150,50,0);
		colors[18] = new Scalar(50,50,150,0);
		colors[19] = new Scalar(100,100,100,0);
		
		for(int j=0; j<clusters.size();j++)
			for(int k=0;k<clusters.get(j).size();k++)
				Core.circle(rgb, new Point(clusters.get(j).get(k),hand.y), 2, colors[j%20], 2);
		
		//Limits of clusters of interest
		int cols = rgb.cols();
		double min = 0.0;//cols/4.0;
		double max = cols*3.0/4.0;
		if (hand.x<=min)
			min = hand.x-10;
		else if (hand.x>=max)
			max = hand.x+10;
		
		
		//boolean detected = false;
		out[0] = rgb.cols();
		out[1] = 0.0;
		double left = rgb.cols();
		double right = 0.0;
		//double clusterLeft_left = hand.x;
		//double clusterLeft_right = 0;
		//double clusterRight_left = rgb.cols();
		//double clusterRight_right = hand.x;
		//boolean handIncluded = false;		
		
		for(int i=0; i<clusters.size();i++){
			//Calculate the right and left limits of the cluster
			left = rgb.cols();
			right = 0.0;
			double x = 0.0;
			if(clusters.get(i).size()==1){
				left = clusters.get(i).get(0);
				right = clusters.get(i).get(0);
			}
			else{
				for(int k=0;k<clusters.get(i).size();k++){
					x= clusters.get(i).get(k);
					if (x<left){
						left=x;
					}
					else if (x>right){
						right=x;
					}
				}	
			}
		
			//Search the limits of the printer
			if ((right>=min) && (right<=max)){
				if (right>out[1])
					out[1]= right;
				if(left<out[0])
					out[0]=left;	
				foundPrinter=true;
			}else if ((left<=max) && (left>=min)){
				if (right>out[1])
					out[1]= right;
				if(left<out[0])
					out[0]=left;
				foundPrinter=true;
			}			
		}
		
		if (limits.get(0).x>=0.0){
			Log.w("ControDetection-selectClustersByHandPoint", "(out[1]-out[0])<=(((limits.get(1).x-limits.get(0).x)*2.0)/3.0) --> "+out[1]+"-"+out[0]+"<="+limits.get(1).x+"-"+limits.get(0).x+"*2.0/3.0");
			if ((out[1]-out[0])<=(((limits.get(1).x-limits.get(0).x))/2.0)){
				Log.w("ControDetection-selectClustersByHandPoint", "true");
				
				out[0] = limits.get(0).x;
				out[1] = limits.get(1).x;
			}
		}
		
		if (out[0]>=hand.x){
			out[0]= hand.x-10.0;
			if(out[0]<0.0)
				out[0]=0.0;
			if (limits.get(0).x>=0.0)
				if(out[0]>=limits.get(0).x)
					out[0]=limits.get(0).x;			
		}		
		
		if (out[1]<=hand.x){
			out[1] = hand.x+10.0;
			if(out[1]>=rgb.cols())
				out[1]=rgb.cols()-1.0;
			
			if (limits.get(0).x>=0.0)
				if(out[1]<limits.get(1).x)
					out[1]=limits.get(1).x;		
		}
		
		return out;
		
		/*double[] out = new double[2];
		for (List<Point> li : clusters)
		{
			
			
			for (Point i : li){
				
				Core.circle(rgb, i, 2, colors[j%20], 2);
				j++;
			}
			double xMin = Double.MAX_VALUE;
			double xMax = 0.0;
			for (double p2 : clusterPoints)
			{
				if (p2<xMin)
					xMin = p2;
				else if (p2>xMax)
					xMax = p2;
			}
			
			if ((hand.x<=xMax) && (hand.x>=xMin))
			{
				out[0]=xMin;
				out[1]=xMax;
			}
		}		
		
		return out;*/
	}
	
		/* Function to calculate the color histogram of an image (to know the colors of a control in a surface) in RGB
	In: image to be analyzed
	Out: color histogram
	*/
		public native void calculateHistogramJNI(long imageIn, long imageOut);
		
		public Mat calculateHistogram (Mat imageRGB){
			//In RGB out, hist of H	
			Mat mH = new Mat();
			Imgproc.cvtColor(imageRGB, mH, Imgproc.COLOR_RGB2HSV, 0);
			List<Mat> channels = new ArrayList<Mat>(3);
	    	Core.split(mH, channels);
	    	mH = new Mat();
	    	mH = channels.get(0);
	    	
	    	Mat hist = new Mat();
			calculateHistogramJNI(mH.getNativeObjAddr(), hist.getNativeObjAddr());		
			
			return hist;
		}
		
		/*
		 * Function to calculate the main colour in the image
		 * In: Histogram of a piece of the image
		 * Out: int[0] maximum colour, int[1] colour with low value in the variance, int[2] colour with high value in the variance
		 */
		public int[] calculateColoursHistogram (Mat hist){			
			int[] colours = new int[3];
			colours[1]=-1;
			colours[2]=-1;
			double meanAux = 0;
			int indexMean = 0;
			
			//Log.i("Hist","Cols="+hist.cols()+", rows="+hist.rows()+", hist(0,0).size="+hist.get(0,0).length);
			for (int i = 1; i<hist.rows();i++){
				//Log.i("Hist","hist.get("+i+",0)[0]="+hist.get(i,0)[0]);
				if (hist.get(i,0)[0]>meanAux){
					meanAux=hist.get(i,0)[0];
					colours[0] = i;
				}
			}			
			//Log.i("Hist","meanAux="+meanAux+", colours[0]="+colours[0]);
			
			for (int i = colours[0]; i<hist.rows();i++)
				if (hist.get(i,0)[0]<=meanAux*0.3){
					colours[2] = i;
					i=hist.rows();
				}
			if (colours[2]<0)
				colours[2] = hist.rows();
			//Log.i("Hist","colours[2]="+colours[2]);
			
			for (int i = colours[0]; i>0;i--)
				if (hist.get(i,0)[0]<=meanAux*0.3){
					colours[1] = i;
					i=0;
				}
			if (colours[1]<0)
				colours[1] = 1;
			//Log.i("Hist","colours[1]="+colours[1]);
			
			return colours;
		}
		
	/*
	 * Return a Mat filtered by a given color
	 */

		private void filterByColor (Mat imgH, Mat imgHFiltered, int meanColor, int minColor, int maxColor)
		    {
			 Mat sampleMat = new Mat();	
			 Core.inRange(imgH, new Scalar(minColor), new Scalar(maxColor), sampleMat);
			
			 imgHFiltered.release();
			 sampleMat.copyTo(imgHFiltered);
		    }
	
}

