package com.technosite.virtuaglass;

import java.util.EnumSet;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.abhi.barcode.frag.libv2.BarcodeFragment;
import com.abhi.barcode.frag.libv2.IScanResultHandler;
import com.abhi.barcode.frag.libv2.ScanResult;
import com.google.zxing.BarcodeFormat;
import com.technosite.LogManager.LogManager;
import com.technosite.media.SoundFX;

public class QRCapturerActivity extends FragmentActivity implements IScanResultHandler {

	public static boolean QRReaded = false;
        public BarcodeFragment fragment;
        private SoundFX sndOk;
        

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        QRCapturerActivity.QRReaded = false;
        sndOk = new SoundFX(this, R.raw.ok);
                setContentView(R.layout.activity_qrreader);
                LogManager.addUserTask("Starting to read the QR code");
                        fragment= (BarcodeFragment) getSupportFragmentManager().findFragmentById(R.id.qrreaderfragment);
                        fragment.setScanResultHandler(this);
        // Support for adding decoding type
                        
        fragment.setDecodeFor(EnumSet.of(BarcodeFormat.QR_CODE));
        Resources res = this.getResources();
    	String text = res.getString(R.string.focus_qr_code);
    	    	NotificationViewerActivity.show(this, text, 5000, R.drawable.qrcode);
this.closeAfterTime();    	
    }

        @Override
        public void scanResult(ScanResult result) {
        	String qrString = result.getRawResult().getText();
        	//String qrString = "pdt-1"; //result.getRawResult().getText();

        	LogManager.addUserTask("QR code is readed by the user. Value=" +qrString);
        	sndOk.play();
        	                                Intent resultData = new Intent();
                                resultData.putExtra("SCAN_RESULT", qrString);
                				setResult(0, resultData);
                				QRCapturerActivity.QRReaded = true;
                				
                				                				                				finish();
        }
        
        public void scanAgain(View v){
        	        	fragment.restart();
        }
        
        
    	@Override
    	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    		closeAfterTime();    
    	}
        
        
        
        private void closeAfterTime() {

        	Timer timer = new Timer();
        	TimerTask task = new TimerTask()
        	{
        		@Override
        		public void run()
        		{
        					runOnUiThread(new Runnable()
        {
        @Override
        	public void run()
        	{
        	if (QRCapturerActivity.QRReaded ==false) {
            	sndOk.play();
            	Intent resultData = new Intent();
                resultData.putExtra("SCAN_RESULT", "pdt-6");
                LogManager.addUserTask("QR code is readed. Value=pdt-6");
    			setResult(0, resultData);
    			QRCapturerActivity.QRReaded = true;
    			    			finish();
        	} 
        	}// end run.
        	});
        				}// end run.
        		};
        	timer.schedule(task, 15000);
        }

}
