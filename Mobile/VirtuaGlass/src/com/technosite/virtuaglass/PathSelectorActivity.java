package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technosite.Interface.GlassActivity;
import com.technosite.Interface.NavigationManager;
import com.technosite.LogManager.LogManager;
import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;
import com.technosite.virtuaglass.model.PDTManager;
import com.technosite.virtuaglass.model.PDTPath;

public class PathSelectorActivity extends GlassActivity
{

	private LinearLayout pathTable;
	private TTS tts;
	private NavigationManager  nav;
	private SoundFX sndMovement; 
    




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		stopLockScreen(true );
	setContentView(R.layout.path_selector);
	tts = TTS.getSharedInstance(this);
	sndMovement = new SoundFX(this, R.raw.tic);
	pathTable = (LinearLayout) findViewById(R.id.pathTable);
	nav = new NavigationManager ();
	nav.loopingNavegationMode  = true;
	// TextView pathSelectorInstructionsTextView = (TextView) findViewById(R.id.pathSelectorInstructionsTextView);
	// pathSelectorInstructionsTextView.requestFocus();
	// tts.speakWithDelay(pathSelectorInstructionsTextView.getText().toString(),300);
	loadPaths();
	first();
	

	}// end onCreate.

// ** System events methods.
	
	// **  Glass gestures management
	
	@Override
	protected void onGlassGestureEvent(int code) {
    	switch(code) {
    	case GlassActivity.GLASSGESTURE_TAB :
this.select();
    		break;
    	case GlassActivity.GLASSGESTURE_RIGHT :
    		sndMovement.play();
    		    		next();    		
    		break;
    	case GlassActivity.GLASSGESTURE_LEFT :
    		sndMovement.play();
    		    		previous();
    		break;
    	case GlassActivity.GLASSGESTURE_DOWN :
    		// onBackPressed();
    		stopLockScreen(false);
    		finish();
    		break;
    	case GlassActivity.GLASSGESTURE_UP :
    		
    		break;
    	case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :
    		
    		break;
    	}
    }
	
	
	// ** Paths management

	private ArrayList<ImageView> bullets = null;
	private void loadPaths() {
		PDTManager manager = PDTManager.getSharedInstance();
		boolean isTherePaths = false;
		if (manager.device.getNumberOfPaths() > 0 ) {
			if (bullets==null) bullets = new ArrayList<ImageView>();
			for (int i=0;i<manager.device.getNumberOfPaths();i++) {
				PDTPath tmpP = manager.device.getPath(i);
				if (tmpP.getNumberOfSteps()>0) {
					isTherePaths = true;
					break;
				}
			}
		 	
		}
			if (isTherePaths) {
		for (int i=0;i<manager.device.getNumberOfPaths();i++) {
		PDTPath item = manager.device.getPath(i);
		if (item.name!=null && !item.name.equalsIgnoreCase("null") && item.getNumberOfSteps()>0) {
			
			addPath(item.name,Integer.valueOf(i));
			
		}

		}
		} else {
			noPathAvailable();		
		}		

	}// end loadPaths.
	
	public void showPath(int pathNumber) {
				PDTManager manager = PDTManager.getSharedInstance();
manager.path = manager.device.getPath(pathNumber);
manager.step = manager.path.getStep(0);
    			Intent i = new Intent(this, StepViewerActivity.class);
    			// send item id to the next activity
    			startActivity(i);		
	}// end showPath.
	
	
	private void addPath(String label, int id) {

	        LinearLayout row = (LinearLayout) getLayoutInflater().inflate(R.layout.path_row, null);
	    

	    // View v = View.inflate(this, R.layout.path_row, null);
	    
	    ImageView bullet = (ImageView) getLayoutInflater().inflate(R.layout.path_bullet, null);
	    bullet.setTag((id));	    
	    bullets.add(bullet);

	    
	    Button pathSelectorButton = (Button) getLayoutInflater().inflate(R.layout.path_button, null);
	pathSelectorButton.setText(label);
	pathSelectorButton.setTag((id));
	pathSelectorButton.setFocusable(true);

	pathSelectorButton.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
Button vv = (Button) v;
				if(hasFocus)
			{
				// arrowPathImageView.setVisibility(View.VISIBLE);
					//vv.setVisibility(View.VISIBLE);
				vv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
		vv.setTypeface(Typeface.DEFAULT_BOLD);
		vv.setPadding(10, 5, 10, 5);
					}// end if hasFocus.
			else
			{
				// arrowPathImageView.setVisibility(View.GONE);
				//vv.setVisibility(View.GONE);
				vv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		vv.setTypeface(Typeface.DEFAULT);
		vv.setPadding(10,5,10,5);
							}// end else hasFocus.
			}// end onFocusChange.
			});
	
	
	
	
	pathSelectorButton.setOnClickListener(new OnClickListener()
	{
	@Override
		public void onClick(View v) {

		showPath(Integer.valueOf(v.getTag().toString()));
				
	}// end onClick.
		});

	
	nav.add(pathSelectorButton);

	row.addView(bullet);
	row.addView(pathSelectorButton);
	pathTable.addView(row);
	if (id==0) pathSelectorButton.requestFocus();
			
	}

private void noPathAvailable() {
	TextView text = new TextView(this);
	text.setText(R.string.no_paths);
	text.setTag("-1");
	text.setFocusable(true);

	pathTable.addView(text);
	text.setId(-1);
	nav.add(text);
 	}


// ** Navigation management

private void changeBulletVisibility(int id, int status) {
for (int i=0;i<bullets.size();i++) {
ImageView item = bullets.get(i);	
int value = Integer.valueOf(item.getTag().toString());
if (value==id) {
	item.setVisibility(status);
break;	
}
}
}

private void first() {
	Button t = (Button) nav.getCurrent();
t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
t.setTypeface(Typeface.DEFAULT);
int value = Integer.valueOf(t.getTag().toString());
changeBulletVisibility(value,View.GONE);
t = (Button) nav.moveFocusToFirst();
if (t!=null) {
t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
t.setTypeface(Typeface.DEFAULT_BOLD);
value = Integer.valueOf(t.getTag().toString());
changeBulletVisibility(value,View.VISIBLE);
t.requestFocus();
tts.speak(t.getText().toString());
}
}
private void next() {
Button t = (Button) nav.getCurrent();
t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
t.setTypeface(Typeface.DEFAULT);
int value = Integer.valueOf(t.getTag().toString());
changeBulletVisibility(value,View.GONE);
t = (Button) nav.moveFocusToNext();
if (t!=null) {
t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
t.setTypeface(Typeface.DEFAULT_BOLD);
value = Integer.valueOf(t.getTag().toString());
changeBulletVisibility(value,View.VISIBLE);
t.requestFocus();
tts.speakNow(t.getText().toString());
LogManager.addUserTask("User moves the focus to '" + t.getText().toString() + "' path.");
}
}

private void previous() {
	Button t = (Button) nav.getCurrent();
	t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
	t.setTypeface(Typeface.DEFAULT);
	int value = Integer.valueOf(t.getTag().toString());
	changeBulletVisibility(value,View.GONE);
	t = (Button) nav.moveFocusToPrevious();
	if (t!=null) {
				t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		t.setTypeface(Typeface.DEFAULT_BOLD);
		value = Integer.valueOf(t.getTag().toString());
		changeBulletVisibility(value,View.VISIBLE);
		t.requestFocus();
		tts.speakNow(t.getText().toString());
		LogManager.addUserTask("User moves the focus to '" + t.getText().toString() + "' path.");
	}
}

private void select() {
TextView t = (TextView) nav.getCurrent();
int value = Integer.valueOf(t.getTag().toString());

if (value>=0) {
	showPath(value);
	LogManager.addUserTask("User selects '" + t.getText().toString() + "' path.");
}
else tts.speakNow(t.getText().toString());

}
}// end class.
