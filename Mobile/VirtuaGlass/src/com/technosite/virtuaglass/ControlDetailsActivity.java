package com.technosite.virtuaglass;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;

public class ControlDetailsActivity extends Activity {

	public static boolean showingAControl = false;
	public static Bitmap imageToShow = null;
	private TextView controlDetailsTextView;
	private ImageView controlDetailsImageView;
	private TTS tts = null;

	private String text;
	private int time = 3000;
	

	private SoundFX sndDone ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showingAControl = true;
		tts = TTS.getSharedInstance(this);
		sndDone = new SoundFX(this, R.raw.close);
		setContentView(R.layout.control_details);
		controlDetailsTextView = (TextView) findViewById(R.id.controlDetailsTextView);
		controlDetailsImageView = (ImageView) findViewById(R.id.controlDetailsImageView);

		Bundle extras = getIntent().getExtras();
		if(extras != null)
		{
			text = extras.getString("text");
		}
		time = (text.length() * 60) +2000;
int timeForNotification = (time/1000) +2;		
		delegate.startAfterTime(timeForNotification);

		controlDetailsTextView.setText(text);
		if (imageToShow !=null) controlDetailsImageView.setImageBitmap(imageToShow );
		else Log.e("ControlDetails", "There is no image to show");
		tts.speakWithDelay(text, 300);

		Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						sndDone.play();
						showingAControl = false;
						tts.stop();
						finish();
					}
				});
			}
		};
		timer.schedule(task, time);
	}



	public static void show(Activity activity, String text, Bitmap image) {
		if (showingAControl) return;
		imageToShow  = image;
		ControlDetailsActivity.delegate = (DeviceExplorerActivity) activity;
		Intent intent = new Intent(activity, ControlDetailsActivity .class);
		Bundle myBundle = new Bundle();
		myBundle.putString("text", text);
		intent.putExtras(myBundle); 
		activity.startActivity(intent);		
	}

	public static DeviceExplorerActivity  delegate = null;
}
