package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technosite.Interface.GlassActivity;
import com.technosite.Interface.NavigationManager;
import com.technosite.TTS.TTS;
import com.technosite.virtuaglass.model.PDTManager;

public class DeviceSelectorActivity extends GlassActivity
{

	private LinearLayout pathTable;
	private TTS tts;
	private NavigationManager  nav;
    




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	setContentView(R.layout.bluetooth_device_selector);
	tts = TTS.getSharedInstance(this);
	pathTable = (LinearLayout) findViewById(R.id.pathTable);
	nav = new NavigationManager ();
	// TextView pathSelectorInstructionsTextView = (TextView) findViewById(R.id.pathSelectorInstructionsTextView);

	loadDevices();
	nav.moveFocusToFirst();

	}// end onCreate.

// ** System events methods.


    

	
	
	// **  Glass gestures management
	
	@Override
	protected void onGlassGestureEvent(int code) {
    	switch(code) {
    	case GlassActivity.GLASSGESTURE_TAB :
this.select();
    		break;
    	case GlassActivity.GLASSGESTURE_RIGHT :
    		previous();
    		
    		break;
    	case GlassActivity.GLASSGESTURE_LEFT :
    		next();
    		break;
    	case GlassActivity.GLASSGESTURE_DOWN :
    		// onBackPressed();
    		finish();
    		break;
    	case GlassActivity.GLASSGESTURE_UP :
    		
    		break;
    	case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :
    		
    		break;
    	}
    }
	
	
	// ** Paths management

	private ArrayList<ImageView> bullets = null;
	private void loadDevices() {
		if (bullets==null) bullets = new ArrayList<ImageView>();
	
		boolean isThereDevices = false;
		
			
		
			if (isThereDevices) {

		} else {
			noDeviceAvailable();		
		}		

	}// end loadDevices.
	
	public void showPath(int pathNumber) {
				PDTManager manager = PDTManager.getSharedInstance();
manager.path = manager.device.getPath(pathNumber);
manager.step = manager.path.getStep(0);
    			Intent i = new Intent(this, StepViewerActivity.class);
    			// send item id to the next activity
    			startActivity(i);		
	}// end showPath.
	
	/*
	private void addDevice(String label, int id) {

	        LinearLayout row = (LinearLayout) getLayoutInflater().inflate(R.layout.path_row, null);
	    

	    // View v = View.inflate(this, R.layout.path_row, null);
	    
	    ImageView bullet = (ImageView) getLayoutInflater().inflate(R.layout.path_bullet, null);
	    bullet.setTag((id));	    
	    bullets.add(bullet);

	    
	    Button pathSelectorButton = (Button) getLayoutInflater().inflate(R.layout.path_button, null);
	pathSelectorButton.setText(label);
	pathSelectorButton.setTag((id));
	pathSelectorButton.setFocusable(true);

	pathSelectorButton.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
Button vv = (Button) v;
				if(hasFocus)
			{
				// arrowPathImageView.setVisibility(View.VISIBLE);
					//vv.setVisibility(View.VISIBLE);
				vv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
		vv.setTypeface(Typeface.DEFAULT_BOLD);
		vv.setPadding(10, 5, 10, 5);
					}// end if hasFocus.
			else
			{
				// arrowPathImageView.setVisibility(View.GONE);
				//vv.setVisibility(View.GONE);
				vv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		vv.setTypeface(Typeface.DEFAULT);
		vv.setPadding(10,5,10,5);
							}// end else hasFocus.
			}// end onFocusChange.
			});
	
	
	
	
	pathSelectorButton.setOnClickListener(new OnClickListener()
	{
	@Override
		public void onClick(View v) {

		showPath(Integer.valueOf(v.getTag().toString()));
				
	}// end onClick.
		});

	
	nav.add(pathSelectorButton);

	row.addView(bullet);
	row.addView(pathSelectorButton);
	pathTable.addView(row);
	if (id==0) pathSelectorButton.requestFocus();
			
	}
*/
private void noDeviceAvailable() {
	TextView text = new TextView(this);
	text.setText(R.string.no_bluetooth_device);
	text.setTag("-1");
	text.setFocusable(true);

	pathTable.addView(text);
	text.setId(-1);
	nav.add(text);
 	}


// ** Navigation management

private void changeBulletVisibility(int id, int status) {
for (int i=0;i<bullets.size();i++) {
ImageView item = bullets.get(i);	
int value = Integer.valueOf(item.getTag().toString());
if (value==id) {
	item.setVisibility(status);
break;	
}
}
}

private void next() {
Button t = (Button) nav.getCurrent();
t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
t.setTypeface(Typeface.DEFAULT);
int value = Integer.valueOf(t.getTag().toString());
changeBulletVisibility(value,View.GONE);
t = (Button) nav.moveFocusToNext();
if (t!=null) {
t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
t.setTypeface(Typeface.DEFAULT_BOLD);
value = Integer.valueOf(t.getTag().toString());
changeBulletVisibility(value,View.VISIBLE);
t.requestFocus();
tts.speak(t.getText().toString());
}
}

private void previous() {
	Button t = (Button) nav.getCurrent();
	t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
	t.setTypeface(Typeface.DEFAULT);
	int value = Integer.valueOf(t.getTag().toString());
	changeBulletVisibility(value,View.GONE);
	t = (Button) nav.moveFocusToPrevious();
	if (t!=null) {
				t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		t.setTypeface(Typeface.DEFAULT_BOLD);
		value = Integer.valueOf(t.getTag().toString());
		changeBulletVisibility(value,View.VISIBLE);
		t.requestFocus();
		tts.speak(t.getText().toString());
	}
}

private void select() {
TextView t = (TextView) nav.getCurrent();
int value = Integer.valueOf(t.getTag().toString());

if (value>=0) showPath(value);
else tts.speak(t.getText().toString());

}
}// end class.
