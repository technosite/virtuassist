package com.technosite.virtuaglass.model;

import java.util.HashMap;

import com.technosite.virtuaglass.translator.TranslatorManager;


public class PDTTypes {

public String name = null;

	public PDTTypes(String nameTable) {
		this.name = nameTable;
		data = new HashMap<String,String>();
	}


	private HashMap<String,String> data = null;


	// ** Method to manage values and indexes

	public void add(int index, String value) {
		data.put(String.valueOf(index), value);
	}

	public int getIndexForValue(String value) {
int result = -1;
for (int i=1;i<=data.size();i++) {
if (value.equals(getValueForIndex(i))) {
result = i;
break;
}
}
return result;
	}

	public String getValueForIndex(int index) {
		String key = data.get(String.valueOf(index));
		String result = TranslatorManager.getSharedInstance(Globals.context).get(key);
		if (result==null) result = key;
		return result;
	}

}
