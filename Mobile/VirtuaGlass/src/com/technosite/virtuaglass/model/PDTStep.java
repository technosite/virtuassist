package com.technosite.virtuaglass.model;

import java.io.Serializable;

import android.util.Log;

public class PDTStep implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;

	public int id = 0;
	public int position = 0;
	public int type = 0;
	public String instruction = null;
	public PDTPath path = null;
	public int idControl = 0;
	public PDTControl control = null;
	public Object image = null;


	public PDTStep(int pId,int pos, int pType, String text,int pIdControl, Object pImage, PDTPath pPath) {
		// TODO Auto-generated constructor stub
		this.position = pos;
		this.instruction = text.replace("\\", "");
		this.type = pType;
		this.idControl = pIdControl;
		this.control = null;
		this.image = pImage;
		this.path = pPath;
	}

	public void update() {
		if (control!=null) {
if (this.image==null) {
	this.image = control.image;
	Log.w("PDTStep-update", "Step " + this.position + " has no image.It will take image from control " + this.idControl + " step description: " + this.instruction);
} else Log.w("PDTStep-update", "Step " + this.position + " has its image.");
		}

	}

}
