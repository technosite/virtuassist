package com.technosite.virtuaglass.model; 

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.technosite.virtuaglass.controls.Frame;

public class PDTSurface implements Serializable  {
	private static final long serialVersionUID = 20140127124000L;
	
	public int id = 0;
	public PDTDevice device= null;
	public String name = null;
	public int type = 0;
	public int width = 0;
	public int height = 0;
	public int tag = 0;
	public Object image = null;

	

	public PDTSurface(int pId, String pName, int pType, int pWidth, int pHeight, Object pImage, int pTag, PDTDevice pDevice) {
		// TODO Auto-generated constructor stub
		this.id = pId;
		this.name = pName.replace("\\", "");
		this.type = pType;
		this.width = pWidth;
		this.height = pHeight;
		this.image = pImage;
		this.tag = pTag;
		this.device= pDevice;
		controls = new ArrayList<PDTControl>();
		initHardnessMap();
	}

	public ArrayList<PDTControl> controls = null;
	
	
	// ** Method to manage controls
	
	public int getNumberOfControls() {
		return controls.size();
	}
	
	public void addControls(ArrayList<PDTControl> pControls) {
		if (pControls==null) return;
				for (int i=0;i<pControls.size();i++) {
					PDTControl it = pControls.get(i);
if (it!=null) this.addControl(it);			
		}
		
	}
	
	public void addControl(PDTControl pControl) {
		pControl.surface = this;
this.controls.add(pControl);
update();
	}
	
	public void removeControl(PDTControl pControl) {
		controls.remove(pControl);
	}
	
	public PDTControl getControl(int index) {
		return controls.get(index);
	}
	
	public PDTControl findControlByName(String name) {
		PDTControl result = null;
		for (int i = 0;i< controls.size();i++) {
result = controls.get(i);
if (result.name.equals(name)) break;
else result = null;
		}
		return result;
	}
	
	public PDTControl findControlById(int pId) {
		PDTControl result = null;
		for (int i = 0;i< controls.size();i++) {
result = controls.get(i);
if (result.id == pId) break;
else result = null;
		}
		return result;
	}
	
	
	// ** Method for drawing
	
	// Limit for the starting point for controls on the surface
	public int toppest;
	public int leftest;
	public int bottomest;
	public int rightest;
	
	public void calculateDimensions() {
		if (width<=0 ||height<=0) {
bottomest = 0;
rightest = 0;
			toppest = 2000;
			leftest = 2000;
			for (int i=0;i<controls.size();i++) {
		PDTControl control = controls.get(i);
		if (bottomest < (control.height+control.y)) bottomest = control.height+control.y;
		if (rightest < (control.width+control.x)) rightest = (control.width+control.x);
		if (toppest > control.y) toppest = control.y;
		if (leftest > control.x) leftest = control.x;
			}
			this.width = rightest;
			this.height = bottomest;
		}
		
		frame = new Frame(0,0,width,height);
	}
	
	
	private static Context context = null;
	
	public static Context getContext() {
		return context;
	}
	
	public void setContext(Context ct) {
		PDTSurface.context = ct;
	}

	public Frame frame = null;
	public Frame scaledFrame = null;
	
	public void draw(ImageView image, int prealWidth, int prealHeight, Context ctx, boolean paintSurface) {
		int realWidth = prealWidth;
		int realHeight = prealHeight;
		this.setContext(ctx);
		Globals globals = Globals.getSharedInstance();
		int partialWidth = 0;
		int partialHeight = 0;
				if (realWidth <=0) realWidth = (globals.screenWidth/100) * 100;
		if (realHeight <=0) realHeight = ((globals.screenHeight/100) * 100) /2 ;
		float xScale = (float) this.width / realWidth;
		float yScale = (float) this.height / realHeight;
		if (yScale>xScale) {
partialWidth = Math.round(this.width * (1/yScale));			
partialHeight = Math.round(this.height * (1/yScale));
		} else {
			partialWidth = Math.round(this.width * (1/yScale));			
			partialHeight = Math.round(this.height * (1/yScale));
		}
scaledFrame = new Frame(0,0,partialWidth,partialHeight);		
		boolean transparent = false;
		Bitmap bitmap = Bitmap.createBitmap(partialWidth, partialHeight, Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		canvas.drawColor(Color.WHITE);
		if (paintSurface ) {
if (this.image!=null) {
transparent=true;
Bitmap backgroundBitmap = Bitmap.createScaledBitmap((Bitmap) this.image, partialWidth, partialHeight, false);
canvas.drawBitmap(backgroundBitmap,0,0,null);
} else {
	Log.w("Draw: Surface drawing", "The surface has not loaded image"); 
}
		}
		
		
// code for drawing
		Frame canvasFrame = new Frame(0,0,partialWidth, partialHeight);
		if (transparent == false) {
			// No image in background
			// First, draw pannels
			for (int i=0;i<controls.size();i++) {
		PDTControl tmp = controls.get(i);
		if (tmp.type == PDTManager.CONTROLPANEL) tmp.draw(canvasFrame, canvas, context);
	}
	 // After, draw controls
	for (int i=0;i<controls.size();i++) {
		PDTControl tmp = controls.get(i);
		if (tmp.type != PDTManager.CONTROLPANEL) tmp.draw(canvasFrame, canvas, context);
	}
		} else {
			 // Draw controls with background image
			for (int i=0;i<controls.size();i++) {
				PDTControl tmp = controls.get(i);
				tmp.draw(canvasFrame, canvas, context);
			}			
		}
		
		
image.setImageBitmap(bitmap);		
	}
	
	
private ImageView backgroundImage = null;

public boolean isBackgroundImage() {
	if (backgroundImage==null) return false;
	else return true;
}

public void clearBackgroundImage() {
	if (backgroundImage!=null) {
	RelativeLayout r = (RelativeLayout) backgroundImage.getParent();
	r.removeView(backgroundImage);
	backgroundImage = null;
	}
}

	public void draw(RelativeLayout canvas, int prealWidth, int prealHeight, Context ctx, boolean paintImage) {
		int realWidth = prealWidth;
		int realHeight = prealHeight;
		this.setContext(ctx);
		Globals globals = Globals.getSharedInstance();
		int partialWidth = 0;
		int partialHeight = 0;
				if (realWidth <=0) realWidth = (globals.screenWidth/100) * 100;
		if (realHeight <=0) realHeight = ((globals.screenHeight/100) * 100) /2 ;
		float xScale = (float) this.width / realWidth;
		float yScale = (float) this.height / realHeight;
		if (yScale>xScale) {
partialWidth = Math.round(this.width * (1/yScale));			
partialHeight = Math.round(this.height * (1/yScale));
		} else {
			partialWidth = Math.round(this.width * (1/yScale));			
			partialHeight = Math.round(this.height * (1/yScale));
		}
		
		// Draw surface
		scaledFrame = new Frame(0,0,partialWidth,partialHeight);		
		boolean transparent = false;
		if (paintImage && this.image!=null) {
			transparent = true;
			clearBackgroundImage();
			Bitmap bitmap = Bitmap.createScaledBitmap((Bitmap) this.image, partialWidth, partialHeight, false);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(partialWidth, partialHeight);
			ImageView item = new ImageView(ctx);
			item.setContentDescription("");
			item.setImageBitmap(bitmap);
			item.setFocusable(false);
			item.setClickable(false);
			item.setTag(-1);
			canvas.addView(item, params);
			backgroundImage = item;
		}

// code for drawing
		// First, draw pannels
				for (int i=0;i<controls.size();i++) {
			PDTControl tmp = controls.get(i);
			if (tmp.type == PDTManager.CONTROLPANEL) tmp.draw(scaledFrame , canvas, context, transparent);
		}
		 // After, draw controls
		for (int i=0;i<controls.size();i++) {
			PDTControl tmp = controls.get(i);
			if (tmp.type != PDTManager.CONTROLPANEL) tmp.draw(scaledFrame , canvas, context,transparent);
		}
	}
	
	public void zoomDraw(RelativeLayout canvas, int prealWidth, int prealHeight, Frame area, Context ctx, boolean paintImage) {
		int realWidth = prealWidth;
		int realHeight = prealHeight;
		this.setContext(ctx);
		Globals globals = Globals.getSharedInstance();
		int partialWidth = 0;
		int partialHeight = 0;
				if (realWidth <=0) realWidth = (globals.screenWidth/100) * 100;
		if (realHeight <=0) realHeight = ((globals.screenHeight/100) * 100) /2 ;
		float xScale = (float) this.width / realWidth;
		float yScale = (float) this.height / realHeight;
		if (yScale>xScale) {
partialWidth = Math.round(this.width * (1/yScale));			
partialHeight = Math.round(this.height * (1/yScale));
		} else {
			partialWidth = Math.round(this.width * (1/yScale));			
			partialHeight = Math.round(this.height * (1/yScale));
		}
		scaledFrame = new Frame(0,0,partialWidth,partialHeight);
		// Draw surface
		boolean transparent = false;
		if (paintImage && this.image!=null) {
			transparent = true;
			clearBackgroundImage();
			Bitmap scaledBitmap = Bitmap.createScaledBitmap((Bitmap) this.image, partialWidth, partialHeight, false);
			Bitmap fullBitmap = Bitmap.createBitmap(realWidth, realHeight, Config.RGB_565);
			Canvas tmpCanvas = new Canvas(fullBitmap);
			tmpCanvas.drawColor(Color.WHITE);
			tmpCanvas.drawBitmap(scaledBitmap,0,0,null);
						Bitmap areaBitmap = Bitmap.createBitmap(fullBitmap, area.x, area.y,area.width,area.height, null, false);
						Bitmap bitmap = Bitmap.createScaledBitmap(areaBitmap, partialWidth, partialHeight, false);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(partialWidth, partialHeight);
			ImageView item = new ImageView(ctx);
			item.setContentDescription("");
			item.setImageBitmap(bitmap);
			item.setFocusable(false);
			item.setClickable(false);
			item.setTag(-1);
			canvas.addView(item, params);
			backgroundImage = item;
		}

// code for drawing
		
		// First, draw pannels
				for (int i=0;i<controls.size();i++) {
			PDTControl tmp = controls.get(i);
			if (tmp.type == PDTManager.CONTROLPANEL) tmp.zoomDraw(scaledFrame , canvas, context, transparent,area);
		}
		 // After, draw controls
		for (int i=0;i<controls.size();i++) {
			PDTControl tmp = controls.get(i);
			if (tmp.type != PDTManager.CONTROLPANEL) tmp.zoomDraw(scaledFrame , canvas, context,transparent,area);
		}
	}
	
	// ** Method for manage the surface
	
	public void update() {
Log.w("PDTSurface", "Update the surface " + id);
				calculateDimensions();
				calculateOffSets();
				manageImageSurface();
		resizeImageSurface();
		
		// update controls and relationships
		
		for (int i=0;i<controls.size();i++) {
			PDTControl control = controls.get(i);
			for (int j=i;j<controls.size();j++) {
PDTControl otherControl = controls.get(j);
if (j!=i) {
if (control.isContainer(otherControl.x,otherControl.y,otherControl.width,otherControl.height)) {
otherControl.container = control;
break;
} else if (otherControl.isContainer(control.x,control.y,control.width,control.height)) {
control.container = otherControl;
break;
}
}
			}
			control.update();
			control.updatePosition(leftOffset, topOffset);
			addRegionToHardnessMap(control.x, control.y, control.width, control.height, control.id);
		}
		
		compareControlColors();
	}
	
	public PDTControl getControlOnTop(PDTControl reference) {
		PDTControl result = null;
		for (int i=0;i<controls.size();i++) {
		PDTControl tmp = controls.get(i);
if (reference.id != tmp.id) {
if (reference.y> tmp.y) 
	if (reference.isVerticalCollided(tmp)) result = tmp;	
}
		}
		return result;
	}
	
	public PDTControl getControlOnBottom(PDTControl reference) {
		PDTControl result = null;
		for (int i=0;i<controls.size();i++) {
		PDTControl tmp = controls.get(i);
		if (!reference.isHorizontalCollided(tmp) && reference.isVerticalCollided(tmp)) {
			if (result == null) result = tmp;
			else if (tmp.y < result.y) result = tmp; 
		}
		}
		return result;
	}
	
	public PDTControl getControlOnLeft(PDTControl reference) {
		PDTControl result = null;
		for (int i=0;i<controls.size();i++) {
		PDTControl tmp = controls.get(i);
		if (reference.isHorizontalCollided(tmp) && !reference.isVerticalCollided(tmp)) {
			if (result == null) result = tmp;
			else if ((tmp.x+tmp.width) > (result.x + result.width)) result = tmp; 
		}
		}
		return result;
	}
	
	public PDTControl getControlOnRight(PDTControl reference) {
PDTControl result = null;
for (int i=0;i<controls.size();i++) {
PDTControl tmp = controls.get(i);
if (reference.isHorizontalCollided(tmp) && !reference.isVerticalCollided(tmp)) {
	if (result == null) result = tmp;
	else if (tmp.x < result.x) result = tmp; 
}
}
return result;
	}
	
	// ** counters
	
	public int getNumberOfControlsOnTop(PDTControl reference) {
		int result = 0;
		for (int i = 0;i<controls.size();i++) {
PDTControl tmp = controls.get(i);			
if (reference.id != tmp.id ) {
	if (reference.y > tmp.y) 
		if (reference.isVerticalCollided(tmp)) result = result +1;
}
		}
		return result;
	}
	
	public int getNumberOfControlsOnBottom(PDTControl reference) {
		int result = 0;
		for (int i = 0;i<controls.size();i++) {
PDTControl tmp = controls.get(i);			
if (reference.id != tmp.id ) {
	if ((reference.y+reference.height) < (tmp.y+tmp.height)) 
		if (reference.isVerticalCollided(tmp)) result = result +1;
}
		}
		return result;
	}
	
	public int getNumberOfControlsOnLeft(PDTControl reference) {
		int result = 0;
		for (int i = 0;i<controls.size();i++) {
PDTControl tmp = controls.get(i);			
if (reference.id != tmp.id ) {
	if (reference.x > tmp.x) 
		if (reference.isHorizontalCollided(tmp)) result = result +1;
}
		}
		return result;
	}
	
	public int getNumberOfControlsOnRight(PDTControl reference) {
		int result = 0;
		for (int i = 0;i<controls.size();i++) {
PDTControl tmp = controls.get(i);			
if (reference.id != tmp.id ) {
	if ((reference.x+reference.width) < (tmp.x+tmp.width)) 
		if (reference.isHorizontalCollided(tmp)) result = result +1;
}
		}
		return result;
	}
	
	// ** Blinking management

	public void changeBlinkingForAll(boolean value) {
		for (int i=0;i<controls.size();i++) {
			PDTControl item = controls.get(i);
			item.setBlinking(value);
		}
	}
	
	public void changeBlinkingOfControlById(int id, boolean value) {
		PDTControl item = findControlById(id);
		item.setBlinking(value);
	}
	
	// ** HardnessMap management
	
	public Bitmap hardnessMap = null;
	
	private void initHardnessMap() {
		this.hardnessMap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888);
		this.addRegionToHardnessMap(0, 0, width, height, 0);
	}
	
	public void addRegionToHardnessMap(int x, int y, int width, int height, int color) {
		if (this.hardnessMap==null) {
			Log.e("Geography of surface " + this.id, "The hardness map is not created in surface " + this.id);
			return;
		}
if (color>255) {
	Log.w("Surface geometry error", "The color value is to high");
	} else {
	Canvas canvas = new Canvas((Bitmap) this.hardnessMap);
	Paint paint = new Paint();
	paint.setColor(Color.rgb(0,0,color));
	canvas.drawRect(x, y, width, height, paint);
}
	}
	
	public int getControlIdAtPosition(int x, int y) {
		if (hardnessMap==null) {
			Log.e("Geography of surface " + this.id, "The hardness map is not created");
			return -2;
		}
		Bitmap bmp = (Bitmap) hardnessMap;
		if (x>bmp.getWidth() || y > bmp.getHeight()) {
			Log.e("PDTSurface-getControlIdAtPosition", "The point ("+x+","+y+") is out of hardness map with size " +bmp.getWidth() + "X" + bmp.getHeight());
			return -2;			
		}
		int result = bmp.getPixel(x, y);
 		return Color.blue(result);
	}
	
	// ** Colors in controls management
	
	
	
	public boolean evaluateControlsByColors(PDTControl ctrl1, PDTControl ctrl2) {
		boolean result = false;
		int limit = 3;
		int finalTone = Math.abs(ctrl1.tone - ctrl2.tone);
		if (finalTone <= limit) result = true;
		return result;
	}
	
	private void compareControlColors() {
		if (this.image==null) return;
		Log.w("PDTSurface", "compareControlColors");
for (int c=0;c<controls.size();c++) {
	PDTControl item = controls.get(c);
	if (item.tone>=0) for (int p=0;p<controls.size();p++) {
if (c!=p) {
	PDTControl comp = controls.get(p);
	if (evaluateControlsByColors(item,comp)) item.similarControls = item.similarControls+1;
}
	}
}
	}
	


	
	// ** Image surface management
	
	public boolean manageImageSurface() {

		if (this.image==null) {
Log.w("Manage image surface", "Image field is null for surface " + this.id);			
return false;
		}
		if (this.image instanceof Bitmap) {
			Log.w("Manage image surface", "Image stored in the surface " +this.id);
					}
		if (this.image instanceof String) {
Log.w("Manage image surface", "No parsed image for surface " + this.id + " the string data will be stored in SDCard to parse it");
saveFileToSDCard("VASwap.jpg", (String) this.image);
this.image = loadImageFromSDCard("VASwap.jpg");
if (this.image == null) {
	Log.e("Manage image surface error", "String data could not be parsed to a JPG compatible data for surface " + this.id);
	return false;
} else {
	Log.w("Manage image surface", "String data was succesfuly parsed for surface " + this.id);
	}
		}

		return false;
	}
	
	private void resizeImageSurface() {
if (this.image==null) return;
Bitmap bmp = (Bitmap) this.image;
if (bmp.getWidth()>this.width || bmp.getHeight()>this.height) {
	Log.w("PDTImage", "resizeImageSurface");
	this.image = Bitmap.createScaledBitmap(bmp, bmp.getWidth() /2, bmp.getHeight() / 2, false);
	if (this.image==null) Log.e("PDTImage-resizeImageSurface", "Error resizing the image. The image is null now");
}
	}
	
	private void saveFileToSDCard(String fileName, String data) {
		String filename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName;
		byte[] arrayResult = null;
		try {
			arrayResult = data.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			Log.e("UnsupportedEncodingException loading a image from cloud", "Error decoding buffer: " +e);
		}
		byte[] b = null;
		try {
			b= Base64.decode(arrayResult, arrayResult.length);
		}
					catch (Exception e) {
							e.printStackTrace();
		}
		
		// Save the file
		File photo=new File(filename);
		if (photo.exists()) photo.delete();
		try {
			FileOutputStream fos=new FileOutputStream(photo.getPath());					
			fos.write(b);
			fos.close();
		} catch (java.io.IOException e) {
Log.e("Saving image to SDCard", "Error saving the image to SDCard memory. " +e);					
		}
	}

	private Bitmap loadImageFromSDCard(String fileName) {
		String filename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName;
		Bitmap bitmap = BitmapFactory.decodeFile(filename);	
				

		return bitmap;
	}
	
	


		
	// ** get description
	
	public String toString() {
		String result ="Surface " + this.name + " Type:" + this.type + "\n";
		result = result + "Controls:\n";
		for (int i=0;i<controls.size();i++) {
PDTControl item = controls.get(i);
result = result + item.name + " (" + item.tone + ") " + item.similarControls + "\n";
		}
		
		return result;
	}
	
	public int leftOffset = 0;
	public int topOffset = 0;
	public int rightOffset = 0;
	public int bottomOffset = 0;
	
	public void calculateOffSets() {
int l = this.width;
int t = this.height;
int r = 0;
int b = 0;
for (int i=0;i<controls.size();i++) {
PDTControl item = controls.get(i);
if (item.x<l) l= item.x;
if (item.y<t) t = item.y;
int tmpR = item.x+item.width;
int tmpB = item.y + item.height;
if (r<tmpR) r = tmpR;
if (b<tmpB) b = tmpB;

}
leftOffset = l;
topOffset = t;
rightOffset = r;
bottomOffset = b;
	}
	
	}
