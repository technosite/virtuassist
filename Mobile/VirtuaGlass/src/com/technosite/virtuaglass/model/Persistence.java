package com.technosite.virtuaglass.model;

public class Persistence {
	
	public static final String kUser = "user";
	public static final String kPairedDevice= "BTDeviceID";
	public static final String kPairedDeviceAddressByDefault= "CC:FA:00:6F:FF:79";
	/*
	 * Address devices
	 Nexus 5: CC:FA:00:6F:FF:79
Galaxy nexus: 9C:02:98:F8:69:85
GoogleGlass: F8:8F:CA:11:18:DD
	 */
	 


	// ** Preferences
	public static final String kTextPreference = "prefText";
	public static final String kImagePreference = "prefImage";
	public static final String kVideoPreference = "prefVideo";
	public static final String kSpeechPreference = "prefSpeech";
	
	
	public static final String kInfoSteps = "infoAboutSteps";
	public static final String kInfoSurfaces = "infoAboutSurfaces";
	
	public static final String kLastDevice = "lastDevice";
	public static final String kDataOfStoredDevice = "binaryArrayOfDevice";

}
