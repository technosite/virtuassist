package com.technosite.virtuaglass.model;

import java.util.List; 

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import android.util.Log;

public class GeometryEngine {


	public PDTSurface surface = null;
	public PDTDevice device = null;
	private int waitForRecognition = 0;
	private int maxForRecognition = 3;
	private Point lastLocation;

	public GeometryEngine(PDTDevice parentDevice, PDTSurface surface) {
		device = parentDevice;
		setSurface(surface);
	}

	public void setSurface(PDTSurface surfaceValue) {
		if (surfaceValue==null) {
			Log.e("Geometry-setSurface", "Surface is null");
			return;
		}
		if (surfaceValue.image==null) {
			Log.e("Geometry-setSurface", "Image for surface is null");
			return;
		}
		surface = surfaceValue;
		Log.w("Geometry-setSurface", "Geometry created by surface " + surface.name);
		calculateArea();
	}

	// ** Area and surface management


	private int mainSurfaceWidth = -1;
	private int mainSurfaceHeight = -1;
	

	private void calculateArea() {
		if (surface==null) return;
		offsetSurfaceWidth = surface.rightOffset - surface.leftOffset ;
		offsetSurfaceHeight = surface.bottomOffset - surface.topOffset;
	}

	// ** Scale and proportion

	private int scaledWidth = -1;
	private int scaledHeight = -1;
	private int offsetSurfaceWidth = 0;
	private int offsetSurfaceHeight = 0;

	public void setScaledMainSurface(int pWidth, int pHeight) {
		Log.w("Geometry-setScaledMainSurface", "Size " + pWidth + "x" + pHeight);
		scaledWidth = pWidth;
		scaledHeight = pHeight;
		Log.w("Geometry-setScaledMainSurface", "Size " + pWidth + "x" + pHeight + " as scaledSize " + getScaledWidth(pWidth) + "x" + getScaledHeight(pHeight));
	}

	public int getScaledWidth(int xPos) {
		return (xPos*offsetSurfaceWidth) / scaledWidth;
	}

	public int getScaledHeight(int yPos) {
		return (yPos*offsetSurfaceHeight) /  scaledHeight;
	}

	public int getScaledMainWidth(int xPos) {
		return (xPos*scaledWidth) / mainSurfaceWidth;
	}

	public int getScaledMainHeight(int yPos) {
		return (yPos*scaledHeight) / mainSurfaceHeight;
	}


	// ** Identification management
/*
	private double getDistance(int x1, int y1, int x2, int y2) {
		double result = -1;
		double lx = Math.abs(x1-x2);
		double ly = Math.abs(y1-y2);
		result = Math.sqrt(lx+ly);
		return result;
	}
*/


	
	private PDTControl findControlByAproximation(int x, int y) {
		Log.w("Geometry-findControlByAproximation", "Init method with data x="+x +" y="+ y);
		PDTControl result = null;
		if (surface==null) {
			Log.e("Geometry-findControlByAproximation", "The surface for this geometry engine is null");
			return result;			
		}
		int distance = 200;
		
		if (surface.controls.size() == 0) {
			Log.e("Geometry-findControlByAproximation", "The surface for this geometry engine has no controls");
			return result;			
		}
		for (int i=0;i<surface.controls.size();i++) {
			PDTControl control = surface.controls.get(i);
			int tmpDistance = distance*20;
			if (control!=null) {
				Log.w("Geometry-findControlByAproximation", "Control " + control.name + " at positionX=" + control.positionX +" and finger is x=" + x);
				tmpDistance = Math.abs(control.positionX - x);
			}
			else Log.e("Geometry-FindControlByAproximation", "Control from surface array is null");
			
				if (tmpDistance<distance){
					result = control;
					distance = tmpDistance;
				}
			
		}
		if (result!=null) Log.w("Geometry-FindControlByAproximation", "Control " + result.name + " distance = " + distance + " position (" +result.positionX + "," + result.positionY +")");
		return result;
	}

	public PDTControl identifyControlAtPoint(Point point, Mat visualData, List<Rect> controls) {
		Log.w("Geometry-identifyControlAtPoint","Init function managing the point for surface " + surface.name + " and a Mat for vision information");
		int x = (int) point.x;
		int y = (int) point.y;

		Log.w("Geometry-identifyControlAtPoint","Getting Mat for colors");
		Mat colors = visualData;

		Log.w("Geometry-identifyControlAtPoint","Finding the limits");
		Rect leftRect= controls.get(0);
		Rect rightRect= controls.get(controls.size()-1);
		int widthMainSurface = (rightRect.x +rightRect.width) - leftRect.x;

		setScaledMainSurface(widthMainSurface , colors.rows());
		int finalX = getScaledMainWidth(x);
		int finalY = getScaledMainHeight(y);
		Log.w("Geometry-identifyControlAtPoint","Searching in the hardness map at point (" + finalX + "," + finalY + ")");
		int resultInt = surface.getControlIdAtPosition( finalX, finalY);
		PDTControl result = null;
		if (resultInt>0) {
			Log.w("Geometry-identifyControlAtPoint","Searching the PDTControl with id="+ resultInt);
			// result = surface.findControlById(resultInt);
			result = findControlByAproximation( finalX, finalY);
		} else {
			Log.w("Geometry-identifyControlAtPoint","Searching the PDTControl by aproximation in point (" + finalX + "," + finalY +")");
			result = findControlByAproximation( finalX, finalY);			
		}
		if (result != null) Log.w("Geometry-identifyControlAtPoint","PDTControl found: " + result.name + " in (" + result.x +"," +result.y +")");
		else Log.w("Geometry-identifyControlAtPoint","PDTControl not found");

		return result ;
	}

	
	//Method to scale the device
	public PDTControl identifyControlAtPoint(Point point, Point left, Point right) {

		if (surface==null) {
			Log.w("Geometry-identifyControlAtPoint","Init function managing the point for surface null and two points for limits. Data: Finger point=(" + point.x+","+point.y+") left=("+left.x+","+left.y+") right=("+right.x+","+right.y+")");
			Log.e("Geometry-identifyControlAtPoint","The geometry engine has no linked surface");
			return null;
		} else Log.w("Geometry-identifyControlAtPoint","Init function managing the point for surface " + surface.name + " and two points for limits. Data: Finger point=(" + point.x+","+point.y+") left=("+left.x+","+left.y+") right=("+right.x+","+right.y+") with surface area (" + surface.width +","+ surface.height +")");
		if (checkIsStableFinger(point)==false) {
			Log.w("Geometry-identifyControlAtPoint","The finger is not stopped");
			return null;			
		}

		int x = (int) (point.x - left.x);
		int y = (int) point.y;

		double dAreaW = Math.abs(left.x - right.x);
		int areaW = (int) dAreaW;
		int areaH = 384;
		PDTControl result = null;
		setScaledMainSurface(areaW, areaH);
		int finalX = getScaledWidth(x);
		int finalY = getScaledHeight(y);
		Log.w("Finger point scaled", "Original="+ x+","+y+"  scaled=" +finalX+"x"+finalY);
		

		Log.w("Geometry-identifyControlAtPoint","Searching in the hardness map the control at (" +finalX + "," + finalY +") converted from point (" + x + "," +y +")");
		int resultInt = -1;
		// resultInt = surface.getControlIdAtPosition( finalX, finalY);

		if (resultInt>0) {
			Log.w("Geometry-identifyControlAtPoint","Searching the PDTControl");
			result = surface.findControlById(resultInt);			
		} else {
			Log.w("Geometry-identifyControlAtPoint","Searching the PDTControl by aproximation");
			result = findControlByAproximation( finalX, finalY);			
		}
		if (result != null) Log.w("Geometry-identifyControlAtPoint","Control found!");
		else Log.w("Geometry-identifyControlAtPoint","PDTControl not found");

		return result;
	}

	private boolean checkIsStableFinger(Point current) {
		int maxDistance = 20;
		if (lastLocation ==null) {
			lastLocation  = current;
			return false;
		}
		int distance = (int) Math.abs(current.x - lastLocation.x);
		Log.w("checkIsStableFinger", "last=" + lastLocation.x +" current=" +current.x +" distance=" +distance);
		lastLocation = current;
		if (distance<=maxDistance) {
			lastLocation = current;
			waitForRecognition ++;
			if (waitForRecognition >= maxForRecognition) {
				waitForRecognition =0;	
				return true;
			}
		}

		return false;
	}

}
