package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

import com.technosite.Interface.GlassActivity;
import com.technosite.virtuaglass.opencv.CameraView;
import com.technosite.virtuaglass.opencv.ControlDetection;
import com.technosite.virtuaglass.opencv.HandGesture;
import com.technosite.virtuaglass.opencv.PointsManager;

public class DeviceExplorerActivity extends GlassActivity implements CvCameraViewListener2 {


    private static final String    TAG = "*TEST*OCVSample::Activity";

    private static final int       VIEW_MODE_RGBA     = 0;
    private static final int       VIEW_MODE_GRAY     = 1;
    private static final int       VIEW_MODE_CANNY    = 2;
    private static final int       VIEW_MODE_FEATURES = 5;

    //private static final int       FEATURES_STATE_CALIBRATE_BG   = 0;
    private static final int       FEATURES_STATE_CALIBRATE_HAND = 1;
    private static final int       FEATURES_STATE_TEST           = 2;

    private int                    mFeaturesState = FEATURES_STATE_CALIBRATE_HAND;//FEATURES_STATE_CALIBRATE_BG;

    //private int countedFrames = 0;
    //private final int framesBetweenFrames = 5;
    private int                    mViewMode;
    private Mat                    mRgba;
    private Mat lastmRgba;
    private Mat                    mIntermediateMat;
    private Mat                    mGray;
    private Mat                    mRgb;
    private Mat                    mOut;
    private Mat                    mOut2;

    private Mat                    binMat;

    private Mat                    sampleHandMats[][];
    //private Mat                    sampleBackgroundMats[][];

	private Scalar                 lowerBound;
	private Scalar                 upperBound;

    private MenuItem               mItemPreviewRGBA;
    private MenuItem               mItemPreviewGray;
    private MenuItem               mItemPreviewCanny;
    private MenuItem               mItemPreviewFeatures;

//    private CameraBridgeViewBase   mOpenCvCameraView;
    private CameraView   mOpenCvCameraView;
   // private Gyroscope myGyroscope = null;

	private HandGesture hg = null;

	// Parte de la inicializaci�n de OpenCV
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("mixed_sample");

                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                	Log.w(TAG, "OpenCV not loaded");
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public DeviceExplorerActivity()
    {
          }// end constructor.

    @Override
	protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		stopLockScreen(true );
		Log.w("**Test OpenCV**","Preparing OpenCV camera");

		//Sensor settings
		//myGyroscope = Gyroscope.getSharedInstance(this, 0.02f);//(1.0f*Math.PI/180));
		
		// ** Camera settings
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.device_explorer);
		startAfterTime(17);
		// Show splashScreen notification
		Resources res = this.getResources();
		String text = res.getString(R.string.focus_hand);
		long time = 5000;
		NotificationViewerActivity.show(this, text, time, R.drawable.pointshand);
    	mFeaturesState = FEATURES_STATE_CALIBRATE_HAND;
		
		mOpenCvCameraView = (CameraView) findViewById(R.id.handdetection_activity_java_surface_view);
		mOpenCvCameraView.setMaxFrameSize(320,240);//512,384);//320,240);
				 
		mOpenCvCameraView.setCvCameraViewListener(this);
		Log.w("**Test OpenCV**","Camera configured");

		mOpenCvCameraView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
			   	intent.setClassName(DeviceExplorerActivity.this, ControlDetailsActivity.class.getName());
			   	DeviceExplorerActivity.this.startActivity(intent);
			}

		});
		
    
    }// end onCreate.
   
    @Override
    public void onPause()
    {
        super.onPause();
        //if (myGyroscope != null)
        	//myGyroscope.stop();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
            }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
            }

    public void onDestroy() {
        super.onDestroy();
                //if (myGyroscope != null)
        	//myGyroscope.stop();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

 // **CvCameraViewListener2 interface methods (OpenCV)
   
    public void onCameraViewStarted(int width, int height) {

    	mRgba = new Mat(height, width, CvType.CV_8UC4);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
        mRgb  = new Mat();
        mOut  = new Mat();
        mOut2 = new Mat();
        
        binMat = new Mat();
        
        PointsManager pm = PointsManager.getSingleton();
        sampleHandMats = new Mat[pm.HAND_POINTS][2];
        for (int i = 0; i < pm.HAND_POINTS;i++){
        	sampleHandMats[i][0] = new Mat();
        	sampleHandMats[i][1] = new Mat();
        }
		lowerBound = new Scalar(0,0,0,0);
		upperBound = new Scalar(0,0,0,0);
		
		if (hg == null)
		hg = new HandGesture();
		hg.delegate = this;
    
    }// end onCameraViewStarted.

    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
        mIntermediateMat.release();
    }// end onCameraViewStopped.
    
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {    	
    	mRgba = inputFrame.rgba();
    	mGray = inputFrame.gray();

    	Log.i(TAG, "called onCameraFrame; before call processMaterial");
    	return processMaterial();    	
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);

        if (item == mItemPreviewRGBA) {
            mViewMode = VIEW_MODE_RGBA;
        } else if (item == mItemPreviewGray) {
            mViewMode = VIEW_MODE_GRAY;
        } else if (item == mItemPreviewCanny) {
            mViewMode = VIEW_MODE_CANNY;
        } else if (item == mItemPreviewFeatures) {
            mViewMode = VIEW_MODE_FEATURES;
        }

        return true;
    }
    
    private void handsAddition (Mat imgIn, Mat mH, Mat mS, Mat imgOut)
    {
    	PointsManager pm = PointsManager.getSingleton();    	
    	
		for (int i = 0; i < pm.HAND_POINTS; i++)
		{			
			Core.inRange(mH, new Scalar(pm.avgColorHS[i][0]-pm.cLowerHS[i][0]), new Scalar(pm.avgColorHS[i][0]+pm.cUpperHS[i][0]), sampleHandMats[i][0]);
			Core.inRange(mS, new Scalar(pm.avgColorHS[i][1]-pm.cLowerHS[i][1]), new Scalar(pm.avgColorHS[i][1]+pm.cUpperHS[i][1]), sampleHandMats[i][1]);
		}
		
		Mat img = new Mat();
		sampleHandMats[0][0].copyTo(img);
		for (int i = 1; i < pm.HAND_POINTS; i++)
			Core.add(img, sampleHandMats[i][0], img);
		
		
		imgOut.release();
		sampleHandMats[0][1].copyTo(imgOut);
		for (int i = 1; i < pm.HAND_POINTS; i++)
			Core.add(imgOut, sampleHandMats[i][1], imgOut);
		
		Imgproc.dilate(imgOut, imgOut, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size( 3, 3)));
		Imgproc.dilate(img, img, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size( 3, 3)));
		
		Core.bitwise_and(img, imgOut, imgOut);
		
		Imgproc.medianBlur(imgOut, imgOut, 3);  
    }

        
    private Mat processMaterial()
    {
        //Bluring the image
    	Mat noBlur = new Mat();
    	mRgba.copyTo(noBlur);
    	Imgproc.blur(mRgba, mRgba, new Size(3,3));//GaussianBlur(mRgba, mRgba, new Size(5,5), 5, 5);

		Mat mRgbaClean = new Mat();
		mRgba.copyTo(mRgbaClean);
		
		Mat mH = new Mat();
		Mat mS = new Mat();
		Imgproc.cvtColor(mRgba, mH, Imgproc.COLOR_RGB2HSV, 0);
		
		/////////////////////////
		//QRFinder qr = new QRFinder();
		//(qr.extractQR(mH)).copyTo(mOut);
		////////////////////////
		
		
		//split img:
    	List<Mat> channels = new ArrayList<Mat>(3);
    	Core.split(mH, channels);
    	// get the channels (dont forget they follow BGR order in OpenCV)
    	mH = new Mat();
    	mH = channels.get(0);
    	mS = channels.get(1);
    	
        PointsManager pm = PointsManager.getSingleton();
        
        pm.initialize(mH);    
        
        
		if (mFeaturesState == FEATURES_STATE_TEST)
		{
			handsAddition(mRgb, mH, mS, mOut);

    		binMat.release();
    		mOut.copyTo(binMat);
	    	int handSize= makeContours();
	    		
	    	
	    	//double z = myGyroscope.z;

	    	if(hg.handleFingerTips(mRgba, mRgbaClean, noBlur, mH, mS, handSize)){//, z))
	    		Log.w("DeviceExplorer", "Show the information of the control");

	    		ControlDetailsActivity.show(this, hg.controlText, hg.controlImg);
	    		}
            
	        Imgproc.cvtColor(mOut, mOut, Imgproc.COLOR_GRAY2RGBA, 0);	        
			return mRgba;
		}
		else
		{		
			mOut.release();
			mRgba.copyTo(mOut);
		}
		
		if (mFeaturesState == FEATURES_STATE_CALIBRATE_HAND){
	        pm.drawHandSamplePoints(mOut);
		}
        return mOut;       
        
    }
    
    int makeContours()
	{
    	int handSize = -1;
    	// Clean previously calculated contours
		hg.contours.clear();
		// Calculating new contours considering the binary image
		Imgproc.findContours(binMat, hg.contours, hg.hie, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
		
		// Calculate bigger contour
		hg.findBiggestContour();

		if (hg.cMaxId > -1) {
			// Calculate the rectangle that contains the contour
			hg.boundingRect = Imgproc.boundingRect(hg.contours.get(hg.cMaxId));	
			
			if ((hg.boundingRect != null) && (hg.boundingRect.height > 0) && (hg.boundingRect.width > 0)) {
				// Simplification of the contour
				handSize = hg.contours.get(hg.cMaxId).toList().size();
				hg.approxContour.fromList(hg.contours.get(hg.cMaxId).toList());
				Imgproc.approxPolyDP(hg.approxContour, hg.approxContour, 2, true);
				hg.contours.get(hg.cMaxId).fromList(hg.approxContour.toList());
			
				// Paint contour
				Imgproc.drawContours(mRgba, hg.contours, hg.cMaxId, new Scalar(255,0,0,0), 2);
			
				//Calculate the circle that represents the hand's palm
				hg.findInscribedCircle(mRgba);
			
				// Calculate the convex polygon nearest to the contour
				Imgproc.convexHull(hg.contours.get(hg.cMaxId), hg.hullI, false);
			
				hg.hullP.clear();
				for (int i = 0; i < hg.contours.size(); i++)
					hg.hullP.add(new MatOfPoint());
			
				int[] cId = hg.hullI.toArray();
				List<Point> lp = new ArrayList<Point>();
				Point[] contourPts = hg.contours.get(hg.cMaxId).toArray();
			
				hg.mFingerTips.clear();
				for (int i = 0; i < cId.length; i++)
				{
					lp.add(contourPts[cId[i]]);
					hg.mFingerTips.add(contourPts[cId[i]]);
				}
    		}
		}
		return handSize;
	}

	boolean isClosedToBoundary(Point pt, Mat img)
	{
		int margin = 5;
		if ((pt.x > margin) && (pt.y > margin) && 
				(pt.x < img.cols()-margin) &&
				(pt.y < img.rows()-margin)) {
			return false;
		}
		
		return true;
	}	
    
    public native void FindFeatures(long matAddrGr, long matAddrRgba);

 // ** System events methods.

    private void calibrate() {
    	/// Reduce noise with a kernel 3x3
    	//Imgproc.blur(mRgba, mRgba, new Size(3,3));//Imgproc.GaussianBlur(mRgba, mRgba, new Size(5,5), 5, 5);
      	Mat mH = new Mat();
      	Mat mS = new Mat();
		Imgproc.cvtColor(mRgba, mH, Imgproc.COLOR_RGB2HSV, 0);
		// split img:
    	List<Mat> channels = new ArrayList<Mat>(3);
    	Core.split(mH, channels);
    	// get the channels (dont forget they follow BGR order in OpenCV)
    	mH = new Mat();
    	mH = channels.get(0);
    	mS = channels.get(1);
		
    	PointsManager pm = PointsManager.getSingleton();
        pm.initialize(mRgba);
        Resources res = this.getResources();
        long time = 5000;
        
                    	            
        if (mFeaturesState == FEATURES_STATE_CALIBRATE_HAND)
        {
        	ControlDetection.reset();        	
        	pm.preSampleHand  (mH, mS);//pm.preSampleHand        (mRgba);
        	pm.boundariesCorrection();
        	mFeaturesState = FEATURES_STATE_TEST;
        	String text = res.getString(R.string.explore_instructions);
        	NotificationViewerActivity.show(this, text, time, R.drawable.instructionexploration);    		                				       		                			       
        }// end else.
    }

	// Detect trackpad events from GoogleGlass

	@Override
	protected void onGlassGestureEvent(int code) {
    	switch(code) {
    	case GlassActivity.GLASSGESTURE_TAB :
    		calibrate();
    		break;
    	case GlassActivity.GLASSGESTURE_RIGHT :    		
    		break;
    	case GlassActivity.GLASSGESTURE_LEFT :
    		break;
    	case GlassActivity.GLASSGESTURE_DOWN :
    		DeviceExplorerActivity .CVOperationAvailable = false;
    		onBackPressed();
    		break;
    	case GlassActivity.GLASSGESTURE_UP :    		
    		break;
    	case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :    		
    		break;
    	}
	}
    

	// ** Time management
	
	public static boolean CVOperationAvailable = false;
    public void startAfterTime(int seconds) {
    	Timer timer = new Timer();
    	TimerTask task = new TimerTask()
    	{
    		@Override
    		public void run()
    		{
    					runOnUiThread(new Runnable()
    {
    @Override
    	public void run()
    	{
    	DeviceExplorerActivity .CVOperationAvailable = true;    

    	}// end run.
    	});
    				}// end run.
    		};
    		DeviceExplorerActivity .CVOperationAvailable = false;
    	timer.schedule(task, seconds* 1000);
    }
    
    }// end class.
