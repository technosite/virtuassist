package com.technosite.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundFX {

	private SoundPool snd = null;
			private int sResource = 0;
			
	public SoundFX(Context context, int soundResource) {
		snd = new SoundPool(8, AudioManager.STREAM_MUSIC , 0); 
		sResource = snd.load(context,soundResource,1); 
	}
	
	public void play() {
		try {
			snd.play(sResource, 1, 1, 0, 0, 1);			
		} catch(Exception e) { }
	}
	
	

}
