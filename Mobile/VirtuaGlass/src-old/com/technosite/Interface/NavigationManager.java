package com.technosite.Interface;

import java.util.ArrayList;

import android.view.View;

public class NavigationManager {

	private ArrayList<View> items = null;
	private int position = 1;
	
	public boolean alwaysReturnControl= true;
	public boolean loopingNavegationMode = true;
	
	public NavigationManager() {
		// TODO Auto-generated constructor stub
		position = 0;
		items = new ArrayList<View>();
	}
	
	public void add(View view) {
items.add(view);		
	}
	
	public int getPosition() {
		return position;
	}
	
	// ** Focus management

	public View getCurrent() {
		if (items.size()==0) return null;
		else return items.get(position);
	}
	
	public View moveFocusToPrevious() {
		View result = null;
		if (items.size()==0) return result;
position--;
if (position<0) {
	if (loopingNavegationMode) position = items.size()-1;
	else position++;
	if (alwaysReturnControl) result = items.get(position);
	} else {
result = items.get(position);
result.requestFocus();
	}
return result;
	}
	
	public View moveFocusToNext() {
		View result = null;
		if (items.size()==0) return result;
position++;
if (position>=items.size()) {
	if (loopingNavegationMode = true) position = 0;
	else position--;
	if (alwaysReturnControl) result = items.get(position);
	} else {
result = items.get(position);
result.requestFocus();
	}
return result;
	}
	
	public View moveFocusToFirst() {
		View result = null;
		if (items.size()==0) return result;
position = 0;
result = items.get(position);
result.requestFocus();
return result;
	}
	
	public View moveFocusToLast() {
		View result = null;
		if (items.size()==0) return result;
position = items.size()-1;
result = items.get(position);
result.requestFocus();
return result;
	}
	

}
