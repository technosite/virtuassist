package com.technosite.Interface;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

public class GlassActivity extends Activity{
	
	// Change this constants to debug on a non Glass device
	public final static boolean isGlassDevice = false;

	public static final int GLASSGESTURE_TAB = 1;
	public static final int GLASSGESTURE_TAPWITHTWOFINGERS = 2;
	public static final int GLASSGESTURE_LEFT = 3;
	public static final int GLASSGESTURE_RIGHT = 4;
	public static final int GLASSGESTURE_DOWN = 5;
	public static final int GLASSGESTURE_UP = 6;
	
	


	private GestureDetector mGestureDetector;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stopLockScreen();
        
        if (isGlassDevice ) mGestureDetector = createGestureDetector(this);
	}
	
	// ** Activity events 
	
	
	protected void onGlassGestureEvent(int gestureCode) {
		
	}
	
	
	// ** Gesture management

	private GestureDetector createGestureDetector(Context context) {
	    GestureDetector gestureDetector = new GestureDetector(context);
	        //Create a base listener for generic gestures
	        gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
	            @Override
	            public boolean onGesture(Gesture gesture) {
	                if (gesture == Gesture.TAP) {
	                    // do something on tap
	                	onGlassGestureEvent(GLASSGESTURE_TAB);
	                    return true;
	                } else if (gesture == Gesture.TWO_TAP) {
	                    // do something on two finger tap
	                	onGlassGestureEvent(GLASSGESTURE_TAPWITHTWOFINGERS);
	                    return true;
	                } else if (gesture == Gesture.SWIPE_RIGHT) {
	                    // do something on right (forward) swipe
	                	onGlassGestureEvent(GLASSGESTURE_RIGHT);
	                    return true;
	                } else if (gesture == Gesture.SWIPE_LEFT) {
	                    // do something on left (backwards) swipe
	                	onGlassGestureEvent(GLASSGESTURE_LEFT);
	                    return true;
	                } else if (gesture == Gesture.SWIPE_DOWN) {
	                	onGlassGestureEvent(GLASSGESTURE_DOWN);
	                	return true;
	                } else if (gesture == Gesture.SWIPE_UP) {
	                	onGlassGestureEvent(GLASSGESTURE_UP);
	                	return true;
	                }
	                return false;
	            }
	        });
	        
	        
	        gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
	            @Override
	            public void onFingerCountChanged(int previousCount, int currentCount) {
	              // do something on finger count changes
	            	
	            }
	        });
	        gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {
	            @Override
	            public boolean onScroll(float displacement, float delta, float velocity) {
	                // do something on scrolling
	            	
	            	return true;
	            }
	        });
	        return gestureDetector;
	    }

	    @Override
	    public boolean onGenericMotionEvent(MotionEvent event) {
	        if (mGestureDetector != null) {
	            return mGestureDetector.onMotionEvent(event);
	        }
	        return false;
	    }

	    // ** Trackpad emulation
	    
	 // Stop lock screen

	    public boolean keepScreenOn = true;
	    
	    public void stopLockScreen() {
	    	// KeyguardManager keyguardManager = (KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE);
	    	// KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
	    	// lock.disableKeyguard();
	    	
	    		if (keepScreenOn)
	    		    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    		else
	    		    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    		
	    }
	    
	    

	    
}
