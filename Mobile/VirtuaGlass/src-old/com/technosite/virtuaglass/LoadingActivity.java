package com.technosite.virtuaglass;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.technosite.Interface.GlassActivity;
import com.technosite.LogManager.LogManager;
import com.technosite.TTS.TTS;
import com.technosite.virtuaglass.DBManager.DataManager;
import com.technosite.virtuaglass.model.PDTDevice;
import com.technosite.virtuaglass.model.PDTManager;
import com.technosite.virtuaglass.model.PDTPath;
import com.technosite.virtuaglass.model.PDTSurface;

public class LoadingActivity extends GlassActivity {

	private TTS tts = null;
	private ProgressBar progress = null;
	private TextView title = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tts = TTS.getSharedInstance(this);
		setContentView(R.layout.activity_loading);
		Bundle myBundle = getIntent().getExtras();
		String tmpQR= myBundle.getString("qr");

		progress = (ProgressBar) findViewById(R.id.progress);
		title = (TextView) findViewById(R.id.titleTextView);
		title.setText("Loading, please wait...");
		if (tmpQR!=null) {
			code = tmpQR;
						loadDataFromDevice();			
		}
	}
	
	@Override
	protected void onGlassGestureEvent(int code) {
    	switch(code) {
    	case GlassActivity.GLASSGESTURE_TAB :

    		break;
    	case GlassActivity.GLASSGESTURE_RIGHT :
    		
    		break;
    	case GlassActivity.GLASSGESTURE_LEFT :

    		break;
    	case GlassActivity.GLASSGESTURE_DOWN :
    		cancelProcedure();
    		onBackPressed();
    		break;
    	case GlassActivity.GLASSGESTURE_UP :
    		
    		break;
    	case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :
    		
    		break;
    	}
    }

	
	public void cancelProcedure() {
		stopProcedure  = true;
				LogManager.addUserTask("Loading data cancelled by the user");
				finish();		
	}
	
	// ** PDT service
    private String code = "pdt-1";
    private static boolean stopProcedure = false;
    
    
    private void loadDataFromDevice() {
    	    	progressValue = 0;
    	    	stopProcedure  = false;
    	    	LogManager.addSystemTask("Starting to load data for a machine from Cloud");
    	th.start();			
    }
    
    private int progressValue = 0;
    public void update(int value) {
		if (value == -2) {
			title.setText(this.getResources().getText(R.string.error_image_not_found));
			tts.stop();
			tts.speak(title.getText().toString());
			progress.setProgress(100);
		} else
		if (value == -1) {
			title.setText(this.getResources().getText(R.string.error_device_not_found));
			tts.stop();
			tts.speak(title.getText().toString());
			LogManager.addSystemTask("Device not found on Cloud");
			progress.setProgress(100);
		} else if (value>=100) {
			tts.stop();
			tts.speakNow("100%");
			LogManager.addSystemTask("Data for the device is loaded.");
						finish();			
		} else {
			progressValue += value;
			progress.setProgress(progressValue );
						tts.speak(" " + progressValue  + "%");
		}
		progress.setContentDescription(String.valueOf(progressValue) + "% " + this.getResources().getString(R.string.description_progressbar));    	
    }
    
    
	public Handler puente = new Handler() {
		  @Override
		  public void handleMessage(Message msg) {
		   update((Integer)msg.obj);

		  }
		 };
	
	private Thread th = new Thread(new Runnable() {
		
		private void updateBar(int value) {
	         Message msg = new Message();
	         msg.obj = value;
	         puente.sendMessage(msg);
		}
		
	       @Override
	       public void run() {
	    	   Log.w("*-* Loading a device", "Loading the device " + code);
	    	   int progress = 0;
	    	   DataManager da = DataManager.getSharedInstance();
	    	   PDTManager manager = PDTManager.getSharedInstance();
	    	   if (stopProcedure  ) return; // Check to cancel
	    	   PDTDevice dev = da.getDeviceforCode(code);
	      		update(10);
	      		if (dev==null) {
	      			update(-1);
	      				   		} else {
	      				   		if (dev!=null) dev.saveToDevice(); // save the device on memory
	   				dev.addSurfaces(da.getSurfaceForId(dev.id));
	   				updateBar(10);
	   				if (stopProcedure  ) return;
	   				
	   				if (dev.surfaces.size()>0) {
	   					progress = 20 / dev.surfaces.size();
	   				for (int i=0;i<dev.surfaces.size();i++) {
	   		PDTSurface sItem = dev.surfaces.get(i);
	   		sItem.addControls(da.getControlsForSurface(sItem.id));
	   		updateBar(progress);
	   		if (stopProcedure  ) return;
	   				} 
	   					   				} else update(20);
	   				dev.addPaths(da.getPathForId(dev.id));
	   				updateBar(10);
	   				if (stopProcedure  ) return;
	   				if (dev.paths.size()>0) {
	   				progress = 20 / dev.paths.size();
	   				for (int i=0;i<dev.paths.size();i++) {
	   		PDTPath sItem = dev.paths.get(i);
	   		sItem.addSteps(da.getStepForPath(sItem.id));
	   		updateBar(progress);
	   		if (stopProcedure  ) return;
	   				}
	   				} else update(20);
	   				
	   	
	   							
	   				if (stopProcedure  ) return;	   										
	   			   				manager.device = dev;
	   			   					   			   			manager.device.update(); 
	   			   					   			   				Log.w("LoadingActivity-Device data", manager.device .toString());
	   			   					   							updateBar(101); // finish	   				
	       } 
	   		}
	      });
	

	
	
}
