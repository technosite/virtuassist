package com.technosite.virtuaglass.opencv;

import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

public class PointsManager {

	public boolean initialized = false;
	
	// Number of samples for the hand and background
	public final  int          HAND_POINTS       = 7;
	//public final  int          BACKGROUND_POINTS = 8;
	
	// Arrays with the sample points
	public        Point[]      handPoints        = new Point[HAND_POINTS];
	//public        Point[]      backgroundPoints  = new Point[BACKGROUND_POINTS];

	// Color of the points in the current frame RGB
	//public        double[][]   avgColorRGB          = new double[HAND_POINTS][3];
	//public        double[][]   avgBackColorRGB      = new double[BACKGROUND_POINTS][3];
	
	// Allowed deviation in each region RGB
	//public        double[][]   cLowerRGB            = new double[HAND_POINTS][3];
	//public        double[][]   cUpperRGB            = new double[HAND_POINTS][3];
	//public        double[][]   cBackLowerRGB        = new double[BACKGROUND_POINTS][3];
	//public        double[][]   cBackUpperRGB        = new double[BACKGROUND_POINTS][3];
	
	// Color of the points in the current frame H
	public        double[][]   avgColorHS         = new double[HAND_POINTS][2];
	//public        double[][]   avgBackColorHS      = new double[BACKGROUND_POINTS][2];
		
	// Allowed deviation in each region H
	public        double[][]   cLowerHS            = new double[HAND_POINTS][2];
	public        double[][]   cUpperHS            = new double[HAND_POINTS][2];
	//public        double[][]   cBackLowerHS        = new double[BACKGROUND_POINTS][2];
	//public        double[][]   cBackUpperHS        = new double[BACKGROUND_POINTS][2];

	public static PointsManager instance = null;
	
	public static PointsManager getSingleton ()
	{
		if (instance == null) {
			synchronized (PointsManager.class) {
				PointsManager inst = instance;
				if (inst == null) {
					synchronized (PointsManager.class) {
						inst = new PointsManager();
					}
					instance = inst;
				}
			}
		}
		return instance;
	}
	
	private PointsManager ()
	{
		initCLowerUpper(10,10,10,10);//50, 50, 10, 10, 10, 10);
	    //initCBackLowerUpper(3,3,3,3);//50, 50, 3, 3, 3, 3);
	};
	
	public void initialize (Mat img)
	{
		if (!initialized)
		{
			//initialBackgroundSamplePointsLocation(img);
			initialHandSamplePointsLocation(img);
			initialized = true;
		}
	}

	//Init the thresholds of the first sample
	void initCLowerUpper(double cl1, double cu1, double cl2, double cu2)//, double cl3,double cu3)
	{
		cLowerHS[0][0] = cl1;
		cUpperHS[0][0] = cu1;
		cLowerHS[0][1] = cl2;
		cUpperHS[0][1] = cu2;
		/*cLower[0][0] = cl1;
		cUpper[0][0] = cu1;
		cLower[0][1] = cl2;
		cUpper[0][1] = cu2;
		cLower[0][2] = cl3;
		cUpper[0][2] = cu3;*/
	}
	
	/*void initCBackLowerUpper(double cl1, double cu1, double cl2, double cu2)//, double cl3,double cu3)
	{
		cBackLowerHS[0][0] = cl1;
		cBackUpperHS[0][0] = cu1;
		cBackLowerHS[0][1] = cl1;
		cBackUpperHS[0][1] = cu1;
	}*/
	
	//Calculate the colors of the hand
	public void preSampleHand(Mat img, Mat imgS)
	{
		for (int i = 0; i < HAND_POINTS; i++)
		{
			avgColorHS[i][0] = (img.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[0];
			avgColorHS[i][1] = (imgS.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[0];
			/*for (int j = 0; j < 3; j++)
			{
				avgColor[i][j] = (img.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[j];
			}*/
		}
	}	
	
	//Recalculate the colors of the hand
	public void updateSampleHand(Mat img, Mat imgS, List<Point> points)
		{
			for (int i = 0; i < HAND_POINTS; i++)
			{
				avgColorHS[i][0] = (img.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[0];
				avgColorHS[i][1] = (imgS.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[0];
				/*for (int j = 0; j < 3; j++)
				{
					avgColor[i][j] = (img.get((int)(handPoints[i].y), (int)(handPoints[i].x)))[j];
				}*/
			}
		}	

	//Calculate the color values for each sample point
	/*public void preSampleBackground(Mat img, Mat imgS)
	{
		 
		for (int i = 0; i < BACKGROUND_POINTS; i++)
		{
			avgBackColorHS[i][0] = (img.get((int)(backgroundPoints[i].y), (int)(backgroundPoints[i].x)))[0];
			avgBackColorHS[i][1] = (imgS.get((int)(backgroundPoints[i].y), (int)(backgroundPoints[i].x)))[0];
		}
	}	*/

	//Place the points of the hand
	public void initialHandSamplePointsLocation(Mat img)
	{
    	int midX = img.cols() >> 1;
    	int midY = img.rows() >> 1;
    	
    	int wOct = midX >> 3;
    	int hOct = midY >> 2;

    	for (int i = 0; i < HAND_POINTS;i++)
    	{
    		handPoints[i] = new Point();
    	}
    	
    	handPoints[0].x = midX       ;
    	handPoints[1].x = midX       ;
    	handPoints[2].x = midX + wOct;
    	handPoints[3].x = midX - wOct;
    	handPoints[4].x = midX + wOct;
    	handPoints[5].x = midX - wOct;
    	handPoints[6].x = midX       ;
    	handPoints[0].y = midY     - hOct         ;
    	handPoints[1].y = midY + hOct       ;
    	handPoints[2].y = midY /*+ hOct*/ - (hOct / 2);
    	handPoints[3].y = midY /*+ hOct*/ - (hOct / 2);
    	handPoints[4].y = midY + hOct + (hOct / 2);
    	handPoints[5].y = midY + hOct + (hOct / 2);
    	handPoints[6].y = midY + hOct + hOct;
	}
	
	//Place the points of the Background
	/*public void initialBackgroundSamplePointsLocation(Mat img)
	{
    	int offsetX = img.cols() >> 4;
    	int offsetY = img.rows() >> 3;
    	int hWidth  = img.cols() >> 1;
    	int qWidth  = img.cols() >> 2;
    	int hHeight = img.rows() >> 1;
    	
    	for (int i = 0; i < BACKGROUND_POINTS;i++)
    	{
    		backgroundPoints[i] = new Point();
    	}
    	
    	backgroundPoints[0].x = offsetX;
    	backgroundPoints[1].x = img.cols() - offsetX;
    	backgroundPoints[2].x = offsetX;
    	backgroundPoints[3].x = img.cols() - offsetX;
    	backgroundPoints[4].x = hWidth;
    	backgroundPoints[5].x = hWidth;
    	backgroundPoints[6].x = qWidth;
    	backgroundPoints[7].x = hWidth + qWidth;
    	backgroundPoints[0].y = offsetY;
    	backgroundPoints[1].y = offsetY;
    	backgroundPoints[2].y = img.rows() - offsetY;
    	backgroundPoints[3].y = img.rows() - offsetY;
    	backgroundPoints[4].y = offsetY;
    	backgroundPoints[5].y = img.rows() - offsetY;
    	backgroundPoints[6].y = hHeight;
    	backgroundPoints[7].y = hHeight;
	}*/
	
	//Paint in the image sent by parameters blue circles in the position of the samples of the hand
	public void drawHandSamplePoints(Mat img)
	{
		for (int i = 0; i < HAND_POINTS; i++)
		{
			Core.circle(img, handPoints[i], 10, new Scalar (0,0,255), -1);
		}
	}
	
	//Paint in the image sent by parameters red circles in the position of the samples of the background
	/*public void drawBackgroundSamplePoints(Mat img)
	{
		for (int i = 0; i < BACKGROUND_POINTS; i++)
		{
			if (backgroundPoints[i] != null)
				Core.circle(img, backgroundPoints[i], 10, new Scalar (255,0,0), -1);
		}
	}*/
	
	// Calculate boundaries
	public void boundariesCorrection()
	{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		double[] avgHS =  new double[]{0,0};
		for (int i = 0; i < HAND_POINTS; i++)
		{
			avgHS[0] = avgHS[0]+avgColorHS[i][0];
			avgHS[1] = avgHS[1]+avgColorHS[i][1];
		}

		avgHS[0] = avgHS[0]/HAND_POINTS;//H
		avgHS[1] = avgHS[1]/HAND_POINTS;//S
		
		double[] stdHS = new double[]{0,0};
		for (int i = 0; i < HAND_POINTS; i++)
		{
			stdHS[0] = stdHS[0]+Math.pow(avgHS[0]-avgColorHS[i][0],2);//H
			stdHS[1] = stdHS[1]+Math.pow(avgHS[1]-avgColorHS[i][1],2);//S
		}
		
		stdHS[0] = stdHS[0]/HAND_POINTS;//Math.sqrt(stdHS[0]/HAND_POINTS);
		stdHS[1] = stdHS[1]/HAND_POINTS;//Math.sqrt(stdHS[1]/HAND_POINTS);
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		for (int i = 1; i < HAND_POINTS; i++)
		{
			cLowerHS[i][0] = stdHS[0]*0.4;
			cUpperHS[i][0] = stdHS[0]*0.4;
			cLowerHS[i][1] = stdHS[1]*0.4;
			cUpperHS[i][1] = stdHS[1]*0.4;
		}
		/*for (int i = 1; i < BACKGROUND_POINTS; i++)
		{
			cBackLowerHS[i][0] = cBackLowerHS[0][0];
			cBackUpperHS[i][0] = cBackUpperHS[0][0];
			cBackLowerHS[i][1] = cBackLowerHS[0][1];
			cBackUpperHS[i][1] = cBackUpperHS[0][1];
		}*/
		
		for (int i = 0; i < HAND_POINTS; i++)
		{
			//H channel
			if (avgColorHS[i][0] - cLowerHS[i][0] < 0)
				cLowerHS[i][0] = avgColorHS[i][0];
				
			if (avgColorHS[i][0] + cUpperHS[i][0] > 180)
				cUpperHS[i][0] = 180 - avgColorHS[i][0];
				
			//S channel
			if (avgColorHS[i][1] - cLowerHS[i][1] < 0)
				cLowerHS[i][1] = avgColorHS[i][1];
				
			if (avgColorHS[i][1] + cUpperHS[i][1] > 255)
				cUpperHS[i][1] = 255 - avgColorHS[i][1];
		}
		/*for (int i = 0; i < BACKGROUND_POINTS; i++)
		{
			//H channel
			if (avgBackColorHS[i][0] - cBackLowerHS[i][0] < 0)
				cBackLowerHS[i][0] = avgBackColorHS[i][0];
				
			if (avgBackColorHS[i][0] + cBackUpperHS[i][0] > 180)
				cBackUpperHS[i][0] = 180 - avgBackColorHS[i][0];
			
			//S channel
			if (avgBackColorHS[i][1] - cBackLowerHS[i][1] < 0)
				cBackLowerHS[i][1] = avgBackColorHS[i][1];
				
			if (avgBackColorHS[i][1] + cBackUpperHS[i][1] > 255)
				cBackUpperHS[i][1] = 255 - avgBackColorHS[i][1];
		}*/
	}
	
}
