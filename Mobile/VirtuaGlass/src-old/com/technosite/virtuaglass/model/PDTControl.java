package com.technosite.virtuaglass.model;

import java.io.Serializable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.technosite.Interface.GlassActivity;
import com.technosite.virtuaglass.R;
import com.technosite.virtuaglass.controls.Frame;
import com.technosite.virtuaglass.controls.ImageViewManager;
import com.technosite.virtuaglass.opencv.ImageManager;


public class PDTControl implements Serializable {
	private static final long serialVersionUID = 20140127124000L; 

	public int id = 0;
	public String name = null;
	public String description= null;
	public PDTSurface surface = null;
	public int type= 0;
	private String typeString = null;
	public int figure= 0;
	public String color= null;
	public String borderColor= null;
	public int lineBorder = 0;
	public int width= 0;
	public int height= 0;
	public int x= 0;
	public int y= 0;
	public int radius= 0;
	public String text = null;
	public int fontSize = 0;
	public String fontFamily = null;
	public Object image = null;
	
	public Frame frame = null;
	
	
	
	public PDTControl(int pId, String pName, String pDescription, int pType, int pFigure, int pWidth, int pHeight, int pX, int pY, int pRadius, String pColor, String pBorderColor, int pLineBorder , String pText, int pFontSize, String pFontFamily, Object pImage,  PDTSurface pSurface) {
		this.id = pId;
		this.name = pName;
		this.description = pDescription;
		this.type = pType;
		this.figure = pFigure;
		this.lineBorder = pLineBorder;
		this.color = pColor;
		this.borderColor = pBorderColor;
		this.width = pWidth;
		this.height = pHeight;
		this.radius = pRadius;
		this.x= pX;
		this.y= pY;
		this.text = pText;
		this.fontSize = pFontSize;
		this.fontFamily = pFontFamily;
		this.image = pImage;
		this.surface= pSurface;
		// load constants
PDTManager manager = PDTManager.getSharedInstance();
typeString = manager.typeControl.getValueForIndex(this.type);
	}
	
	// ** Control management operations
	
	
	
	public void update() {
		calculateDimensions();
		calculatePosition();
		if (this.frame == null) frame = new Frame(this.x,this.y,this.width,this.height);
		else frame.update(this.x,this.y,this.width,this.height);
				updateImage();
		// updateColor();
		Log.w("***", this.name + " - " + this.description);
	}

	public Frame drownFrame = null;
	public void draw(Frame canvasFrame, Canvas canvas, Context context) {
		// Code for drawing in a canvas
		if (this.blinking == true) {
Frame customFrame = new Frame(x,y,width,height).getProportionalTo(surface.frame,canvasFrame);			
		int iX = customFrame.x;
		int iY = customFrame.y;
		int iWidth = customFrame.width;
		int iHeight = customFrame.height;
		drownFrame = new Frame(iX,iY,iWidth,iHeight);
		Paint paint = new Paint();
		
		switch(figure) {
		case 1 : // line
			break;
		case 2 : // rectangle
		case 3 : // fill rectangle
						paint.setColor(Color.parseColor(this.color));
						
						canvas.drawRect(customFrame.left,customFrame.top,customFrame.right,customFrame.bottom, paint);
			
			
			break;
		case 4 : // circle
		case 5 : // fill circle
			paint.setColor(Color.parseColor(this.color));
			paint.setColor(Color.RED);
			canvas.drawCircle(customFrame.centerX, customFrame.centerY, radius, paint);
			
			break;
		case 6 : // text
						paint.setColor(Color.parseColor(this.borderColor));
						
			//canvas.drawText(text, iX, iY, paint);
						canvas.drawRect(customFrame.left,customFrame.top,customFrame.right,customFrame.bottom, paint);
						
			break;
		case 7 : // image
			break;
		}
		
		}
		}

	
	public void draw(Frame canvasFrame, RelativeLayout canvas, Context context, boolean transparent) {
		// Code for drawing in a RelativeLayouts using Views
		Frame customFrame = new Frame(x,y,width,height).getProportionalTo(surface.frame,canvasFrame);			
		int iX = customFrame.x;
		int iY = customFrame.y;
		int iWidth = customFrame.width;
		int iHeight = customFrame.height;
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(iWidth, iHeight);
				params.leftMargin = iX;
				params.topMargin = iY;
				
				if (this.figure == PDTManager.FIGURETEXT) {
		// Text control
					TextView item= new TextView(context);
					item.setText(this.text);
					if (transparent) {
						item.setBackgroundColor(Color.TRANSPARENT);
									}
					else item.setBackgroundColor(Color.parseColor(this.borderColor));
					item.setTextColor(Color.parseColor(this.color));
					item.setFocusable(true);
					item.setClickable(true);
					canvas.addView(item, params);
					item.setTag(this.id);
					view = item;
				} else {
					ImageView item = new ImageView(context);
					item.setContentDescription(this.name + " (" + typeString + ")");
					Bitmap bitmap = Bitmap.createBitmap(iWidth, iHeight, Config.RGB_565);
					Paint paint = new Paint();
					Canvas can = new Canvas(bitmap);
					switch(figure) {
					case 2 : // rectangle
					case 3 : // fill rectangle
										if (transparent) {
											paint.setColor(Color.TRANSPARENT);
											ImageViewManager.changeAlpha(item,0);
										}
										else paint.setColor(Color.parseColor(this.color));
						can.drawRect(0, 0, iWidth, iHeight, paint);
						break;
					case 4 : // circle
					case 5 : // fill circle
						can.drawColor(Color.TRANSPARENT);
						if (transparent) {
							paint.setColor(Color.TRANSPARENT);
							ImageViewManager.changeAlpha(item,0);
						}
						else paint.setColor(Color.parseColor(this.color));
						can.drawCircle(iWidth/2, iHeight/2, radius, paint);
						break;
					case 6 : // text
						break;
					case 7 : // image
						break;
					}
					if (!transparent) item.setImageBitmap(bitmap);
					item.setFocusable(true);
					item.setClickable(true);
					item.setTag(this.id);
					canvas.addView(item, params);
					view = item;
						}
			}
	
				
	public void zoomDraw(Frame canvasFrame, RelativeLayout canvas, Context context, boolean transparent,Frame area) {
// Code for drawing in a RelativeLayouts using Views
		Frame customFrame = new Frame(x,y,width,height).getProportionalTo(surface.frame,canvasFrame);			
		int iX = customFrame.x;
		int iY = customFrame.y;
		int iWidth = customFrame.width;
		int iHeight = customFrame.height;
		
		// Area operations
		Frame origin = new Frame(iX,iY,iWidth,iHeight);
		Frame finalFrame = null;
		if (!origin.isOverlappingWithFrame(area)) {
						return; // no drawable			
		} else{
if (origin.isInFrame(area)) {
finalFrame = origin;	
} else {
finalFrame = origin.adjustToFrame(area); 	
}
		} 
		// move to margin top-left
		finalFrame.x = finalFrame.x - area.x;
		finalFrame.y = finalFrame.y -area.y;
		finalFrame.update();
		// scale the final frame
finalFrame.x = (finalFrame.x*canvasFrame.width) / area.width;
finalFrame.width = (finalFrame.width*canvasFrame.width) / area.width;
finalFrame.y = (finalFrame.y*canvasFrame.height) / area.height;
finalFrame.height = (finalFrame.height*canvasFrame.height) / area.height;
finalFrame.update();

// Calculate location
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(finalFrame.width, finalFrame.height);
		params.leftMargin = finalFrame.x;
		params.topMargin = finalFrame.y;
		
		
		if (!finalFrame.isDrawable()) {
			Log.e("zoomDraw:Error calculatin translation frame for control " +this.name, "Final frame: " + finalFrame.x+":"+finalFrame.y + " size:" + finalFrame.width + ":" + finalFrame.height);
			return;
		} 
		
		if (this.figure == PDTManager.FIGURETEXT) {
// Text control
			TextView item= new TextView(context);
			item.setText(this.text);
			if (transparent) {
				item.setBackgroundColor(Color.TRANSPARENT);
							}
			else item.setBackgroundColor(Color.parseColor(this.borderColor));
			item.setTextColor(Color.parseColor(this.color));
			item.setFocusable(true);
			item.setClickable(true);
			canvas.addView(item, params);
			item.setTag(this.id);
			view = item;
		} else {
			ImageView item = new ImageView(context);
			item.setContentDescription(this.name + " (" + typeString + ")");
			Bitmap bitmap = Bitmap.createBitmap(finalFrame.width, finalFrame.height, Config.RGB_565);
			Paint paint = new Paint();
			Canvas can = new Canvas(bitmap);
			switch(figure) {
			case 2 : // rectangle
			case 3 : // fill rectangle
								if (transparent) {
									paint.setColor(Color.TRANSPARENT);
									ImageViewManager.changeAlpha(item,0); 
								}
								else paint.setColor(Color.parseColor(this.color));
				can.drawRect(0, 0, finalFrame.width, finalFrame.height, paint);
				break;
			case 4 : // circle
			case 5 : // fill circle
				can.drawColor(Color.TRANSPARENT);
				if (transparent) {
					paint.setColor(Color.TRANSPARENT);
					ImageViewManager.changeAlpha(item,0);
				}
				else paint.setColor(Color.parseColor(this.color));
				can.drawCircle(finalFrame.width/2, finalFrame.height/2, radius, paint);
				break;
			case 6 : // text
				break;
			case 7 : // image
				break;
			}
			if (!transparent) item.setImageBitmap(bitmap);
			item.setFocusable(true);
			item.setClickable(true);
			item.setTag(this.id);
			canvas.addView(item, params);
			view = item;
				}
	}
	
	// ** Relationships
	
	
	public PDTControl container = null;
	
	public boolean isContainer(int xPos, int yPos, int wPos, int hPos) {
		if (x<=xPos && y <= yPos) {
if ((x+width) >= (xPos + wPos) && (y +height) >= (yPos + hPos)) {
return true;	
}
		}
		return false;
	}
	
	public boolean isHorizontalCollided(PDTControl other) {
		if ((this.y>=other.y)&& (this.y<=(other.y+other.height))) return true;
		else if ((other.y>=this.y)&& (other.y<=(this.y+this.height))) return true;
		else return false;
	}
	
	public boolean isVerticalCollided(PDTControl other) {
		if ((this.x>=other.x)&& (this.x<=(other.x+other.width))) return true;
		else if ((other.x>=this.x)&& (other.x<=(this.x+this.width))) return true;
		else return false;
			}
	
	public boolean isCollided(PDTControl other) {
if (isHorizontalCollided(other) && isVerticalCollided(other)) return true;
else return false;		
	}
	
	// ** Geography
	
	public String getLocation() {
		String result = null;
		Context context = Globals.getContext();
		if (this.container == null) {
			// Global position
			result = this.surface.name + ". ";
			result = result + getStringForLocation(globalPosition) + ", ";
			int c = 0;
			switch(globalPosition) {
			case 1:
				c = surface.getNumberOfControlsOnTop(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1)+ context.getResources().getText(R.string.ordinal) + " " + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.up);
c = surface.getNumberOfControlsOnLeft(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1)+ context.getResources().getText(R.string.ordinal) + " " + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.left);
result = result + ".";
break;
			case 2:
				c = surface.getNumberOfControlsOnTop(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.up);
c = surface.getNumberOfControlsOnLeft(this);
result = result + " " + context.getResources().getText(R.string.commathe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.left);
c = surface.getNumberOfControlsOnRight(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.right);
result = result + ".";
				break;
			case 3:
				c = surface.getNumberOfControlsOnTop(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.up);
c = surface.getNumberOfControlsOnRight(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.right);
result = result + ".";
				break;
			case 4 :
				c = surface.getNumberOfControlsOnTop(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.up);
c = surface.getNumberOfControlsOnLeft(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.left);
result = result + ".";
				break;
			case 5 :
				c = surface.getNumberOfControlsOnTop(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.up);
c = surface.getNumberOfControlsOnBottom(this);
result = result + " " + context.getResources().getText(R.string.commathe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.down);
c = surface.getNumberOfControlsOnLeft(this);
result = result + " " + context.getResources().getText(R.string.commathe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.left);
c = surface.getNumberOfControlsOnRight(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.right);
result = result + ".";
				break;
			case 6 :
				c = surface.getNumberOfControlsOnTop(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.up);
c = surface.getNumberOfControlsOnRight(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.right);
result = result + ".";
				break;
			case 7 :
				c = surface.getNumberOfControlsOnBottom(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.down);
c = surface.getNumberOfControlsOnLeft(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.left);
result = result + ".";
				break;
			case 8 :
				c = surface.getNumberOfControlsOnBottom(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.down);
c = surface.getNumberOfControlsOnLeft(this);
result = result + " " + context.getResources().getText(R.string.commathe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.left);
c = surface.getNumberOfControlsOnRight(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.right);
result = result + ".";
				break;
			case 9 :
				c = surface.getNumberOfControlsOnBottom(this);
result = result + context.getResources().getText(R.string.isthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.down);
c = surface.getNumberOfControlsOnRight(this);
result = result + " " + context.getResources().getText(R.string.andthe) + " " + (c+1) + context.getResources().getText(R.string.ordinal) + context.getResources().getText(R.string.elementin) + " " + context.getResources().getText(R.string.right);
result = result + ".";
			}
			
		} else {
			// inside a panel
			result = context.getResources().getText(R.string.isin) + " " + container.name + ", " +container.getLocation();
			
		}
		
		return result;
		
	}

	public int globalPosition = 0;
	public int containerPosition = 0;
		
	public void calculateDimensions() {
if (width<=0 || height<=0) {
// Circle
	if (radius>0) {
		// Fix coordenades
		x = x- radius;
		y = y-radius;
		width = radius*2;
		height = width;
	} else {
		Log.e("calculateDimensions:Control drawing", "Control has no dimensions");
	}
}

	}
	
	public void calculatePosition() {
		// Global
		if (this.surface != null) {
			int pWidth3 = surface.width / 3;
			int pHeight3 = surface.height / 3;
			if (this.x <= pWidth3) {
				if (this.y <=pHeight3) globalPosition= 1;
				else if (this.y <=(pHeight3*2)) globalPosition= 4;
				else globalPosition= 7;
			} else if (this.x <= (pWidth3*2)) {
				if (this.y <=pHeight3) globalPosition= 2;
				else if (this.y <=(pHeight3*2)) globalPosition= 5;
				else globalPosition= 8;
			} else {
				if (this.y <=pHeight3) globalPosition= 3;
				else if (this.y <=(pHeight3*2)) globalPosition= 6;
				else globalPosition= 9;				
			}
		}
		// Container
		if (this.container != null) {
			int pWidth3 = container.width / 3;
			int pHeight3 = container.height / 3;
			int fx = x - container.x;
			int fy = y - container.y;
			if (fx <= pWidth3) {
				if (fy <=pHeight3) containerPosition= 1;
				else if (fy <=(pHeight3*2)) containerPosition= 4;
				else containerPosition= 7;
			} else if (fx <= (pWidth3*2)) {
				if (fy <=pHeight3) containerPosition= 2;
				else if (fy <=(pHeight3*2)) containerPosition= 5;
				else containerPosition= 8;
			} else {
				if (fy <=pHeight3) containerPosition= 3;
				else if (fy <=(pHeight3*2)) containerPosition= 6;
				else containerPosition= 9;				
			}
		}
	}
	
	private String getStringForLocation(int location) {
		String result = null;
		Context context = Globals.getContext();
		
		switch(location) {
		case 1:
			result = context.getResources().getString(R.string.position1);
			break;
		case 2:
			result = context.getResources().getString(R.string.position2);
			break;
		case 3:
			result = context.getResources().getString(R.string.position3);
			break;
		case 4:
			result = context.getResources().getString(R.string.position4);
			break;
		case 5:
			result = context.getResources().getString(R.string.position5);
			break;
		case 6:
			result = context.getResources().getString(R.string.position6);
			break;
		case 7:
			result = context.getResources().getString(R.string.position7);
			break;
		case 8:
			result = context.getResources().getString(R.string.position8);
			break;
		case 9:
			result = context.getResources().getString(R.string.position9);
			break;
		}

return result;
	}
	
	// ** Blink management
	
private boolean blinking = false;
public void setBlinking(boolean value) {
	this.blinking = value;
}

public boolean getBlinking() {
	return this.blinking;
}

// ** link from onClickEvent

private View view = null;

public View getView() {
	return this.view;
}

public void setView(View value) {
	this.view = value;
}

// ** Image and OpenCV management


private void updateImage() {
	if (surface !=null) {
if (surface .image != null) {
	Bitmap imgSurface = (Bitmap) surface.image;
	Bitmap im= Bitmap.createBitmap(imgSurface, frame.x, frame.y,frame.width,frame.height, null, false);
	
	int realWidth = 640;
	int realHeight = 200;
	if (this.width < 480) {
		realWidth = 480;
		realHeight = 150;
			}
	if (this.width < 400) {
realWidth = 384;
realHeight = 120;
	}
	
	int canvasWidth = 640;
	int canvasHeight = 200;
	int partialWidth ;
	int partialHeight;
	float xScale = (float) this.width / realWidth;
	float yScale = (float) this.height / realHeight;
	if (yScale>xScale) {
partialWidth = Math.round(this.width * (1/yScale));			
partialHeight = Math.round(this.height * (1/yScale));
	} else {
		partialWidth = Math.round(this.width * (1/yScale));			
		partialHeight = Math.round(this.height * (1/yScale));
	}
	
	
	Bitmap bitmap = Bitmap.createBitmap(canvasWidth, canvasHeight, Config.RGB_565);
	Canvas canvas = new Canvas(bitmap);
	canvas.drawColor(Color.TRANSPARENT);
	
	Bitmap backgroundBitmap = Bitmap.createScaledBitmap((Bitmap) im, partialWidth, partialHeight, false);
	int pWidth = (int) partialWidth;
	int pHeight = (int) partialHeight;
	int posXCanvas = (640 -pWidth) / 2;
	int posYCanvas = (200 -pHeight) / 2;
	canvas.drawBitmap(backgroundBitmap,posXCanvas,posYCanvas,null);
	
	this.image = bitmap;

if (this.image ==null) Log.e("PDTControl", "Control " + this.name +" has no image");
} else Log.e("PDTControl", "Control " + this.name +" has no image because the surface " + surface.name + " has no image");
	} 
}


public int tone = -1;
public int minTone = -1;
public int maxTone = -1;
public int similarControls = 0;

private void updateColor() {
	if (image==null) return;
	if (!GlassActivity.isGlassDevice) return;
			
// Calculate the histogram
		if (this.image!=null) {
			Log.w("PDTControl", "updateColor");

try {
	ImageManager ig = ImageManager.getSharedInstance();
	int[] colors= ig.calculateTonesByHistogram((Bitmap) this.image);
	tone = colors[0];
	minTone = colors[1];
	maxTone = colors[2];	
} catch (Exception e) {
	Log.e("PDTControl", "updateColor error: " + e);
tone = -1;
minTone = -1;
maxTone = -1;
}
		}
	}

	

// ** Position management

public int positionX = -1;
public int positionY = -1;

public void updatePosition(int leftOffset, int topOffset) {
	positionX = (x + (width/2)) - leftOffset;
	positionY = (y + (height /2)) - topOffset;
}




}
