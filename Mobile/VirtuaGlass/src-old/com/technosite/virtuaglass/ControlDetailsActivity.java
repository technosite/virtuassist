package com.technosite.virtuaglass;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;

public class ControlDetailsActivity extends Activity {

	public static boolean showingAControl = false;
	public static Bitmap imageToShow = null;
	private TextView controlDetailsTextView;
	private ImageView controlDetailsImageView;
	private TTS tts = null;

	private String text;
	private long time = 3000;
	private int image;

	private SoundFX sndDone ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showingAControl = true;
		tts = TTS.getSharedInstance(this);
		sndDone = new SoundFX(this, R.raw.close);
		setContentView(R.layout.control_details);
		controlDetailsTextView = (TextView) findViewById(R.id.controlDetailsTextView);
		controlDetailsImageView = (ImageView) findViewById(R.id.controlDetailsImageView);

		Bundle extras = getIntent().getExtras();
		if(extras != null)
		{
			text = extras.getString("text");
		}

		controlDetailsTextView.setText(text);
		if (imageToShow !=null) controlDetailsImageView.setImageBitmap(imageToShow );
		else Log.e("ControlDetails", "There is no image to show");
		tts.speakWithDelay(text, 300);

		Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						sndDone.play();
						showingAControl = false;
						finish();
					}
				});
			}
		};
		timer.schedule(task, time);
	}



	public static void show(Activity activity, String text, Bitmap image) {
		if (showingAControl) return;
		imageToShow  = image;
		Intent intent = new Intent(activity, ControlDetailsActivity .class);
		Bundle myBundle = new Bundle();
		myBundle.putString("text", text);
		intent.putExtras(myBundle); 
		activity.startActivity(intent);		
	}

}
