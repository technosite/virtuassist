package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.technosite.Interface.GlassActivity;
import com.technosite.LogManager.LogManager;
import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;
import com.technosite.virtuaglass.model.PDTManager;

public class StepViewerActivity extends GlassActivity {

	
	private TextView operationNameTextView;
	private TextView descriptionStepTextView;

	private ImageView imageStepImageView;


	
	private TTS tts = null;
	private PDTManager manager = null;
		
private int totalSteps;
	private int index;
	
	private SoundFX sndTic = null;
	private SoundFX sndToc = null;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	 
		manager = PDTManager.getSharedInstance();
		tts = TTS.getSharedInstance(this);
		index = 0;
		
		sndTic = new SoundFX(this,R.raw.tic);
		sndToc = new SoundFX(this,R.raw.toc);
		
		setContentView(R.layout.step_viewer);

	operationNameTextView = (TextView) findViewById(R.id.operationNameTextView);
	descriptionStepTextView = (TextView) findViewById(R.id.descriptionStepTextView);
	imageStepImageView = (ImageView) findViewById(R.id.imageStepImageView);

	updateData();
 
	
	// only in first step.
	Resources res = this.getResources();
	String text = res.getString(R.string.operations_instructions);
	long time = 5000;
	NotificationViewerActivity.showWithRequest(this, text, time, R.drawable.instructionoperations);

	}// end onCreate.

	// ** System events methods.

	@Override
	protected void onGlassGestureEvent(int code) {
    	switch(code) {
    	case GlassActivity.GLASSGESTURE_TAB :
speakStep();
    		break;
    	case GlassActivity.GLASSGESTURE_RIGHT :
next();    		
    		break;
    	case GlassActivity.GLASSGESTURE_LEFT :
previous();
    		break;
    	case GlassActivity.GLASSGESTURE_DOWN :
    		    		onBackPressed(); 
    		break;
    	case GlassActivity.GLASSGESTURE_UP :
    		next();
    		break;
    	case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :
speakPosition();    		
    		break;
    	}
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
tts.speakWithDelay(descriptionStepTextView.getText().toString(),1000);
	}
	
// 	** Helper methods.

	private void updateData()
	{
		manager.step = manager.path.getStep(index);
		totalSteps = manager.path.getNumberOfSteps();
		String title = manager.path.name+ " (" + String.valueOf(manager.step.position) + "/" + String.valueOf(totalSteps) + ")";
operationNameTextView.setText(title);
descriptionStepTextView.setText(manager.step.instruction );


Bitmap b =  (Bitmap) manager.step.image;
if (b!=null) {
	imageStepImageView .setVisibility(View.VISIBLE);
		imageStepImageView.setImageBitmap(b);
} else {
	Log.e("StepViewActivity-Update", "Step " + title  + " has no image");
	imageStepImageView .setVisibility(View.GONE);	
}

LogManager.addUserTask("Showing step '" + title + "'");	
	}// end updateData.
	
	private void previous() {
		index--;
    	if(index >= 0)
    {
    		LogManager.addUserTask("Moving to previous step.");
    		sndTic.play();
    	           updateData();
    	           speakStep();
               }// end if index 1.
    	else
    	{
    		LogManager.addUserTask("User tryes to show the step previous the first one.");
    		sndToc.play();
    		index = 0;
    	}// end else index 0.		
	}
	
	private void next() {
        index++;
    	if(index < totalSteps)
{
    		LogManager.addUserTask("Moving to next step.");
    		sndTic.play();
updateData();
speakStep();
}// end if.
    	else
    	{
finishPath();    	
    		            }// end else.		
	}
	
	private void finishPath() {
		index = totalSteps;
		Intent intent = new Intent(Intent.ACTION_VIEW);
	   	intent.setClassName(StepViewerActivity.this, PathFinishedActivity.class.getName());
	   	this.finish();
	startActivity(intent);		
	}
	
	private void speakStep(){
		tts.speakNow(descriptionStepTextView.getText().toString());
			}
	
	private void speakPosition() {
		tts.speakNow("" + (index+1) + " of " + totalSteps);
	}

}// end class.
