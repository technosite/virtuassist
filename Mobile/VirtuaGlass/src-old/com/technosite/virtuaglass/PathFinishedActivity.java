package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.widget.TextView;

import com.technosite.Interface.GlassActivity;
import com.technosite.LogManager.LogManager;
import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;

public class PathFinishedActivity extends GlassActivity {

	private TTS tts = null;
	private SoundFX sndFinish = null;
	private static String message = null; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sndFinish = new SoundFX(this,R.raw.finish);
		tts = TTS.getSharedInstance(this);
		setContentView(R.layout.path_finished);
		TextView t = (TextView) findViewById(R.id.pathFinishedTextView);
		message= t.getText().toString();
		
		
		
		

		Timer timer = new Timer();
		TimerTask task = new TimerTask()
		{
			@Override
			public void run()
			{
						runOnUiThread(new Runnable()
	{
	@Override
		public void run()
		{
		sndFinish.play();
		tts.speakNow(message );
		LogManager.addUserTask("The path is finished");
		}// end run.
		});
					}// end run.
			};
		timer.schedule(task, 1000);
		
	
		}// end onCreate.

	@Override
	protected void onGlassGestureEvent(int code) {
    	switch(code) {
    	case GlassActivity.GLASSGESTURE_TAB :

    		break;
    	case GlassActivity.GLASSGESTURE_RIGHT :
    		
    		break;
    	case GlassActivity.GLASSGESTURE_LEFT :

    		break;
    	case GlassActivity.GLASSGESTURE_DOWN :
    		onBackPressed(); 
    		
    		
    		break;
    	case GlassActivity.GLASSGESTURE_UP :
    		
    		break;
    	case GlassActivity.GLASSGESTURE_TAPWITHTWOFINGERS :
    		
    		break;
    	}
    	
    }

	

	
}// end class.
