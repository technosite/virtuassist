package com.technosite.virtuaglass;

/*
 * � Copyright 2014 Fundosa Technosite S.A.
 *
 * VirtuAssist is licensed under the Apache License, Version 2.0. You may
 * obtain a copy of this license at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an �AS IS� BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This software includes methods of the openCV library.
 */

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.technosite.LogManager.LogManager;
import com.technosite.TTS.TTS;
import com.technosite.media.SoundFX;

public class NotificationViewerActivity extends Activity {

	private TextView notificationViewerHeaderTextView;
	private ImageView notificationViewerImageView;
	private TTS tts = null;

	private String text;
	private long time;
	private int image;
	private boolean request = true;
	
	private SoundFX sndDone ;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tts = TTS.getSharedInstance(this);
		sndDone = new SoundFX(this, R.raw.close);
setContentView(R.layout.notification_viewer);

// get data from MainActivity
Bundle extras = getIntent().getExtras();
int r = -1;

if(extras != null)
{
	text = extras.getString("text");
	time = extras.getLong("time");
	image= extras.getInt("image");
	r= extras.getInt("response");
}// end if extras.
if (r==0) request = false;
else request = true;


notificationViewerHeaderTextView = (TextView) findViewById(R.id.notificationViewerHeaderTextView);
	notificationViewerImageView = (ImageView) findViewById(R.id.notificationViewerImageView);
	

	notificationViewerHeaderTextView.setText(text);
	;	notificationViewerImageView.setImageResource(image);

	LogManager.addUserTask("Reading notification with text '" + text + "'");	
	// ** TTS output
	tts.speakWithDelay(text, 300);
	// This activity will be hidden after set time (5 seconds).
	Timer timer = new Timer();
	TimerTask task = new TimerTask()
	{
		@Override
		public void run()
		{
					runOnUiThread(new Runnable()
{
@Override
	public void run()
	{
	sndDone.play();
	if (request) {
		
		Intent resultData = new Intent();
				setResult(0, resultData);
					}
	tts.stop();
	LogManager.addSystemTask("Closing notification for the user");
NotificationViewerActivity.this.finish();

	}// end run.
	});
				}// end run.
		};
	timer.schedule(task, time);

	}// end onCreate.


	public final static int NotificationViewerRequest = 14152096;
	
	public static void show(Activity activity, String text, long time, int image) {
			Intent intent = new Intent(activity, NotificationViewerActivity.class);
			Bundle myBundle = new Bundle();
			myBundle.putString("text", text);
			myBundle.putLong("time", time);
			myBundle.putInt("image", image);
			myBundle.putInt("response", 0);
			
						intent.putExtras(myBundle);
									activity.startActivity(intent);		
	}
	
	public static void showWithRequest(Activity activity, String text, long time, int image) {
		Intent intent = new Intent(activity, NotificationViewerActivity.class);
		Bundle myBundle = new Bundle();
		myBundle.putString("text", text);
		myBundle.putLong("time", time);
		myBundle.putInt("image", image);
		myBundle.putInt("response", NotificationViewerRequest);
		intent.putExtras(myBundle); 
		activity.startActivityForResult(intent, NotificationViewerRequest );		
}
	
	
}// end class.
