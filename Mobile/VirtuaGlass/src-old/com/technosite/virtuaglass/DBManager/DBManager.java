package com.technosite.virtuaglass.DBManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import android.util.Log;

public class DBManager {

	private static DBManager  _instance = null;
	
	public static DBManager  getSharedInstance() {
if (_instance==null) _instance = new DBManager ();
return _instance;
	}
	
	public DBManager() {
		// TODO Auto-generated constructor stub
		initConexionMySQLURL();
	}

	private String conexionMySQLURL = null;
	private static Connection conDB = null;
	private ResultSet rs = null;
	
	private void initConexionMySQLURL() {
		conexionMySQLURL = "jdbc:mysql://" + DBPersistence.DBHost + ":" + DBPersistence.DBPort;		
	}
	
	
	public boolean isServerConnectionAvailable() {
		boolean result = true;
		try {
			if (conexionMySQLURL == null) initConexionMySQLURL(); 
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(conexionMySQLURL, DBPersistence.DBUser, DBPersistence.DBPassword);
			con.close();			
		} catch (ClassNotFoundException e) {
Log.e("DBManager", "Class not found. " +e);
result = false;
		}
		catch (SQLException e) {
Log.e("DBManager", "SQL exception: " + e);
result = false;
		}
		catch (Exception e) {
			Log.e("DBManager", "Error checking availability connection to server: " +e);
			result = false;
		}
		return result;
	}

	public boolean isDataBaseConnectionAvailable() {
		boolean result = true;
		try {
			if (conexionMySQLURL == null) initConexionMySQLURL(); 
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(conexionMySQLURL + "/" + DBPersistence.DBName, DBPersistence.DBUser, DBPersistence.DBPassword);
			con.close();			
		} catch (ClassNotFoundException e) {
Log.e("DBManager", "Class not found. " +e);
result = false;
		}
		catch (SQLException e) {
Log.e("DBManager", "SQL exception: " + e);
result = false;
		}
		catch (Exception e) {
			Log.e("DBManager", "Error checking availability connection: " +e);
			result = false;
		}
		return result;
	}

	 
	public boolean startDataBaseConnection() {
		boolean result = true;
		try {
			if (conexionMySQLURL == null) initConexionMySQLURL(); 
			Class.forName("com.mysql.jdbc.Driver");
			conDB= DriverManager.getConnection(conexionMySQLURL + "/" + DBPersistence.DBName, DBPersistence.DBUser, DBPersistence.DBPassword);
		} catch (ClassNotFoundException e) {
Log.e("DBManager", "Class not found. " +e);
result = false;
		}
		catch (SQLException e) {
Log.e("DBManager", "Error starting connection. SQL exception: " + e);
result = false;
		}
		catch (Exception e) {
			Log.e("DBManager", "Error starting connection: " +e);
			result = false;
		}
		return result;
	}

	public void closeDataBaseConnection() {
try {
	if (conDB != null) {
		conDB.close();
		conDB = null;
if (rs != null) {
	rs.close();
	rs = null;
}
	}	
} catch (SQLException e) {
	Log.e("DBManager", "Error closing the connection. SQL exception: " + e);
				}
	}
	
	public int getNumberOfRows() {
		int result = -1;
		try {
		if (rs != null) {
			result = rs.getRow();
		}
		} catch (SQLException e) {
			Log.e("DBManager", "Error getting the number of rows. SQL exception: " + e);
		}
		return result;
	}
	
	public int getNumberOfColumns() {
		int result = -1;
		try {
		if (rs != null) {
			result = rs.getMetaData().getColumnCount();
		}
		} catch (SQLException e) {
			Log.e("DBManager", "Error getting the number of columns. SQL exception: " + e);
		}
		return result;
	}
	
	public String getColumnName(int columnNumber) {
String result = null;		
		try {
			result = rs.getMetaData().getColumnName(columnNumber).toString();
		} catch (SQLException e) {
			Log.e("DBManager", "Error getting the column name. SQL exception: " + e);
		}
		return result;
	}
	
	public int getColumnNumber(String columnName) {
		int result = -1;
for (int i=1;i<=this.getNumberOfColumns();i++) {
if (columnName.equalsIgnoreCase(this.getColumnName(i))) {
result = i;
break;
}
}
return result;
	}

	public boolean last() {
		boolean result = false;
		try {
result = rs.last();			
		} catch (SQLException e) {
			Log.e("DBManager", "Error jumping to last element. SQL exception: " + e);
		}
		return result;
	}
	
	public boolean first() {
		boolean result = false;
		try {
result = rs.first();			
		} catch (SQLException e) {
			Log.e("DBManager", "Error jumping to first element. SQL exception: " + e);
		}
		return result;
	}
	
	public boolean next() {
		boolean result = false;
		try {
result = rs.next();			
		} catch (SQLException e) {
			Log.e("DBManager", "Error jumping to next element. SQL exception: " + e);
		}
		return result;
	}
	
	public Object getValue(int columnNumber) {
		Object result = null;
		try {
result = rs.getObject(columnNumber);			
		} catch (SQLException e) {
			Log.e("DBManager", "Error getting a element. SQL exception: " + e);
		}
		return result;
	}
	
	public Object getValue(String columnName) {
		int col = this.getColumnNumber(columnName);
		return this.getValue(col);		
			}

	public void execute(String SQLString) {
if (conDB == null) {
	
}
if (_instance.startDataBaseConnection()) {
	
}
	}
}
