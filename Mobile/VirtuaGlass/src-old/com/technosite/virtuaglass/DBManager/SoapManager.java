package com.technosite.virtuaglass.DBManager;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.os.AsyncTask;
import android.util.Log;

public class SoapManager {
	
	// ** Attributes and subclasses
	
	
	public boolean isRunning = false;
	public SoapResult result = null;
	private SoapHeader header = null;
	
	

	public SoapManager() {
		// TODO Auto-generated constructor stub
	}
	
	public static SoapResult createResult(SoapObject so) {
return (SoapResult) new SoapResult(so);		
	}
	
	public void createCommunicationWithMethod(String method, String action) {
		header = new SoapHeader(DBPersistence.SOAPNameSpace, DBPersistence.SOAPUrl,method, "\""+action +"\"", this); 
	}
	
	public void addParameter(String key, String value) { 
		header.request.addProperty(key,value);
	}
	
	public void addParameter(String key, int value) {
				header.request.addProperty(key,value);
			}
	
	public void execute() {
		if (!this.isRunning) {
			this.isRunning = true;
			// ** call asyncTask
			WSQueryTask task = new WSQueryTask();
			task.execute(this.header);
		}
	}
	
	public SoapResult getResult() {
		while(this.isRunning) {
			// Empty
		}
		return this.result;
	}
	
	
	//** AsyncTask in background

		private class WSQueryTask extends AsyncTask<SoapHeader, Integer,SoapResult> {
			
			private SoapManager delegate = null;
			
			protected SoapResult doInBackground(SoapHeader... headers) {
				SoapResult tmpResult = null;
				SoapHeader header = headers[0];
				
				this.delegate = header.delegate;
				
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(header.request);
				HttpTransportSE transporte = new HttpTransportSE(header.url); 
				try {
					transporte.call(header.action, envelope);
										// Get results
					SoapObject tmpObjectResult  =( SoapObject ) envelope.bodyIn;
					tmpResult = new SoapResult(tmpObjectResult );
					delegate.result = tmpResult ;
					delegate.isRunning = false;
				} catch (Exception e) {
		Log.e("SOAP connection management", "Error: " + e.toString());
		delegate.result = new SoapResult(null);
		delegate.isRunning = false;						
				}
				return tmpResult;
			}
			
			protected void onPostExecute(SoapResult result) {
				delegate.result = result ;
				delegate.isRunning = false;
				
			}
		
		}
		

		private class SoapHeader {

			public SoapObject request = null;
			public String nameSpace = null;
			public String url = null;
			public String action= null;
			public String method= null;
			public SoapManager delegate = null;
			
			
			public SoapHeader(String nameSpace, String pUrl, String pMethod, String pAction, SoapManager pDelegate) {
				this.nameSpace = nameSpace;
				method = pMethod;
				action = pAction;
				url = pUrl;
				delegate = pDelegate;
								if (request != null) request = null;
				request = new SoapObject(nameSpace, this.method );
			}
			
			public String toString() {
				return "Soap header info:\nNamespace: " + nameSpace + "\nURL: " +url + "\nMethod: " + method + "\nAction: " +action;
			}
		}
		
		
}