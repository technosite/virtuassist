LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#OPENCV_CAMERA_MODULES:=off
#OPENCV_INSTALL_MODULES:=off
#OPENCV_LIB_TYPE:=SHARED
include /Developer/OpenCV/android/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := VirtuaGlass
LOCAL_SRC_FILES := VirtuaGlass.cpp
LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_LDLIBS +=  -llog -ldl


include $(BUILD_SHARED_LIBRARY)
