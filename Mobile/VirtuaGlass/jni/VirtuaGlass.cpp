#include <jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

using namespace std;
using namespace cv;

extern "C" {

JNIEXPORT jdouble JNICALL Java_com_technosite_virtuaglass_opencv_HandGesture_findInscribedCircleJNI(JNIEnv* env, jobject obj, jlong imgAddr, jdouble rectTLX, jdouble rectTLY, jdouble rectBRX, jdouble rectBRY, jdoubleArray incircleX, jdoubleArray incircleY, jlong contourAddr);
JNIEXPORT void JNICALL Java_com_technosite_virtuaglass_opencv_ControlDetection_calculateHistogramJNI(JNIEnv* env, jobject obj, jlong imgAddrIn, jlong imgAddrOut);
JNIEXPORT void JNICALL Java_com_technosite_virtuaglass_opencv_ControlDetection_rotateImage(JNIEnv* env, jobject obj, jlong src, jlong dest, jdouble angle);

int limitLeft;
int limitRight;
int limitTop;
int limitBottom;

/*
 * http://opencv-code.com/quick-tips/how-to-rotate-image-in-opencv/
 * Rotate an image
 */
JNIEXPORT void JNICALL Java_com_technosite_virtuaglass_opencv_ControlDetection_rotateImage(JNIEnv* env, jobject obj, jlong addrIn, jlong addrOut, jdouble angle){
	Mat* out = (Mat*)addrOut;
	Mat& in  = *(Mat*)addrIn;

	std::vector<cv::Point> points;
	cv::Mat_<uchar>::iterator it = in.begin<uchar>();
	cv::Mat_<uchar>::iterator end = in.end<uchar>();
	for (; it != end; ++it)
		if (*it)
			points.push_back(it.pos());

	cv::RotatedRect box = cv::minAreaRect(cv::Mat(points));

	cv::Mat rot_mat = cv::getRotationMatrix2D(box.center, angle, 1);

	cv::warpAffine(in, *out, rot_mat, in.size(), cv::INTER_CUBIC);

	/*cv::Size box_size = box.size;
	if (box.angle < -45.)
	    std::swap(box_size.width, box_size.height);
	cv::Mat cropped;
	cv::getRectSubPix(*out, box_size, box.center, cropped);
*/
    /*int len = std::max(in.cols, in.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(in, *out, r, cv::Size(len, len));*/
}


JNIEXPORT jdouble JNICALL Java_com_technosite_virtuaglass_opencv_HandGesture_findInscribedCircleJNI(JNIEnv* env, jobject obj, jlong imgAddr,
		jdouble rectTLX, jdouble rectTLY, jdouble rectBRX, jdouble rectBRY,
		jdoubleArray incircleX, jdoubleArray incircleY, jlong contourAddr)
{
	Mat& img_cpp  = *(Mat*)imgAddr;

	//vector<Point2f>& contour = *(vector<Point2f> *)contourAddr;
	Mat& contourMat = *(Mat*)contourAddr;
	vector<Point2f> contourVec;
	contourMat.copyTo(contourVec);

	double r = 0;
	double targetX = 0;
	double targetY = 0;

	for (int y = (int)rectTLY; y < (int)rectBRY; y++)
	{
		for (int x = (int)rectTLX; x < (int)rectBRX; x++)
		{
			double curDist = pointPolygonTest(contourVec, Point2f(x, y), true);

			if (curDist > r) {
				r = curDist;
				targetX = x;
				targetY = y;
			}
		}
	}

	jdouble outArrayX[] = {0};
	jdouble outArrayY[] = {0};

	outArrayX[0] = targetX;
	outArrayY[0] = targetY;

	env->SetDoubleArrayRegion(incircleX, 0 , 1, (const jdouble*)outArrayX);
	env->SetDoubleArrayRegion(incircleY, 0 , 1, (const jdouble*)outArrayY);
	//Core.circle(img, inCircle, (int)inCircleRadius, new Scalar(240,240,45,0), 2);

	return r;
}

JNIEXPORT void JNICALL Java_com_technosite_virtuaglass_opencv_ControlDetection_calculateHistogramJNI(JNIEnv* env, jobject obj, jlong imgAddrIn, jlong imgAddrOut)
{
	Mat& image  = *(Mat*)imgAddrIn;

	/// Establish the number of bins
	int histSize[] = {181};

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 181 } ;
	const float* histRange[] = { range };

	bool uniform = true;
	bool accumulate = false;
	int channels[] = {0};

	Mat* hist = (Mat*) imgAddrOut;

	/// Compute the histograms:
	calcHist(&image, 1, channels, Mat(), *hist, 1, histSize, histRange, uniform, accumulate);

	/// Normalize the result to [ 0, histImage.rows ]
	normalize(*hist, *hist, 0, (*hist).rows, NORM_MINMAX, -1, Mat() );
}



}
